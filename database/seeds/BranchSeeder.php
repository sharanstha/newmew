<?php

use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Modules\Branch\Models\Branch::create([
            'id' => 1,
            'name' => 'admin',
            'location' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => 'admin'
        ]);
    }
}
