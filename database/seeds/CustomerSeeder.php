<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Modules\Customer\Models\Customer::create([
            'first_name' => 'not_available',
            'last_name' => 'not_available',
            'primary_contact_no' => 'not_available'
        ]);
    }
}
