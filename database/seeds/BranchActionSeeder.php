<?php

use Illuminate\Database\Seeder;

class BranchActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Modules\Permission\Models\BranchAction::create([
            'id' => 1,
            'action' => 'customer',
        ]);
        App\Modules\Permission\Models\BranchAction::create([
            'id' => 2,
            'action' => 'user',
        ]);
        App\Modules\Permission\Models\BranchAction::create([
            'id' => 6,
            'action' => 'stockin',
        ]);
        App\Modules\Permission\Models\BranchAction::create([
            'id' => 7,
            'action' => 'stockout',
        ]);
        App\Modules\Permission\Models\BranchAction::create([
            'id' => 10,
            'action' => 'stockin create',
        ]);
        App\Modules\Permission\Models\BranchAction::create([
            'id' => 11,
            'action' => 'stockin edit',
        ]);
        App\Modules\Permission\Models\BranchAction::create([
            'id' => 12,
            'action' => 'stockin detail',
        ]);
        App\Modules\Permission\Models\BranchAction::create([
            'id' => 13,
            'action' => 'detail import',
        ]);
        App\Modules\Permission\Models\BranchAction::create([
            'id' => 14,
            'action' => 'branch permission',
        ]);
    }
}
