<?php

use Illuminate\Database\Seeder;

class BranchPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Modules\Permission\Models\BranchPermission::create([
            'permission_code'=>'11',
            'staff_id'=>'1',
            'action_id'=>'1',
        ]);
        App\Modules\Permission\Models\BranchPermission::create([
            'permission_code'=>'12',
            'staff_id'=>'1',
            'action_id'=>'2',
        ]);
        App\Modules\Permission\Models\BranchPermission::create([
            'permission_code'=>'16',
            'staff_id'=>'1',
            'action_id'=>'6',
        ]);
        App\Modules\Permission\Models\BranchPermission::create([
            'permission_code'=>'17',
            'staff_id'=>'1',
            'action_id'=>'7',
        ]);
    }
}
