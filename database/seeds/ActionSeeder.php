<?php

use Illuminate\Database\Seeder;

class ActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Modules\Permission\Models\Action::create([
            'action'=>'customer',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'user',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'vendor',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'category',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'brand',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'stockin',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'stockout',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'branch',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'user permission',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'stockin create',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'stockin edit',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'stockin detail',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'detail import',
        ]);
        App\Modules\Permission\Models\Action::create([
            'action'=>'branch permission',
        ]);
    }
}
