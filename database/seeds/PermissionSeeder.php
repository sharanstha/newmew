<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'11',
            'staff_id'=>'1',
            'action_id'=>'1',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'12',
            'staff_id'=>'1',
            'action_id'=>'2',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'13',
            'staff_id'=>'1',
            'action_id'=>'3',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'14',
            'staff_id'=>'1',
            'action_id'=>'4',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'15',
            'staff_id'=>'1',
            'action_id'=>'5',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'16',
            'staff_id'=>'1',
            'action_id'=>'6',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'17',
            'staff_id'=>'1',
            'action_id'=>'7',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'18',
            'staff_id'=>'1',
            'action_id'=>'8',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'19',
            'staff_id'=>'1',
            'action_id'=>'9',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'110',
            'staff_id'=>'1',
            'action_id'=>'10',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'111',
            'staff_id'=>'1',
            'action_id'=>'11',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'112',
            'staff_id'=>'1',
            'action_id'=>'12',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'113',
            'staff_id'=>'1',
            'action_id'=>'13',
        ]);
        App\Modules\Permission\Models\Permission::create([
            'permission_code'=>'114',
            'staff_id'=>'1',
            'action_id'=>'14',
        ]);

    }
}
