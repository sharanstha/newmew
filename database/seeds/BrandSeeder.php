<?php

use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Modules\Brand\Models\Brand::create([
            'name'=>'not_available'
        ]);
    }
}
