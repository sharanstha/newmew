<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'user_type' => 'admin',
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
            'branch_id'=>1
        ]);
        \App\User::create([
            'user_type' => 'staff',
            'name' => 'staff',
            'email' => 'staff@gmail.com',
            'password' => Hash::make('staff'),
            'branch_id'=>1
        ]);

    }
}
