@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==3)
@section('content')

    <div class="single_page" style="text-align: center">

        <h1><b>Company Name: </b>{{ $vendors->company_name }}</h1>

        <div class="post_commentbox"><span><i

                        class="fa fa-calendar"></i><b>Created_Date: </b>{{ $vendors->created_at }}</span><br><br>

            <span>

                            <b>Website: </b>{!! $vendors->website !!}</span><br><br>

                <span>

                            <b>VAT-Number: </b>{!! $vendors->vat_no !!}</span><br><br>

            <span>

                            <b>Phone-No: </b>{!! $vendors->phone_no !!}</span><br><br>

            <span>

                            <b>Location: </b>{!! $vendors->location !!}</span><br><br>

            <span>

                            <b>Contact-Person-Email: </b>{!! $vendors->contact_person_email !!}</span><br><br>

            <span>

                            <b>Contact-Person-Phone: </b>{!! $vendors->contact_person_phone !!}</span><br><br>

            <span>

                            <b>Notes: </b>{!! $vendors->notes !!}</span><br><br>

        </div>

    </div>

@endsection

@endif
@endfor
@empty
@endforelse

