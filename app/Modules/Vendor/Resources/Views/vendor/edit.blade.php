@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==3)
@section('content')

    <section class="content-header" xmlns="http://www.w3.org/2999/html">
        <h2>
            Edit
        </h2>
    </section>


    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">

            <div class="box-body">
                <div class="box-body">

                    {{ Form::model($vendor, array('method' => 'post', 'route' => array('vendor.update', $vendor->id),'files' => true)) }}

                    <div class="form-group">
                        {!! Form::label('company_name', 'Company_Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('company_name', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('company_name') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('website', 'Website', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('website', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('website') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('vat_no', 'VAT_Number', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('vat_no', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('vat_no') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('phone_no', 'Phone_No', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('phone_no', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('phone_no') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('location', 'Location', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('location', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('location') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('contact_person_email', 'Contact_Person_Email', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('contact_person_email', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('contact_person_email') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('contact_person_phone', 'Contact_Person_Phone', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('contact_person_phone', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('contact_person_phone') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('notes', 'Notes', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('notes', $value = null, ['id'=>'editor','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('notes') }}</span>
                        </div>
                    </div>
                    <br><br><br>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-rounded btn-success btn-raised active"
                            onclick="return confirm('Ready to UPDATE ?');">
                        <b>UPDATE</b>
                    </button>

                    <a href="{{route('vendor.index')}}"
                       class="glyphicon glyphicon-chevron-left"
                       onclick="return confirm('Are you sure ?');">
                        <b>BACK</b></a>
                </div>

                {{ Form::close() }}

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->

        </div>
        </div>
    </section>
@endsection

@endif
@endfor
@empty
@endforelse