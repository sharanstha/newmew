@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==3)
@section('content')
    <body>
    <section class="content">

        <div class="box box-primary ">

            <div class="panel-heading">
                <div class="col-xs-4">
                    <input type="text" class="search form-control" placeholder="search?...">
                </div>
            </div>
            <br><br>

            {!! Form::open(['route' => 'vendor.destroy','method'=>'DELETE','id'=>'formDelete']) !!}
            <div class="box-header with-border">

                @include('flash::message')

                <ul class="icons-list pull-right">

                    <a href="{{route('vendor.create')}}">
                        <button type="button" class="btn btn-primary btn-sm glyphicon glyphicon-plus">
                            <b>CREATE</b>
                        </button>
                    </a>

                    <a href="{{route('vendor.index')}}">
                        <button type="button" class="btn btn-default btn-sm glyphicon glyphicon-repeat">
                            <b>RESET</b>
                        </button>
                    </a>
                    {{--<button type="submit" class="btn btn-danger btn-sm glyphicon glyphicon-remove"--}}
                    {{--onclick="return confirm('Are you sure ?');">--}}
                    {{--<b>DELETE</b>--}}
                    {{--</button>--}}
                </ul>
                <br><br><br>

                <div style="width: auto; height: auto; overflow: scroll;" id="userTbl">

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Check_All</th>
                            <th>ID</th>
                            <th>Company_Name</th>
                            <th>Website</th>
                            <th>VAT_Number</th>
                            <th>Phone_No</th>
                            <th>Location</th>
                            <th>Contact_Person_Email</th>
                            <th>Contact_Person_Phone</th>
                            <th>Notes</th>
                            <th>Created_Date</th>

                        </tr>
                        </thead>

                        <tbody>

                        @forelse($vendor as $vendors)
                            <tr>
                                <td>
                                    <a href="{{ route('vendor.edit', array($vendors->id))}}"
                                       class="btn btn-info btn-xs"><label
                                                class="glyphicon glyphicon-pencil"></label> &nbsp;Edit</a>

                                    <a href="{{ route('vendor.detail', array($vendors->id))}}"
                                       class="btn btn-success btn-xs"><label
                                                class="glyphicon glyphicon-eye-open"></label> &nbsp;View Detail</a>
                                </td>

                                <td>
                                    <div class="pretty p-default">
                                        {!! Form::checkbox('toDelete[]',$vendors->id, false,['class'=>'checkItem']) !!}
                                        <div class="state">
                                            <label></label>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$vendors->id}}

                                <td>{{$vendors->company_name}}

                                <td>{{$vendors->website}}

                                <td>{{$vendors->vat_no}}

                                <td>{{$vendors->phone_no}}

                                <td>{{$vendors->location}}

                                <td>{{$vendors->contact_person_email}}

                                <td>{{$vendors->contact_person_phone}}

                                <td>{!! $vendors->notes !!}

                                <td>{!! $vendors->created_at !!}</td>


                            </tr>
                        @empty
                            <tr>
                                <td colspan="15">
                                    <p class="text-danger text-center"><b>No data found !</b></p>
                                </td>

                            </tr>
                        @endforelse

                        </tbody>

                    </table>

                </div>
                {{ $vendor->links() }}
            </div>
            {!! Form::close() !!}
        </div>
    </section>

    </body>

@endsection

@endif
@endfor
@empty
@endforelse