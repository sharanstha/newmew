@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==3)
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Create
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">

            <div class="box-body">
                <div class="box-body">

                    {{ Form::open(['route'=>'vendor.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) }}

                    <div class="form-group">
                        {!! Form::label('company_name', 'Company-Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('company_name', $value = null, ['placeholder'=>'Company-Name','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('company_name') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('website', 'Website', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('website', $value = null, ['placeholder'=>'Website','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('website') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('vat_no', 'VAT-Number', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('vat_no', $value = null, ['placeholder'=>'VAT-Number','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('vat_no') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone_no', 'Phone_No', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('phone_no', $value = null, ['placeholder'=>'Phone_No','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('phone_no') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('location', 'Location', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('location', $value = null, ['placeholder'=>'Location','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('location') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('primary_contact_no', 'Primary_Contact_No', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('primary_contact_no', $value = null, ['placeholder'=>'Primary_Contact_No','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('primary_contact_no') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('contact_person_email', 'Contact-Person-Email', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('contact_person_email', $value = null, ['placeholder'=>'Contact-Person-Email','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('contact_person_email') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('contact_person_phone', 'Contact-Person-Phone', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('contact_person_phone', $value = null, ['placeholder'=>'Contact-Person-Phone','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('contact_person_phone') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('notes', 'Notes', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('notes', $value = null, ['id'=>'editor','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('notes') }}</span>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" id="btnSIsave" class="btn btn-rounded btn-success btn-raised active"
                                onclick="return confirm('want to SAVE?');"><b>SAVE</b>
                        </button>

                        <a href="{{route('vendor.index')}}"
                           class="glyphicon glyphicon-chevron-left"
                           onclick="return confirm('Are you sure ?');">
                            <b>BACK</b></a>
                    </div>

                {{ Form::close() }}

                <!-- /.box-body -->
                    <div class="box-footer">
                        Footer
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>
        </div>
    </section>
@endsection

@endif
@endfor
@empty
@endforelse