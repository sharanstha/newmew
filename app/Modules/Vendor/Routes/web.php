<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {

    Route::group(['prefix' => 'vendor'], function () {

//INDEX
        Route::get('', ['as' => 'vendor.index', 'uses' => 'VendorController@index']);

//CREATE
        Route::get('create', ['as' => 'vendor.create', 'uses' => 'VendorController@create']);

//STORE
        Route::post('store', ['as' => 'vendor.store', 'uses' => 'VendorController@store']);

//EDIT
        Route::get('edit/{id}',['as' => 'vendor.edit', 'uses' => 'VendorController@edit']);

//UPDATE
        Route::post('edit/{id}',['as' => 'vendor.update', 'uses' => 'VendorController@update']);

//DELETE
        Route::get('delete/{id}',['as' => 'vendor.delete', 'uses' => 'VendorController@delete']);

        Route::delete('delete', ['as' => 'vendor.destroy', 'uses' => 'VendorController@destroy']);

//DETAIL
        Route::get('detail/{id}', ['as' => 'vendor.detail', 'uses' => 'VendorController@detail']);

    });
});