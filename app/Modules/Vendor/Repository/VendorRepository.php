<?php
namespace App\Modules\Vendor\Repository;


use App\Modules\Vendor\Models\Vendor;

class VendorRepository implements VendorInterface
{

    public function findAll($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = Vendor::when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['company_name'])) {
                $query->where('company_name', 'like', '%' . $filter['company_name'] . '%')->get();
            }

            return $query;

        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    /**
     * @param $id
     * @return mixed|static
     */
    public function find($id)
    {
        return Vendor::find($id);
    }

    public function findList()
    {
        return Vendor::find('company_name', null, '---');
    }


    public function save($data)
    {

        $vendor = Vendor::create($data);

        $vendor->save();

    }

    public function update($id, $data)
    {
        $vendor = Vendor::find($id);

        $vendor->update($data);

    }

    public function delete($ids)
    {
        return Vendor::destroy($ids);
    }

}
