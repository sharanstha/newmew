<?php

namespace App\Modules\Vendor\Models;

use App\Modules\StockIn\Models\StockIn;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = [
      'company_name','website','vat_no','phone_no','location','contact_person_email','contact_person_phone','notes'
    ];

    public function stockIn()
    {
        return $this->belongsTo(StockIn::class);
    }
}
