<?php

namespace App\Modules\Vendor\Providers;

use App\Modules\Vendor\Repository\VendorInterface;
use App\Modules\Vendor\Repository\VendorRepository;
use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'vendor');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'vendor');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'vendor');
        $this->loadConfigsFrom(__DIR__.'/../config');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->vendorRegister();
    }

    public function vendorRegister()
    {
        $this->app->bind(VendorInterface::class, VendorRepository::class);
    }
}
