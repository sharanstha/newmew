<?php

namespace App\Modules\Vendor\Http\Controllers;

use App\Modules\Vendor\Http\Requests\VendorFormRequest;
use App\Modules\Vendor\Models\Vendor;
use App\Modules\Vendor\Repository\VendorInterface;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class VendorController extends Controller
{
    protected $vendor;

    public function __construct(VendorInterface $vendor)
    {
        $this->vendor = $vendor;
    }

    public function index(Request $request)
    {
        $filter['company_name'] = $request->get('company_name');

        $limit = $request->get('limit', 100000);

        $data['vendor'] = $this->vendor->findAll($limit, $filter);
        $data['vendor']->appends(['company_name' => $filter['company_name']]);

        return view('vendor::vendor.index', $data);
    }

    public function create()
    {
        $data['vendor'] = $this->vendor->findList();

        return view('vendor::vendor.create', $data);
    }

    public function store(VendorFormRequest $request)
    {

        $input = $request->all();

        try {

            $this->vendor->save($input);

            Flash::success("Successfully Created");

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('vendor.index'));

    }

    public function edit($id)
    {
        $data['vendor'] = $this->vendor->find($id);

        $data['vendors'] = $this->vendor->findList();

        return view('vendor::vendor.edit', $data);

    }

    public function update(VendorFormRequest $request, $id)
    {
        try {
            $input = $request->all();

            $this->vendor->update($id, $input);

            Flash::success("Updated Successfully");

            return redirect(route('vendor.index'));

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());
        }
    }

    public function delete($ids)
    {
        return Vendor::destroy($ids);

    }

    public function destroy(Request $request)
    {
        $ids = $request->all();

        try {

            if ($request->has('toDelete')) {

                Vendor::destroy($ids['toDelete']);

                Flash::success("Successfully Deleted");

            } else {

                Flash::error("Please check at least one to delete");

            }
        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('vendor.index'));
    }

    public function detail($id)
    {
        $data['vendors'] = Vendor::find($id);

        return view('vendor::vendor.detail',$data);
    }
}
