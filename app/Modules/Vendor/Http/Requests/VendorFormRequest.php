<?php

namespace App\Modules\Vendor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VendorFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()){

            case 'POST':
                return [
                    'company_name' => 'required|unique:vendors,company_name',
                    'vat_no' => 'required',
                    'phone_no' => 'required|numeric',
                ];

            case 'PUT':
                return [
                    'company_name' => 'required|unique:vendors,company_name,'.$this->route('id'),
                    'vat_no' => 'required',
                    'phone_no' => 'required|numeric',
                ];
        }
    }
}
