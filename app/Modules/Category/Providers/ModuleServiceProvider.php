<?php

namespace App\Modules\Category\Providers;

use App\Modules\Category\Repository\CategoryInterface;
use App\Modules\Category\Repository\CategoryRepository;
use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'category');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'category');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'category');
        $this->loadConfigsFrom(__DIR__.'/../config');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->categoryRegister();
    }

    public function categoryRegister()
    {
        $this->app->bind(CategoryInterface::class,CategoryRepository::class);
    }
    
}
