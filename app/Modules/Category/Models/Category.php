<?php

namespace App\Modules\Category\Models;

use App\Modules\Stockin\Models\StockIn;
use Baum\Node as Baum;

class Category extends Baum
{
    protected $fillable = [
        'parent_id', 'name','display_unit','description','gender'
    ];

    public function stockIn ()
    {
        return $this->belongsTo(StockIn::class);
    }
}


