<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {

    Route::group(['prefix' => 'category'], function () {
//INDEX
        Route::get('', ['as' => 'category.index', 'uses' => 'CategoryController@index']);

//CREATE
        Route::get('create', ['as' => 'category.create', 'uses' => 'CategoryController@create']);

//STORE
        Route::post('store', ['as' => 'category.store', 'uses' => 'CategoryController@store']);

//EDIT
        Route::get('edit/{id}',['as' => 'category.edit', 'uses' => 'CategoryController@edit']);

//UPDATE
        Route::post('edit/{id}',['as' => 'category.update', 'uses' => 'CategoryController@update']);

//DELETE
        Route::get('delete/{id}',['as' => 'category.delete', 'uses' => 'CategoryController@delete']);

        Route::delete('delete', ['as' => 'category.destroy', 'uses' => 'CategoryController@destroy']);

//DETAIL
        Route::get('detail/{id}', ['as' => 'category.detail', 'uses' => 'CategoryController@detail']);


    });
});
