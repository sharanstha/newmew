@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==4)
@section('content')
    <body>
    <section class="content">

        <div class="box box-primary ">

            <div class="panel-heading">
                <div class="col-xs-4">
                    <input type="text" class="search form-control" placeholder="search?...">
                </div>
            </div><br>

            {!! Form::open(['route' => 'category.destroy','method'=>'DELETE','id'=>'formDelete']) !!}
            <div class="box-header with-border">

                @include('flash::message')

                <ul class="icons-list pull-right">

                    <a href="{{route('category.create')}}">
                        <button type="button" class="btn btn-primary btn-sm glyphicon glyphicon-plus">
                            <b>CREATE</b>
                        </button>
                    </a>

                    <a href="{{route('category.index')}}">
                        <button type="button" class="btn btn-default btn-sm glyphicon glyphicon-repeat">
                            <b>RESET</b>
                        </button>
                    </a>
                    {{--<button type="submit" class="btn btn-danger btn-sm glyphicon glyphicon-remove" onclick="return confirm('Are you sure ?');">--}}
                        {{--<b>DELETE</b>--}}
                    {{--</button>--}}
                </ul><br><br><br>

                <div style="width: auto; height: auto; overflow: scroll;" id="userTbl">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Action</th>
                        <th>Check All</th>
                        <th>ID</th>
                        <th>Gender</th>
                        <th>Name</th>
                        <th>Display_Unit</th>
                        <th>Created_Date</th>
                        <th>Updated_Date</th>

                    </tr>
                    </thead>

                    <tbody>

                    @forelse($categories as $category)
                        <tr>

                            <td>
                                <a href="{{ route('category.edit', array($category->id))}}" class="btn btn-info btn-xs"><label class="glyphicon glyphicon-pencil"></label> &nbsp;Edit</a>
                                <a href="{{ route('category.detail', array($category->id))}}" class="btn btn-success btn-xs"><label class="glyphicon glyphicon-eye-open"></label> &nbsp;View Detail</a>
                            </td>
                            <td>
                                <div class="pretty p-default">
                                    {!! Form::checkbox('toDelete[]',$category->id, false,['class'=>'checkItem']) !!}
                                    <div class="state">
                                        <label></label>
                                    </div>
                                </div>
                            </td>
                            <td>{{$category->id}}</td>

                            <td>{{$category->gender}}</td>

                            <td>{{$category->name}}</td>

                            <td>{{$category->display_unit}}</td>

                            <td>{!! $category->created_at !!}</td>

                            <td>{!! $category->updated_at !!}</td>


                        </tr>
                    @empty
                        <tr>
                            <td colspan="15">
                                <p class="text-danger text-center"><b>No data found !</b></p>
                            </td>

                        </tr>
                    @endforelse

                    </tbody>

                </table>
                    </div>
                {{ $categories->links() }}
            </div>
            {!! Form::close() !!}
        </div>
    </section>

    </body>

@endsection

@endif
@endfor
@empty
@endforelse