@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==4)
@section('content')

    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Edit Category
        </h1>

    </section>


    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">
            <div class="box-header with-border">


                <div class="box-body">
                    <div class="box-body">

                        {{ Form::model($category, array('method' => 'post', 'route' => array('category.update', $category->id))) }}

                        <div class="form-group">
                            {!! Form::label('gender', 'Parent_Category', ['class' => 'col-lg-2 control-label required_label']) !!}
                            <div class="col-lg-8">
                                <div class="col-lg-22.5">
                                    <select class="form-control" name="gender">
                                        <option value="Mens">Mens</option>
                                        <option value="Womens">Womens</option>
                                    </select>
                                </div>
                                <span class="error">{{ $errors->first('gender') }}</span>
                            </div>
                        </div>
                        <br><br><br>

                        <div class="form-group">
                            {!! Form::label('parent_id', 'Category', ['class' => 'col-lg-2 control-label required_label']) !!}
                            <div class="col-lg-8">
                                {!! Form::select('parent_id',$categories,null,['class'=>'form-control select','placeholder'=>'Select Category']) !!}
                                <span class="error">{{ $errors->first('parent_id') }}</span>
                            </div>
                        </div>
                        <br><br><br>

                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                            <div class="col-lg-8">
                                {!! Form::text('name', $value = null, ['class'=>'form-control']) !!}
                                <span class="error">{{ $errors->first('name') }}</span>
                            </div>
                        </div>
                        <br><br><br>

                        <div class="form-group">
                            {!! Form::label('display_unit', 'Display_unit', ['class' => 'col-lg-2 control-label required_label']) !!}
                            <div class="col-lg-8">
                                {!! Form::text('display_unit', $value = null, ['class'=>'form-control']) !!}
                                <span class="error">{{ $errors->first('display_unit') }}</span>
                            </div>
                        </div>
                        <br><br><br>

                        <div class="form-group">
                            {!! Form::label('description', 'Description', ['class' => 'col-lg-2 control-label required_label']) !!}
                            <div class="col-lg-8">
                                {!! Form::text('description', $value = null, ['class'=>'form-control']) !!}
                                <span class="error">{{ $errors->first('description') }}</span>
                            </div>
                        </div>
                        <br><br><br>

                        <div class="text-right">
                            <button type="submit" class="btn btn-rounded btn-success btn-raised active"
                                    onclick="return confirm('Ready to UPDATE ?');">
                                <b>UPDATE</b>
                            </button>

                            <a href="{{route('category.index')}}"
                               class="glyphicon glyphicon-chevron-left"
                               onclick="return confirm('Are you sure ?');">
                                <b>BACK</b></a>
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>


                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->

            </div>
        </div>
    </section>

@endsection

@endif
@endfor
@empty
@endforelse