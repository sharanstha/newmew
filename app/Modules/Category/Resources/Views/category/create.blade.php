@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==4)
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Create New Category
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">
            <div class="box-body">
                <div class="box-body">

                    {{ Form::open(['route'=>'category.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) }}

                    <div class="form-group">
                        {!! Form::label('gender', 'Parent Category', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            <select class="form-control" name="gender">
                                <option value="Mens">Mens</option>
                                <option value="Womens">Womens</option>
                            </select>
                            <span class="error">{{ $errors->first('gender') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('category', 'Category', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::select('parent_id',$categories,null,['class'=>'form-control select','placeholder'=>'Select Category']) !!}
                            <span class="error">{{ $errors->first('parent_id') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('name', $value = null, ['placeholder'=>'Name','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('name') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('display_unit', 'Display-unit', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('display_unit', $value = null, ['placeholder'=>'Display-unit','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('display_unit') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Description', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('description', $value = null, ['id'=>'editor','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('description') }}</span>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" id="btnSIsave" class="btn btn-rounded btn-success btn-raised active"
                                onclick="return confirm('want to SAVE?');"><b>SAVE</b>
                        </button>

                        <a href="{{route('category.index')}}"
                           class="glyphicon glyphicon-chevron-left"
                           onclick="return confirm('Are you sure ?');">
                            <b>BACK</b></a>
                    </div>

                {{ Form::close() }}

                <!-- /.box-body -->
                    <div class="box-footer">
                        Footer
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>
        </div>
    </section>
@endsection

@endif
@endfor
@empty
@endforelse