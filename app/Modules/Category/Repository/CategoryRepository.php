<?php
namespace App\Modules\Category\Repository;


use App\Modules\Category\Models\Category;
use Illuminate\Support\Facades\Auth;

class CategoryRepository implements CategoryInterface
{

    public function findAll($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = Category::when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['name'])) {
                $query->where('name', 'like', '%' . $filter['name'] . '%')->get();
            }

            return $query;
        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    /**
     * @param $id
     * @return mixed|static
     */
    public function find($id)
    {
        return Category::find($id);
    }

    public function findList()
    {
        return Category::getNestedList('name', null, '---');
    }

    public function save($data)
    {
        $data['status'] = 1;
        $data['created_by_id'] = Auth::user()->id;
        $category = Category::create($data);
        $node = Category::find($category->id);

        if (isset($data['parent_id'])) {
            $root = Category::find($data['parent_id']);
            $node->makeChildOf($root);

        } else {
            $category->makeRoot();
        }

        return Category::rebuild(true);

    }

    public function update($id, $data)
    {
        $category = Category::find($id);

        if ($data['parent_id'] != 0) {

            $root = Category::find($data['parent_id']);
            $category->makeChildOf($root);

        } else {
            $category->makeRoot();
        }

        $category->update($data);

        return Category::rebuild(true);
    }

    public function delete($ids)
    {
        return Category::destroy($ids);
    }

    public function findUnit($id)
    {
        $data = Category::where('id', '=', $id)->get();
        foreach ($data as $row) {
            return $row->display_unit;
        }
    }

}
