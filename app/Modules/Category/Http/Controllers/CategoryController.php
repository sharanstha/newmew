<?php

namespace App\Modules\Category\Http\Controllers;

use App\Modules\Category\Http\Requests\CategoryFormRequest;
use App\Modules\Category\Models\Category;
use App\Modules\Category\Repository\CategoryInterface;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(CategoryInterface $category)
    {
        $this->category = $category;
    }

    public function index(Request $request)
    {
        $filter['name'] = $request->get('name');

        $limit = $request->get('limit', 100000);

        $data['categories'] = $this->category->findAll($limit, $filter);

        $data['categories']->appends(['name' => $filter['name']]);

        return view('category::category.index', $data);
    }

    public function create()
    {
        
        $data['categories'] = $this->category->findList();

        return view('category::category.create', $data);
    }

    public function store(CategoryFormRequest $request)
    {
        $input = $request->all();

        try {

            $this->category->save($input);

            Flash::success("Data Created Successfully");

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('category.index'));

    }

    public function edit($id)
    {

        $data['category'] = $this->category->find($id);

        $data['categories'] = $this->category->findList();

        return view('category::category.edit', $data);
        
    }

    public function update(CategoryFormRequest $request, $id)
    {
        try {
            $input = $request->all();

            $this->category->update($id, $input);


            Flash::success("Data Updated Successfully");

            return redirect(route('category.index'));

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());
        }
    }

    public function delete($ids)
    {
        return Category::destroy($ids);

    }

    public function destroy(Request $request)
    {
        $ids = $request->all();

        try {

            if ($request->has('toDelete')) {

                Category::destroy($ids['toDelete']);

                Flash::success("Successfully Deleted");

            } else {

                Flash::error("Please check at least one to delete");

            }
        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('category.index'));
    }

    public function detail($id)
    {
        $data['categories'] = Category::find($id);

        return view('category::category.detail', $data);
    }
    
}
