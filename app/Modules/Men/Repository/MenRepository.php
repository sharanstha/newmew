<?php
namespace App\Modules\Men\Repository;


use App\Modules\Men\Models\Men;

class MenRepository implements MenInterface
{

    public function findAll($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = Men::when(array_keys($filter, true), function ($query) use ($filter) {
            $query->where('id', 'like', '%' . $filter['id'] . '%');

            return $query;
        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    /**
     * @param $id
     * @return mixed|static
     */
    public function find($id)
    {
        return Men::find($id);
    }

    public function findList()
    {
        return Men::find('id', null, '---');
    }


    public function save($data)
    {

        $men = Men::create($data);

        $men->save();

    }

    public function update($id, $data)
    {
        $men = Men::find($id);

        $men->update($data);

    }

    public function delete($ids)
    {
        return Men::destroy($ids);
    }

}
