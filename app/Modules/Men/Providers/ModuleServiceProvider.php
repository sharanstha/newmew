<?php

namespace App\Modules\Men\Providers;

use App\Modules\Men\Repository\MenInterface;
use App\Modules\Men\Repository\MenRepository;
use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'men');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'men');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'men');
        $this->loadConfigsFrom(__DIR__.'/../config');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->menRegister();
    }

    public function menRegister()
    {
        $this->app->bind(MenInterface::class,MenRepository::class);
    }
}
