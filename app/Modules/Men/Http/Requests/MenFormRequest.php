<?php

namespace App\Modules\Men\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()){
            case 'POST':
                return[
                    'item_id' => 'required|unique:mens',
                ];

            case 'PUT':
                return[
                    'item_id' => 'required|unique:mens,name,' . $this->route('id'),
                ];
            default:
                break;
        }
    }
}
