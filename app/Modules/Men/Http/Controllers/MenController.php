<?php

namespace App\Modules\Men\Http\Controllers;

use App\Modules\Item\Models\Item;
use App\Modules\Item\Repository\ItemInterface;
use App\Modules\Men\Http\Requests\MenFormRequest;
use App\Modules\Men\Models\Men;
use App\Modules\Men\Repository\MenInterface;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class MenController extends Controller
{
    protected $men;

    protected $item;

    public function __construct(MenInterface $men, ItemInterface $item)
    {
        $this->men = $men;

        $this->item = $item;
    }

    public function index(Request $request)
    {
        $filter['id'] = $request->get('id');

        $limit = $request->get('limit', 100000);

        $data['men'] = $this->men->findAll($limit, $filter);
        $data['men']->appends(['id' => $filter['id']]);

        $data['items'] = $this->item->findAll();

        return view('men::men.index', $data);
    }

    public function create()
    {
        $data['items'] = Item::pluck('name', 'id');

        $data['item'] = $this->item->findAll();

        $data['men'] = $this->men->findList();

        return view('men::men.create', $data);
    }

    public function store(MenFormRequest $request)
    {

        $input = $request->all();

        try {

            $this->men->save($input);

            Flash::success("Data Created Successfully");

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('men.index'));

    }

    public function edit($id)
    {
        $data['men'] = $this->men->find($id);

        $data['mens'] = $this->men->findList();

        $data['items'] = Item::pluck('name', 'id');

        return view('men::men.edit', $data);

    }

    public function update(MenFormRequest $request, $id)
    {
        try {
            $input = $request->all();

            $this->men->update($id, $input);

            Flash::success("Data Updated Successfully");

            return redirect(route('men.index'));

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());
        }
    }

    public function delete($ids)
    {
        return Men::destroy($ids);

    }

    public function destroy(Request $request)
    {
        $ids = $request->all();

        try {

            if ($request->has('toDelete')) {

                Men::destroy($ids['toDelete']);

                Flash::success("Successfully Deleted");

            } else {

                Flash::error("Please check at least one to delete");

            }
        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('men.index'));
    }

    public function detail($id)
    {
        $data['mens'] = Men::find($id);

        return view('men::men.detail',$data);
    }

}
