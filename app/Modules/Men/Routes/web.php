<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {

    Route::group(['prefix' => 'men'], function () {

//INDEX        
        Route::get('', ['as' => 'men.index', 'uses' => 'MenController@index']);

//CREATE
        Route::get('create', ['as' => 'men.create', 'uses' => 'MenController@create']);

//STORE
        Route::post('store', ['as' => 'men.store', 'uses' => 'MenController@store']);

//EDIT
        Route::get('edit/{id}',['as' => 'men.edit', 'uses' => 'MenController@edit']);

//UPDATE
        Route::post('edit/{id}',['as' => 'men.update', 'uses' => 'MenController@update']);

//DELETE
        Route::get('delete/{id}',['as' => 'men.delete', 'uses' => 'MenController@delete']);

        Route::delete('delete', ['as' => 'men.destroy', 'uses' => 'MenController@destroy']);

//DETAIL
        Route::get('detail/{id}', ['as' => 'men.detail', 'uses' => 'MenController@detail']);


    });
});


