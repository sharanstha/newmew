@extends("admin::layout")
@section('content')
    <body>
    <section class="content">

        <div class="box box-primary ">

            <div class="panel-heading">
                <div class="col-xs-4">
                    <form action="{{ route('men.index') }}" method="GET">
                        <div class="form-group input-group">
                            <input type="text" name="name" class="form-control" placeholder="Search By Item" required/>
                            <label class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-sm" style="margin-left: 10px">
                                    <i class="fa fa-search"></i>
                                    Find
                                </button>
                            </label>
                        </div>
                    </form>
                </div>
            </div>
            <br><br>

            {!! Form::open(['route' => 'men.destroy','method'=>'DELETE','id'=>'formDelete']) !!}
            <div class="box-header with-border">

                @include('flash::message')

                <ul class="icons-list pull-right">

                    <a href="{{route('men.create')}}">
                        <button type="button" class="btn btn-primary btn-sm glyphicon glyphicon-plus">
                            <b>CREATE</b>
                        </button>
                    </a>

                    <a href="{{route('men.index')}}">
                        <button type="button" class="btn btn-default btn-sm glyphicon glyphicon-repeat">
                            <b>RESET</b>
                        </button>
                    </a>
                    {{--<button type="submit" class="btn btn-danger btn-sm glyphicon glyphicon-remove"--}}
                            {{--onclick="return confirm('Are you sure ?');">--}}
                        {{--<b>DELETE</b>--}}
                    {{--</button>--}}
                </ul>
                <br><br><br>

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Check All</th>
                        <th>ID</th>
                        <th>Item</th>
                        <th>Created_Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>

                    @forelse($men as $mens)
                        <tr>
                            <td>
                                <div class="pretty p-default">
                                    {!! Form::checkbox('toDelete[]',$mens->id, false,['class'=>'checkItem']) !!}
                                    <div class="state">
                                        <label></label>
                                    </div>
                                </div>
                            </td>
                            <td>{{$mens->id}}

                            <td>{{$mens->item->name}}

                            <td>{!! $mens->created_at !!}</td>

                            <td>
                                <a href="{{ route('men.edit', array($mens->id))}}" class="btn btn-info btn-xs"><label
                                            class="glyphicon glyphicon-pencil"></label> &nbsp;Edit</a>

                                <a href="{{ route('item.detail', array($mens->id))}}"
                                   class="btn btn-success btn-xs"><label
                                            class="glyphicon glyphicon-eye-open"></label> &nbsp;View Detail</a>
                            </td>

                        </tr>
                    @empty
                        <tr>
                            <td colspan="15">
                                <p class="text-danger text-center"><b>No data found !</b></p>
                            </td>

                        </tr>
                    @endforelse

                    </tbody>

                </table>
                {{ $men->links() }}
            </div>
            {!! Form::close() !!}
        </div>
    </section>

    </body>

@endsection