<?php

namespace App\Modules\Men\Models;

use App\Modules\Stockin\Models\StockIn;
use Illuminate\Database\Eloquent\Model;

class Men extends Model
{
    protected $fillable = [
        'item_id'
    ];

    public function stockIn()
    {
        return $this->belongsTo(StockIn::class);
    }
}
