@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==5)
@section('content')
    <body>
    <section class="content">

        <div class="box box-primary ">

            <div class="panel-heading">
                <div class="col-xs-4">
                    <input type="text" class="search form-control" placeholder="search?...">
                </div>
            </div>
            <br>

            {!! Form::open(['route' => 'brand.destroy','method'=>'DELETE','id'=>'formDelete']) !!}
            <div class="box-header with-border">

                @include('flash::message')

                <ul class="icons-list pull-right">

                    <a href="{{route('brand.create')}}">
                        <button type="button" class="btn btn-primary btn-sm glyphicon glyphicon-plus">
                            <b>CREATE</b>
                        </button>
                    </a>

                    <a href="{{route('brand.index')}}">
                        <button type="button" class="btn btn-default btn-sm glyphicon glyphicon-repeat">
                            <b>RESET</b>
                        </button>
                    </a>
                    {{--<button type="submit" class="btn btn-danger btn-sm glyphicon glyphicon-remove" onclick="return confirm('Are you sure ?');">--}}
                    {{--<b>DELETE</b>--}}
                    {{--</button>--}}
                </ul>
                <br><br><br>

                <div style="width: auto; height: auto; overflow: scroll;" id="userTbl">

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Check All</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Created_Date</th>
                            <th>Updated_Date</th>

                        </tr>
                        </thead>

                        <tbody>

                        @forelse($brand as $brands)
                            @if($brands->id<>1)
                                <tr>
                                    <td>
                                        <a href="{{ route('brand.edit', array($brands->id))}}"
                                           class="btn btn-info btn-xs"><label
                                                    class="glyphicon glyphicon-pencil"></label> &nbsp;Edit</a>
                                        <a href="{{ route('brand.detail', array($brands->id))}}"
                                           class="btn btn-success btn-xs"><label
                                                    class="glyphicon glyphicon-eye-open"></label> &nbsp;View Detail</a>
                                    </td>
                                    <td>
                                        <div class="pretty p-default">
                                            {!! Form::checkbox('toDelete[]',$brands->id, false,['class'=>'checkItem']) !!}
                                            <div class="state">
                                                <label></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{$brands->id}}</td>

                                    <td>{{$brands->name}}</td>

                                    <td>{!! $brands->created_at !!}</td>

                                    <td>{!! $brands->updated_at !!}</td>



                                </tr>
                            @endif
                        @empty
                            <tr>
                                <td colspan="15">
                                    <p class="text-danger text-center"><b>No data found !</b></p>
                                </td>

                            </tr>
                        @endforelse

                        </tbody>

                    </table>
                </div>
                {{ $brand->links() }}
            </div>
            {!! Form::close() !!}
        </div>
    </section>

    </body>

@endsection

@endif
@endfor
@empty
@endforelse