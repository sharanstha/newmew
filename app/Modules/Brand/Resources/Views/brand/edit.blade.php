@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==5)
@section('content')

    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Edit Brand
        </h1>

    </section>


    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">
            <div class="box-header with-border">

                <div class="box-body">

                        {{ Form::model($brands, array('method' => 'post', 'route' => array('brand.update', $brands->id))) }}


                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'col-lg-1 control-label required_label']) !!}
                            <div class="col-lg-8">
                                {!! Form::text('name', $value = null, ['class'=>'form-control']) !!}
                                <span class="error">{{ $errors->first('name') }}</span>
                            </div>
                        </div>
                        <br><br><br>

                        <div class="form-group">
                            {!! Form::label('detail', 'Detail', ['class' => 'col-lg-1 control-label required_label']) !!}
                            <div class="col-lg-8">
                                {!! Form::text('detail', $value = null, ['class'=>'form-control']) !!}
                                <span class="error">{{ $errors->first('detail') }}</span>
                            </div>
                        </div>
                        <br><br><br>

                        <div class="text-right">
                            <button type="submit" class="btn btn-rounded btn-success btn-raised active"
                                    onclick="return confirm('Ready to UPDATE ?');">
                                <b>UPDATE</b>
                            </button>

                            <a href="{{route('brand.index')}}"
                               class="glyphicon glyphicon-chevron-left"
                               onclick="return confirm('Are you sure ?');">
                                <b>BACK</b></a>
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>


                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->

            </div>
        </div>
    </section>

@endsection

@endif
@endfor
@empty
@endforelse

