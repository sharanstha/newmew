@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==5)
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Create New Brand
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">
            <div class="box-body">

                {{ Form::open(['route'=>'brand.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) }}


                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('name', $value = null, ['placeholder'=>'Name','class'=>'form-control']) !!}
                        <span class="error">{{ $errors->first('name') }}</span>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('detail', 'Detail', ['class' => 'col-lg-2 control-label required_label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('detail', $value = null, ['id'=>'editor','class'=>'form-control']) !!}
                        <span class="error">{{ $errors->first('detail') }}</span>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" id="btnSIsave" class="btn btn-rounded btn-success btn-raised active"
                            onclick="return confirm('want to SAVE?');"><b>SAVE</b>
                    </button>

                    <a href="{{route('brand.index')}}"
                       class="glyphicon glyphicon-chevron-left"
                       onclick="return confirm('Are you sure ?');">
                        <b>BACK</b></a>
                </div>

            {{ Form::close() }}

            <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
    </section>
@endsection

@endif
@endfor
@empty
@endforelse