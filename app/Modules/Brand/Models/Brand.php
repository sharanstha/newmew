<?php

namespace App\Modules\Brand\Models;

use App\Modules\Category\Models\Category;
use App\Modules\StockIn\Models\StockIn;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = [
        'name', 'detail'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function stockIn()
    {
        return $this->belongsTo(StockIn::class);
    }
}