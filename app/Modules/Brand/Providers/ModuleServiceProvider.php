<?php

namespace App\Modules\Brand\Providers;

use App\Modules\Brand\Repository\BrandInterface;
use App\Modules\Brand\Repository\BrandRepository;
use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'brand');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'brand');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'brand');
        $this->loadConfigsFrom(__DIR__.'/../config');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->brandRegister();
    }

    public function brandRegister()
    {
        $this->app->bind(BrandInterface::class,BrandRepository::class);
    }
}
