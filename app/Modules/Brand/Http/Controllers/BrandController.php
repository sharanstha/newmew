<?php

namespace App\Modules\Brand\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Brand\Http\Requests\BrandFormRequest;
use App\Modules\Brand\Models\Brand;
use App\Modules\Brand\Repository\BrandInterface;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class BrandController extends Controller
{
    protected $brand;

    public function __construct(BrandInterface $brand)
    {
        $this->brand = $brand;
    }

    public function index(Request $request)
    {
        $filter['name'] = $request->get('name');

        $limit = $request->get('limit', 100000);

        $brand = $this->brand->findAll($limit, $filter);

        $brand->appends(['name' => $filter['name']]);

        return view('brand::brand.index', compact('brand'));
    }

    public function create()
    {

        return view('brand::brand.create');
        
    }

    public function store(BrandFormRequest $request)
    {

        $input = $request->all();

        try {

            $this->brand->save($input);

            Flash::success("Data Created Successfully");

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('brand.index'));

    }

    public function edit($id)
    {

        $brands = $this->brand->find($id);

        return view('brand::brand.edit', compact('brands'));
    }

    public function update(BrandFormRequest $request, $id)
    {
        try {
            $input = $request->all();

            $this->brand->update($id, $input);

            Flash::success("Data Updated Successfully");

            return redirect(route('brand.index'));

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());
        }
    }

    public function delete($ids)
    {
        return Brand::destroy($ids);

    }

    public function destroy(Request $request)
    {
        $ids = $request->all();

        try {

            if ($request->has('toDelete')) {

                Brand::destroy($ids['toDelete']);

                Flash::success("Successfully Deleted");

            } else {

                Flash::error("Please check at least one to delete");

            }
        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('brand.index'));
    }

    public function detail($id)
    {
        $data['brands'] = Brand::find($id);
        
        return view('brand::brand.detail',$data);
    }
}
