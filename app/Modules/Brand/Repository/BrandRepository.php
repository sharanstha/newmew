<?php
namespace App\Modules\Brand\Repository;



use App\Modules\Brand\Models\Brand;

class BrandRepository implements BrandInterface
{

    public function findAll($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = Brand::when(array_keys($filter, true), function ($query) use ($filter) {
            $query->where('name', 'like', '%' . $filter['name'] . '%');

            return $query;
        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    /**
     * @param $id
     * @return mixed|static
     */
    public function find($id)
    {
        return Brand::find($id);
    }


    public function save($data)
    {

        $brand = Brand::create($data);

        $brand->save();

    }

    public function update($id, $data)
    {
        $brand = Brand::find($id);

        $brand->update($data);

    }

    public function delete($ids)
    {
        return Brand::destroy($ids);
    }

}
