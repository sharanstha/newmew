<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {

    Route::group(['prefix' => 'brand'], function () {
        
//INDEX        
        Route::get('', ['as' => 'brand.index', 'uses' => 'BrandController@index']);

//CREATE
        Route::get('create', ['as' => 'brand.create', 'uses' => 'BrandController@create']);

//STORE
        Route::post('store', ['as' => 'brand.store', 'uses' => 'BrandController@store']);

//EDIT
        Route::get('edit/{id}',['as' => 'brand.edit', 'uses' => 'BrandController@edit']);

//UPDATE
        Route::post('edit/{id}',['as' => 'brand.update', 'uses' => 'BrandController@update']);

//DELETE
        Route::get('delete/{id}',['as' => 'brand.delete', 'uses' => 'BrandController@delete']);

        Route::delete('delete', ['as' => 'brand.destroy', 'uses' => 'BrandController@destroy']);

//DETAIL
        Route::get('detail/{id}', ['as' => 'brand.detail', 'uses' => 'BrandController@detail']);


    });
});


