<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {

    Route::group(['prefix' => 'permission'], function () {

        //INDEX
        Route::get('', ['as' => 'permission.index', 'uses' => 'PermissionController@index']);

        //CREATE
        Route::get('create', ['as' => 'permission.create', 'uses' => 'PermissionController@create']);

        Route::get('branchcreate', ['as' => 'branchpermission.create', 'uses' => 'BranchPermissionController@create']);

        Route::get('branchusercreate', ['as' => 'branchuserpermission.usercreate', 'uses' => 'BranchPermissionController@usercreate']);

        //GRANT
        Route::get('grant/{id}', ['as' => 'permission.grant', 'uses' => 'PermissionController@grant']);
        Route::get('grantAll', ['as' => 'permission.grantAll', 'uses' => 'PermissionController@grantAll']);

        Route::get('branchgrant/{id}', ['as' => 'branchpermission.grant', 'uses' => 'BranchPermissionController@grant']);
        Route::get('branchgrantAll', ['as' => 'branchpermission.grantAll', 'uses' => 'BranchPermissionController@grantAll']);

        Route::get('branchusergrant/{id}', ['as' => 'branchuserpermission.grant', 'uses' => 'BranchPermissionController@usergrant']);
        Route::get('branchusergrantAll', ['as' => 'branchuserpermission.grantAll', 'uses' => 'BranchPermissionController@usergrantAll']);

        //REVOKE
        Route::get('revoke/{id}', ['as' => 'permission.revoke', 'uses' => 'PermissionController@revoke']);
        Route::get('revokeAll', ['as' => 'permission.revokeAll', 'uses' => 'PermissionController@revokeAll']);


        Route::get('branchrevoke/{id}', ['as' => 'branchpermission.revoke', 'uses' => 'BranchPermissionController@revoke']);
        Route::get('branchrevokeAll', ['as' => 'branchpermission.revokeAll', 'uses' => 'BranchPermissionController@revokeAll']);

        Route::get('branchuserrevoke/{id}', ['as' => 'branchuserpermission.revoke', 'uses' => 'BranchPermissionController@userrevoke']);

    });
});

