@extends("admin::layout")
@forelse ($permissions as $permission)
    @for($i=0;$i<$forlastid;$i++)
        @if(Session::get('sess_staff_id')== $permission->staff_id && $permission->action_id==9)
            {{--sess_staff_id is id of currently login user--}}
@section('grantRevoke')
    <script>

        @forelse($actions as $action)
        {{--$("#grant{{$action->id}}").show();--}}
        $("#revoke{{$action->id}}").hide();

        @forelse($permissions as $permission)
        @if($permission->permission_code== (Session::get('sess_userid').$action ->id))
        $("#grant{{$action->id}}").hide();
        $("#revoke{{$action->id}}").show();
        $("#grantall").hide();
        @endif

        @empty
        @endforelse

        @empty
        @endforelse

        $('.selectall').click(function () {
            $('.checkItem').attr('checked', true);
        });
    </script>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html"></section>
    <!-- Main content -->
    <section class="content">
    {{--section to display recently created data--}}
    <!-- Default box -->
        @include('flash::message')

        {!! Form::open(['route' => 'permission.grantAll','method'=>'get','id'=>'']) !!}

        <div class="box box-primary">
            <div class="box-body">
                <div class="box-body">

                    <ul class="icons-list pull-right">

                        <button style="margin-right:200px;" type="submit"
                                class="btn btn-info btn-xs glyphicon glyphicon-pencil"
                                onclick="return confirm('Are you sure ?');">
                            <b>GRANT CHECKED ACTIONS</b>
                        </button>

                        <a onclick="return confirm('Do you want to revoke permission?');"
                           href="{{ route('permission.revokeAll')}}"
                           class="btn btn-success btn-xs"><label
                                    class="glyphicon glyphicon-eye-open"></label>REVOKE ALL</a>



                        <button type="submit" id="grantall"
                                class="btn btn-info btn-xs glyphicon glyphicon-pencil selectall"
                                onclick="return confirm('Are you sure ?');">
                            <b>GRANT ALL</b>
                        </button>
                    </ul>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Term</th>
                            <th>Check_All</th>
                            <th>Action</th>
                            <th>Created_Date</th>
                            <th>Updated_Date</th>
                        </tr>
                        </thead>

                        <tbody>

                        @forelse($actions as $action)
                            <tr>
                                <td>{{$action->id}}</td>
                                <td>{{$action->action}}</td>

                                <td>
                                    <div class="pretty p-default">
                                        {!! Form::checkbox('toGrant[]',$action->id, false,['class'=>'checkItem']) !!}
                                        <div class="state">
                                            <label></label>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{--{{ route('stockout.edit', array($action->id))}}--}}


                                    <a id="revoke{{$action->id}}"
                                       onclick="return confirm('Do you want to revoke permission?');"
                                       href="{{ route('permission.revoke', array($action->id))}}"
                                       class="btn btn-success btn-xs"><label
                                                class="glyphicon glyphicon-eye-open"></label>revoke</a>

                                    <a id="grant{{$action->id}}"
                                       onclick="return confirm('Do you want to grant permission?');"
                                       href="{{ route('permission.grant', array($action->id))}}"
                                       class="btn btn-info btn-xs"><label
                                                class="glyphicon glyphicon-pencil"></label> grant</a>
                                </td>

                                <td>{{$action->created_at}}</td>
                                <td>{{$action->updated_at}}</td>


                            </tr>
                        @empty
                            <tr>
                                <td colspan="15">
                                    <p class="text-danger text-center"><b>No data found !</b></p>
                                </td>

                            </tr>
                        @endforelse

                        <div>
                            <a href="{{route('user.index')}}"
                               class="glyphicon glyphicon-chevron-left"
                               onclick="return confirm('Are you sure ?');">
                                <b>BACK</b></a>

                        </div>

                        <br>

                    </table>
                    <br><br>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        {{--Footer--}}
                    </div>
                    <!-- /.box-footer-->
                    </tbody>

                </div>
            </div>
        </div>
        {{ Form::close() }}

    </section>

@endsection

@endif
@endfor
@empty
@endforelse
