@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==14)
            {{--sess_staff_id is id of currently login user--}}
@section('grantRevoke')
    <script>


        @forelse($branchactions as $branchaction)

        $("#revoke{{$branchaction->id}}").hide();

        @forelse($branchpermissions as $branchpermission)
        @if($branchpermission->permission_code== (Session::get('sess_branchid').$branchaction->id))
        $("#grant{{$branchaction->id}}").hide();
        $("#revoke{{$branchaction->id}}").show();
        $("#grantall").hide();

        @endif

        @empty
        @endforelse

        @empty
        @endforelse

        $('.selectall').click(function () {
            $('.checkItem').attr('checked', true);
        });
    </script>
@stop
@section('content')

    <!-- Content Header (Page header) -->

    <section class="content-header" xmlns="http://www.w3.org/1999/html"></section>
    <!-- Main content -->
    <section class="content">
    {{--section to display recently created data--}}
    <!-- Default box -->
        @include('flash::message')

        {!! Form::open(['route' => 'branchpermission.grantAll','method'=>'get','id'=>'']) !!}

        <div class="box box-primary">
            <div class="box-body">
                <div class="box-body">
                    <ul class="icons-list pull-right">

                        <button style="margin-right:200px;" type="submit"
                                class="btn btn-info btn-xs glyphicon glyphicon-pencil"
                                onclick="return confirm('Are you sure ?');">
                            <b>GRANT CHECKED ACTIONS</b>
                        </button>

                        <a onclick="return confirm('Do you want to revoke permission?');"
                           href="{{ route('branchpermission.revokeAll')}}"
                           class="btn btn-success btn-xs"><label
                                    class="glyphicon glyphicon-eye-open"></label>REVOKE ALL</a>


                        <button type="submit" id="grantall"
                                class="btn btn-info btn-xs glyphicon glyphicon-pencil selectall"
                                onclick="return confirm('Are you sure ?');">
                            <b>GRANT ALL</b>
                        </button>
                    </ul>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Term</th>
                            <th>Check_All</th>
                            <th>Action</th>
                            <th>Created_Date</th>
                            <th>Updated_Date</th>

                        </tr>
                        </thead>

                        <tbody>

                        @forelse($branchactions as $branchaction)
                            <tr>
                                <td>{{$branchaction->id}}</td>
                                <td>{{$branchaction->action}}</td>
                                <td>
                                    <div class="pretty p-default">
                                        {!! Form::checkbox('toGrant[]',$branchaction->id, false,['class'=>'checkItem']) !!}
                                        <div class="state">
                                            <label></label>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{--{{ route('stockout.edit', array($action->id))}}--}}


                                    <a id="revoke{{$branchaction->id}}"
                                       onclick="return confirm('Do you want to revoke permission?');"
                                       href="{{ route('branchpermission.revoke', array($branchaction->id))}}"
                                       class="btn btn-success btn-xs"><label
                                                class="glyphicon glyphicon-eye-open"></label>revoke</a>

                                    <a id="grant{{$branchaction->id}}"
                                       onclick="return confirm('Do you want to grant permission?');"
                                       href="{{ route('branchpermission.grant', array($branchaction->id))}}"
                                       class="btn btn-info btn-xs"><label
                                                class="glyphicon glyphicon-pencil"></label> grant</a>
                                </td>
                                <td>{{$branchaction->created_at}}</td>
                                <td>{{$branchaction->updated_at}}</td>

                            </tr>
                        @empty
                            <tr>
                                <td colspan="15">
                                    <p class="text-danger text-center"><b>No data found !</b></p>
                                </td>

                            </tr>
                        @endforelse

                        <div>
                            <a href="{{route('branch.index')}}"
                               class="glyphicon glyphicon-chevron-left"
                               onclick="return confirm('Are you sure ?');">
                                <b>BACK</b></a>
                        </div>
                        <br>
                        {{ Form::close() }}

                    </table>
                    <br><br>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        {{--Footer--}}
                    </div>
                    <!-- /.box-footer-->
                    </tbody>

                </div>
            </div>
        </div>
    </section>
    {{ Form::close() }}

@endsection

@endif
@endfor
@empty
@endforelse
