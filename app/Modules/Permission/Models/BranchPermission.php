<?php

namespace App\Modules\Permission\Models;

use Illuminate\Database\Eloquent\Model;

class BranchPermission extends Model
{
    protected $table = 'branchpermissions';
    protected $fillable = [
        'permission_code',
        'staff_id',
        'action_id'
    ];
}
