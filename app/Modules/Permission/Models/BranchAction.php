<?php

namespace App\Modules\Permission\Models;

use Illuminate\Database\Eloquent\Model;

class BranchAction extends Model
{
    protected $table = 'branchactions';

    protected $fillable = [
        'action'
    ];
}
