<?php

namespace App\Modules\Permission\Models;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = [
        'action'
    ];
}
