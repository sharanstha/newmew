<?php

namespace App\Modules\Permission\Models;

use Illuminate\Database\Eloquent\Model;

class BranchUserPermission extends Model
{
    protected $table = 'branchuserpermissions';
    protected $fillable = [
        'permission_code',
        'staff_id',
        'action_id'
    ];
}
