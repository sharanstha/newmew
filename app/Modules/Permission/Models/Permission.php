<?php

namespace App\Modules\Permission\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'permission_code',
        'staff_id',
        'action_id'
    ];

    protected $table ='permissions';
}
