<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchuserpermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branchuserpermissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('permission_code')->unique();
            $table->unsignedInteger('staff_id');
            $table->foreign('staff_id')->references('id')->on('branchusers')->onDelete('cascade');
            //this staff_id == staff id of branch
            //if i used column name branch id then there will be lots of changes in other pages so that
            //i have used column name staff_id
            $table->integer('action_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branchuserpermissions');
    }
}
