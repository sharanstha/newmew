@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==7)
@section('content')
    <body>
    <section class="content">

        <div class="box box-primary">

            <div class="panel-heading">
                {{--<input type="text" class="search form-control" placeholder="search?..." style="width:350px;float:left;margin-right: 50px;">--}}
                <input type="text" class="search form-control" placeholder="search?..." style="width:350px;">
                <form action="{{ route('stockout.index') }}" method="GET"></form>
            </div>

            {!! Form::open(['route' => 'stockout.destroy','method'=>'DELETE','id'=>'formDelete']) !!}
            <div class="box-header with-border">

                @include('flash::message')

                <ul class="icons-list pull-right">

                    <a href="{{route('stockout.create')}}">
                        <button type="button" class="btn btn-primary btn-sm glyphicon glyphicon-plus">
                            <b>CREATE</b>
                        </button>
                    </a>

                    <a href="{{route('stockout.index')}}">
                        <button type="button" class="btn btn-default btn-sm glyphicon glyphicon-repeat">
                            <b>RESET</b>
                        </button>
                    </a>

                </ul>
                <br><br><br>

                <div style="width: auto; height: auto; overflow: scroll;">

                    <table class="table table-striped" id="userTbl">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>ID</th>
                            <th>Branch</th>
                            <th>Item</th>
                            <th>Customer</th>
                            <th>Quantity</th>
                            <th>Total rate</th>
                            <th>Discount</th>
                            <th>Total amount</th>
                            <th>Created_Date</th>

                        </tr>
                        </thead>

                        <tbody>

                        @forelse($stock_outs as $stock_out)
                            <tr>

                                <td>
                                    {{--<a href="{{ route('stockout.edit', array($stock_out->id))}}"--}}
                                    {{--class="btn btn-info btn-xs"><label--}}
                                    {{--class="glyphicon glyphicon-pencil"></label> &nbsp;Edit</a>--}}


                                    <a href="{{ route('stockout.detail', array($stock_out->id))}}"
                                       class="btn btn-success btn-xs"><label
                                                class="glyphicon glyphicon-eye-open"></label> &nbsp;View Detail</a>
                                </td>

                                <td>{{$stock_out->id}}</td>
                                <td>{{$stock_out->branch->name}}</td>

                                <td>{{$stock_out->stockin->name}}</td>

                                <td>{{$stock_out->customer->first_name}}</td>

                                <td>{{$stock_out->quantity}}</td>

                                <td>{{$stock_out->totalrate}}</td>

                                <td>{{$stock_out->discount}}</td>

                                <td>{{$stock_out->totalamount}}</td>

                                <td>{!! $stock_out->created_at !!}</td>


                            </tr>
                        @empty
                            <tr>
                                <td colspan="15">
                                    <p class="text-danger text-center"><b>No data found !</b></p>
                                </td>

                            </tr>
                        @endforelse

                        </tbody>

                    </table>
                </div>
                {{ $stock_outs->links() }}
            </div>
            {!! Form::close() !!}
        </div>
    </section>

    </body>

@endsection

@endif
@endfor
@empty
@endforelse