@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==7)
@section('printitemlist')
    <script type="text/javascript">
        jQuery(function ($) {
            $("#itemlist-print").click(function () {
                var thePopup = window.open('', "item Listing");
                $('#itemlist').clone().appendTo(thePopup.document.body);
                thePopup.print();
                thePopup.close();
            });
        });
    </script>
    @stop
@section('content')

    <div style="width:300px;">
        @include('flash::message')
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html"></section>
    <!-- Main content -->
    <section class="content">
        {{--section to display recently created data--}}
        <div>
            {!! Form::open(['route' => 'stockout.destroy','method'=>'DELETE','id'=>'formDelete']) !!}
            <div class="box-header with-border">


                <div style="width: auto; height: auto; overflow: scroll;">
                    <table class="table table-striped table-bordered" id="itemlist">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Item</th>
                            <th>Customer</th>
                            <th>Quantity</th>
                            <th>Total_Rate</th>
                            <th>Discount</th>
                            <th>Total_Amount</th>
                            <th>Created_Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($stock_outs as $stock_out)
                            <tr>

                                <td>{{$stock_out->id}}</td>

                                <td>{{$stock_out->stockin->name}}</td>

                                <td>{{$stock_out->customer->first_name}}</td>

                                <td>{{$stock_out->quantity}}</td>

                                <td>{{$stock_out->totalrate}}</td>

                                <td>{{$stock_out->discount}}</td>

                                <td>{{$stock_out->totalamount}}</td>

                                <td>{!! $stock_out->created_at !!}</td>

                            </tr>

                        @empty
                            <tr>
                                <td colspan="15">
                                    <p class="text-danger text-center"><b>No data found !</b></p>
                                </td>

                            </tr>
                        @endforelse
                        <tr>
                            <td  colspan="6"><b>grand total:</b>
                            </td>
                            <td>
                            </td>
                        </tr>
                        </tbody>

                    </table>
                    <a class="btn btn-success" id="itemlist-print">
                        print
                    </a>
                </div>
                {{ $stock_outs->links() }}
            </div>
            {!! Form::close() !!}
        </div>
        <!-- Default box -->
        <div class="box box-primary ">
            <div class="box-body">
                <div class="box-body">

                    {{ Form::open(['route'=>'stockout.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','id'=>'formid','files' => true]) }}

                    @if (Session::get('sess_branch_id'))
                        {!! Form::hidden('branch_id', $value = Session::get('sess_branch_id')) !!}
                    @else
                        {!! Form::hidden('branch_id', $value = 1) !!}
                    @endif

                    <div class="form-group">
                        {!! Form::label('customer', 'Customer-Number', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-3">
                            {!! Form::select('customer_number',$customers_number,null,['required'=>'required','id'=>'customerNumber','class'=>'a form-control select','placeholder'=>'Select Customer']) !!}
                            <span class="error">{{ $errors->first('customer_id') }}</span>
                        </div>

                        {!! Form::label('customer', 'Customer-Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-3">
                            {!! Form::text('',null,['required'=>'required','id'=>'customerName','class'=>'form-control select','placeholder'=>'Select Customer','readonly']) !!}
                            <span class="error">{{ $errors->first('customer_id') }}</span>
                        </div>

                        <div class="col-lg-3">
                            {!! Form::hidden('customer_id',null,['id'=>'customerId','class'=>'form-control select','placeholder'=>'Select Customer']) !!}
                            <span class="error">{{ $errors->first('customer_id') }}</span>
                        </div>

                    </div>
                    <br><br><br>

                    <table class="table table-striped" id="tblDoc">

                        <thead>
                        <tr>
                            <th>Item_barcode</th>
                            <th>Item</th>
                            <th>Rate</th>
                            <th>Max. Quantity</th>
                            <th>Quantity</th>
                            <th>Total_Rate</th>
                            <th>Discount_By</th>
                            <th>Discount</th>
                            <th>Payable_Amount</th>
                            <th colspan="2">Action</th>
                        </tr>
                        <tr id="uploadrow_0">


                            <td>
                                {!! Form::select('stockin_id0',$stockins,null,['required'=>'required','id'=>'itembarcode0','class'=>'form-control select','placeholder'=>'Select Item barcode']) !!}
                                <span class="error">{{ $errors->first('stockin_id') }}</span>
                                <span id="itemresult0"></span>
                            </td>
                            <td>
                                {{ Form::text('item',null,['required'=>'required','id'=>'item0','class'=>'form-control','readonly'] ) }}
                                <span class="error">{{ $errors->first('item') }}</span>
                            </td>
                            <td>
                                {{ Form::text('rate',null,['required'=>'required','id'=>'rate0','class'=>'form-control','readonly'] ) }}
                                <span class="error">{{ $errors->first('rate') }}</span>
                            </td>

                            <td>
                                {{ Form::text('',null,['required'=>'required','id'=>'maxquantity0','class'=>'form-control','readonly'] ) }}
                                <span class="error">{{ $errors->first('rate') }}</span>
                            </td>

                            <td>
                            {{ Form::number('quantity0',null,['pattern'=>'^[0-9]','min'=>0,'required'=>'required','id'=>'quantity0','class'=>'form-control'] ) }}
                            {{ Form::hidden('rem_quantity0',null,['id'=>'rem_quantity0','class'=>'form-control'] ) }}
                            {{ Form::hidden('reorder_level0',null,['id'=>'reorder_level0','class'=>'form-control'] ) }}
                            <!-- The Modal -->
                                <div id="myModal" class="modal">
                                    <!-- Modal content -->
                                    <div class="modal-content">
                                        <span class="close">&times;</span>
                                        <h1 id="itemname"></h1>
                                        <p>This item is going to be finished</p>
                                    </div>
                                </div>
                                <span class="error">{{ $errors->first('quantity') }}</span>
                                <span id="quantityMsg0"></span>
                            </td>

                            <td>
                                {{ Form::text('totalrate0',null,['required'=>'required','id'=>'totalrate0','class'=>'form-control','readonly'] ) }}
                                <span class="error">{{ $errors->first('totalrate') }}</span>
                            </td>
                            <td>
                                {!! Form::select('sign',['%','NPR'],null,['required'=>'required','id'=>'sign0','class'=>'form-control select']) !!}
                            </td>

                            <td>
                                {{ Form::text('discount0',null,['required'=>'required','id'=>'discount0','class'=>'form-control'] ) }}
                                <br>
                                <span class="error">{{ $errors->first('discount') }}</span>
                                {{--<div class="col-lg-9 pull-right">--}}
                                {{--</div>--}}
                            </td>

                            <td>
                                {{ Form::text('totalamount0',null,['required'=>'required','id'=>'totalamount0','class'=>'form-control','readonly'] ) }}
                                <span class="error">{{ $errors->first('totalamount') }}</span>
                            </td>

                            <td>
                                <a href="#">
                                    <span id="remove0" class="delete-row">Delete</span>
                                </a>
                            </td>
                            <td>
                                {{ Form::hidden('clickcount',null,['id'=>'clickcount0','class'=>'form-control'] ) }}
                            </td>
                        </tr>

                        </thead>

                    </table>

                    <div>
                        <a href="#">
                            <span id="addAnother" class="add-another">+ Add Another</span>
                        </a>
                        <div class="col-lg-8 pull-right">

                            <div class="col-lg-4">
                                {{ Form::label('total calculation',null,['class'=>'form-label'] ) }}
                                {{ Form::text('net_total',$value=0,['required'=>'required','id'=>'net_total','class'=>'form-control','readonly'] ) }}
                                <span class="error">{{ $errors->first('net_total') }}</span>
                            </div>
                            <div class="col-lg-2">
                                {{ Form::label('taken cost',null,['class'=>'form-label'] ) }}
                                {{ Form::text('taken_cost',null,['required'=>'required','id'=>'taken_cost','class'=>'form-control'] ) }}
                                <span class="error">{{ $errors->first('taken_cost') }}</span>
                            </div>
                            <div class="col-lg-2">
                                {{ Form::label('return cost',null,['class'=>'form-label'] ) }}
                                {{ Form::text('return_cost',null,['required'=>'required','id'=>'return_cost','class'=>'form-control','readonly'] ) }}
                                <span class="error">{{ $errors->first('return_cost') }}</span>
                            </div>
                        </div>
                    </div>

                </div>
                <br>

                <div class="text-right">

                    <button type="submit" id="btnSOsave" class="btn btn-rounded btn-success btn-raised active"
                            onclick="return confirm('want to SAVE?');"><b>CHECKOUT</b>
                    </button>

                    <a href="{{route('stockout.index')}}"
                       class="glyphicon glyphicon-chevron-left"
                       onclick="return confirm('Are you sure ?');">
                        <b>BACK</b></a>

                </div>
                <br>

            {{ Form::close() }}


            <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
        </div>


    </section>


@endsection

@endif
@endfor
@empty
@endforelse

