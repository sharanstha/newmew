@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==7)
@section('content')

    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Edit Stock-Out
        </h1>
    </section>


    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">

            <div class="box-body">
                <div class="box-body">

                    {{ Form::model($stock_out, array('method' => 'post', 'route' => array('stockout.update', $stock_out->id),'files' => true)) }}

                    <div class="form-group">
                        {{ Form::label('customer', 'Customer_Number',['class'=>'col-lg-2 control-label']) }}
                        <div class="col-lg-4">
                            <select name="customer_id" class="form-control">
                                @foreach($customers_number as $key=>$customer)
                                    <option value="{{$key}}"
                                            @if($key == $stock_out->customer_id) selected @endif >{{$customer}}</option>
                                @endforeach
                            </select>
                        </div>

                        {{ Form::label('customer', 'Customer_Name',['class'=>'col-lg-2 control-label']) }}
                        <div class="col-lg-4">
                            <select name="customer_id" class="form-control">
                                @foreach($customers as $key=>$customer)
                                    <option value="{{$key}}"
                                            @if($key == $stock_out->customer_id) selected @endif >{{$customer}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br><br><br><br><br>

                    <div class="form-group">
                        {{ Form::label('stockin', 'Item',['class'=>'col-lg-2 control-label']) }}
                        <div class="col-lg-4">
                            <select name="stockin_id" class="form-control">
                                @foreach($stockins as $key=>$stockin)
                                    <option value="{{$key}}"
                                            @if($key == $stock_out->stockin_id) selected @endif >{{$stockin}}</option>
                                @endforeach
                            </select>
                        </div>

                        {!! Form::label('quantity', 'Quantity', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-4">
                            {!! Form::text('quantity', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('quantity') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="text-right">
                        <button type="submit" class="btn btn-rounded btn-success btn-raised active"
                                onclick="return confirm('Ready to UPDATE ?');">
                            <b>UPDATE</b>
                        </button>

                        <a href="{{route('stockout.index')}}"
                           class="glyphicon glyphicon-chevron-left"
                           onclick="return confirm('Are you sure ?');">
                            <b>BACK</b></a>
                    </div>

                    {{ Form::close() }}

                <!-- /.box-body -->
                    <div class="box-footer">
                        Footer
                    </div>
                    <!-- /.box-footer-->

                </div>
            </div>
        </div>
    </section>
@endsection

@endif
@endfor
@empty
@endforelse
