<?php
namespace App\Modules\Stockout\Repository;


interface StockoutInterface
{

    public function findAll($limit =null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC']);

    public function findByBranchId($limit =null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC']);

    public function find($id);

    public function findList();

    public function save($data);

    public function update($id, $role);

    public function delete($ids);

    public function findAllByDate($limit =null, $filter = [], $sort = ['by' => 'created_at', 'sort' => 'DESC']);

}

