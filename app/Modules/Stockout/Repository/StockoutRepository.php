<?php

namespace App\Modules\Stockout\Repository;


use App\Modules\Stockout\Models\Stockout;
use Illuminate\Support\Facades\Session;

class StockoutRepository implements StockoutInterface
{

    public function findAll($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = Stockout::when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['id'])) {
                $query->where('id', 'like', '%' . $filter['id'] . '%')->get();
            }

            return $query;
        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    public function findByBranchId($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = Stockout::where('branch_id', '=', Session::get('sess_branch_id'))->when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['id'])) {
                $query->where('id', 'like', '%' . $filter['id'] . '%')->get();
            }

            return $query;
        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    public function findAllByDate($limit = 99999, $filter = [], $sort = ['by' => 'created_at', 'sort' => 'DESC'])
    {
        $result = Stockout::when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['created_at'])) {
                $query->where('created_at', 'like', '%' . $filter['created_at'] . '%')->get();
            }

            return $query;
        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    /**
     * @param $id
     * @return mixed|static
     */
    public function find($id)
    {
        return Stockout::find($id);
    }

    public function findList()
    {
        return Stockout::find('name', null, '---');
    }

    public function save($data)
    {

        $stock_out = Stockout::create($data);
        $stock_out->save();

    }

    public function update($id, $data)
    {
        $stock_out = Stockout::find($id);

        $stock_out->update($data);

    }

    public function delete($ids)
    {
        return Stockout::destroy($ids);
    }

}
