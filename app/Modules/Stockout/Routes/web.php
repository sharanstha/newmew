<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {

    Route::group(['prefix' => 'stockout'], function () {
//INDEX
        Route::get('', ['as' => 'stockout.index', 'uses' => 'StockoutController@index']);

//CREATE
        Route::get('create', ['as' => 'stockout.create', 'uses' => 'StockoutController@create']);

//STORE
        Route::post('store', ['as' => 'stockout.store', 'uses' => 'StockoutController@store']);

//EDIT
        Route::get('edit/{id}', ['as' => 'stockout.edit', 'uses' => 'StockoutController@edit']);

//UPDATE
        Route::post('edit/{id}', ['as' => 'stockout.update', 'uses' => 'StockoutController@update']);

//DELETE
        Route::get('delete/{id}', ['as' => 'stockout.delete', 'uses' => 'StockoutController@delete']);

        Route::delete('delete', ['as' => 'stockout.destroy', 'uses' => 'StockoutController@destroy']);

//DETAIL
        Route::get('detail/{id}', ['as' => 'stockout.detail', 'uses' => 'StockoutController@detail']);

    });

//FETCH NAME

    Route::get('getName', ['as' => 'getName', 'uses' => 'StockoutController@getName']);


//FETCH DATA

    Route::get('getData', ['as' => 'getData', 'uses' => 'StockoutController@getData']);

});
