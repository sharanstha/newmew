<?php

namespace App\Modules\Stockout\Models;

use App\Modules\Branch\Models\Branch;
use App\Modules\Customer\Models\Customer;
use App\Modules\Stockin\Models\StockIn;
use Illuminate\Database\Eloquent\Model;

class Stockout extends Model
{
    protected $fillable = [
        'branch_id',
        'customer_id',
        'stockin_id',
        'quantity',
        'totalrate',
        'discount',
        'totalamount'

    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function stockIn()
    {
        return $this->belongsTo(StockIn::class, 'stockin_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }
}
