<?php

namespace App\Modules\Stockout\Providers;

use App\Modules\Stockout\Repository\StockoutInterface;
use App\Modules\Stockout\Repository\StockoutRepository;
use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'stockout');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'stockout');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'stockout');
        $this->loadConfigsFrom(__DIR__.'/../config');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->stockoutRegister();
    }

    public function stockoutRegister()
    {
        $this->app->bind(StockoutInterface::class, StockoutRepository::class);
    }
}
