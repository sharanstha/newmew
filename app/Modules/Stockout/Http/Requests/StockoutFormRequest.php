<?php

namespace App\Modules\Stockout\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StockoutFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()){
            case 'POST':
                return [
//                  'customer_id' => 'required',
//                    'stockIn_id' => 'required',
//                    'quantity' => 'required|numeric',
                ];

            case 'PUT':
                return [
//                    'customer_id' => 'required',
//                    'stockIn_id' => 'required',
//                    'quantity' => 'required|numeric',
                ];
        }
    }
}
