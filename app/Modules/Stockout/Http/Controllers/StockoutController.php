<?php

namespace App\Modules\Stockout\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Branch\Repository\BranchInterface;
use App\Modules\Customer\Models\Customer;
use App\Modules\Customer\Repository\CustomerInterface;
use App\Modules\Stockin\Models\StockIn;
use App\Modules\Stockin\Repository\StockinInterface;
use App\Modules\Stockout\Http\Requests\StockoutFormRequest;
use App\Modules\Stockout\Models\Stockout;
use App\Modules\Stockout\Repository\StockoutInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class StockoutController extends Controller
{
    protected $stock_out;
    protected $stockin;
    protected $customer;
    protected $branch;

    public function __construct(StockoutInterface $stock_out, StockinInterface $stockin, CustomerInterface $customer, BranchInterface $branch)
    {
        $this->stock_out = $stock_out;
        $this->stockin = $stockin;
        $this->customer = $customer;
        $this->branch = $branch;
    }

    public function index(Request $request)
    {
        $filter['id'] = $request->get('id');

        $limit = $request->get('limit', 100000);
        if (Session::get('sess_branch_id')) {
            $data['stock_outs'] = $this->stock_out->findByBranchId($limit, $filter);

        } else {
            $data['stock_outs'] = $this->stock_out->findAll($limit, $filter);
        }

        $data['stockin'] = $this->stockin->findList();
        $data['customer'] = $this->customer->findList();

        return view('stockout::stock_out.index', $data);
    }

    public function create(Request $request)
    {
        $data['stockin'] = $this->stockin->findList();
        $data['customer'] = $this->customer->findList();

        if (Session::get('sess_branch_id')) {

            $data['stockins'] = StockIn::where('branch_id', '=', Session::get('sess_branch_id'))->pluck('sku', 'id');
        } else {
            $data['stockins'] = StockIn::where('branch_id', '=', Session::get('sess_id_forstockout'))->pluck('sku', 'id');
        }

        $data['stockin'] = $this->stockin->findList();

        $data['customers'] = Customer::pluck('first_name', 'id');

        $data['customer'] = $this->customer->findList();

        if (Session::get('sess_branch_id')) {
            $data['customers_number'] = Customer::where('branch_id', '=', Session::get('sess_branch_id'))->pluck('primary_contact_no', 'id');
        } else {
            $data['customers_number'] = Customer::where('branch_id', '=', Session::get('sess_id_forstockout'))->pluck('primary_contact_no', 'id');
        }

        $data['customer_number'] = $this->customer->findList();

        $filter['created_at'] = date('y-m-d h:i');

        $limit = $request->get('limit', 100000);

        $data['stock_outs'] = $this->stock_out->findAllByDate($limit, $filter);

        return view('stockout::stock_out.create', $data);
    }

    public function store(StockoutFormRequest $request)
    {
        $count = Input::get('clickcount');


        try {
            for ($i = 0; $i <= $count; $i++) {
                $input = array(
                    'branch_id' => Input::get('branch_id'),
                    'customer_id' => Input::get('customer_id'),
                    'stockin_id' => Input::get('stockin_id' . $i),
                    'quantity' => Input::get('quantity' . $i),
                    'totalrate' => Input::get('totalrate' . $i),
                    'discount' => Input::get('discount' . $i),
                    'totalamount' => Input::get('totalamount' . $i),
                );
                $rem_quantity = Input::get('rem_quantity' . $i);
                DB::table('stock_ins')
                    ->where('id', Input::get('stockin_id' . $i))
                    ->update(['quantity' => $rem_quantity]);
                $this->stock_out->save($input);
            }

            Flash::success("Data Created Successfully");

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('stockout.create'));

    }

    public function edit($id)
    {

        $data['stock_out'] = $this->stock_out->find($id);

        $data['stock_outs'] = $this->stock_out->findList();

        $data['stockins'] = StockIn::pluck('name', 'id');

        $data['customers'] = Customer::pluck('first_name', 'id');

        $data['customers_number'] = Customer::pluck('primary_contact_no', 'id');

        return view('stockout::stock_out.edit', $data);

    }

    public function update(StockoutFormRequest $request, $id)
    {
        try {
            $input = $request->all();

            $this->stock_out->update($id, $input);

            Flash::success("Updated Successfully");

            return redirect(route('stockout.index'));

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());
        }
    }

    public function delete($ids)
    {
        return Stockout::destroy($ids);

    }

    public function destroy(Request $request)
    {
        $ids = $request->all();

        try {

            if ($request->has('toDelete')) {

                Stockout::destroy($ids['toDelete']);

                Flash::success("Successfully Deleted");

            } else {

                Flash::error("Please check at least one to delete");

            }
        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('stockout.index'));
    }

    public function detail($id)
    {
        $data['stock_outs'] = Stockout::find($id);

        return view('stockout::stock_out.detail', $data);
    }

    public function getName()
    {
        $id = Input::get('customerNumber');
        return $this->customer->findName($id);
    }


    public function getData()
    {
        $id = Input::get('item');
        return $this->stockin->findData($id);
    }

}
