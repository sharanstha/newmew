<?php

namespace App\Modules\Customer\Http\Controllers;

use App\Modules\Branch\Repository\BranchInterface;
use App\Modules\Customer\Http\Requests\CustomerFormRequest;
use App\Modules\Customer\Models\Customer;
use App\Modules\Customer\Repository\CustomerInterface;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class CustomerController extends Controller
{
    protected $customer;
    protected $branch;

    public function __construct(CustomerInterface $customer, BranchInterface $branch)
    {
        $this->customer = $customer;
        $this->branch = $branch;
    }

    public function index(Request $request)
    {
        $filter['first_name'] = $request->get('first_name');

        $limit = $request->get('limit', 100000);

        if (Session::get('sess_branch_id')) {
            $data['customer'] = $this->customer->findByBranchId($limit, $filter);
        } else {
            $data['customer'] = $this->customer->findAll($limit, $filter);
        }

        $data['customer']->appends(['first_name' => $filter['first_name']]);
        return view('customer::customer.index', $data);
    }

    public function create()
    {
        $data['customer'] = $this->customer->findList();

        return view('customer::customer.create', $data);
    }

    public function store(CustomerFormRequest $request)
    {

        $input = $request->all();

        try {

            $this->customer->save($input);

            Flash::success("Successfully Created");

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('customer.index'));

    }

    public function edit($id)
    {
        $data['customer'] = $this->customer->find($id);

        $data['customers'] = $this->customer->findList();

        return view('customer::customer.edit', $data);

    }

    public function update(CustomerFormRequest $request, $id)
    {
        try {
            $input = $request->all();

            $this->customer->update($id, $input);

            Flash::success("Updated Successfully");

            return redirect(route('customer.index'));

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());
        }
    }

    public function delete($ids)
    {
        return Customer::destroy($ids);

    }

    public function destroy(Request $request)
    {
        $ids = $request->all();

        try {

            if ($request->has('toDelete')) {

                Customer::destroy($ids['toDelete']);

                Flash::success("Successfully Deleted");

            } else {

                Flash::error("Please check at least one to delete");

            }
        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('customer.index'));
    }

    public function detail($id)
    {
        $data['customers'] = Customer::find($id);

        return view('customer::customer.detail', $data);
    }
}
