<?php

namespace App\Modules\Customer\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()){

            case 'POST':
                return [
                    'first_name' => 'required|regex:/^[a-zA-Z]+$/u|max:255,customers',
                    'last_name' => 'required|regex:/^[a-zA-Z]+$/u|max:255,customers',
                    'primary_contact_no' => 'required|unique:customers',
                ];

            case 'PUT':
                return [
                    'first_name' => 'required|regex:/^[a-zA-Z]+$/u|max:255,first_name,'.$this->route('id'),
                    'last_name' => 'required|regex:/^[a-zA-Z]+$/u|max:255,last_name,'.$this->route('id'),
                    'primary_contact_no' => 'required|unique:customers',
                ];
        }
    }
}
