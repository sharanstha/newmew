<?php

namespace App\Modules\Customer\Repository;


use App\Modules\Customer\Models\Customer;
use Illuminate\Support\Facades\Session;

class CustomerRepository implements CustomerInterface
{

    public function findAll($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = Customer::when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['first_name'])) {
                $query->where('first_name', 'like', '%' . $filter['first_name'] . '%')->get();
            }

            return $query;

        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    public function findByBranchId($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = Customer::where('branch_id', '=', Session::get('sess_branch_id'))->when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['first_name'])) {
                $query->where('first_name', 'like', '%' . $filter['first_name'] . '%')->get();
            }

            return $query;

        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    /**
     * @param $id
     * @return mixed|static
     */
    public function find($id)
    {
        return Customer::find($id);
    }

    public function findList()
    {
        return Customer::find('first_name', null, '---');
    }


    public function save($data)
    {

        $customer = Customer::create($data);

        $customer->save();

    }

    public function update($id, $data)
    {
        $customer = Customer::find($id);

        $customer->update($data);

    }

    public function delete($ids)
    {
        return Customer::destroy($ids);
    }

    public function findName($id)
    {
        $data = Customer::where('id', '=', $id)->get();
        return json_encode($data);

    }


}
