<?php
/**
 * Created by PhpStorm.
 * User: bibek
 * Date: 4/9/18
 * Time: 4:48 PM
 */

namespace App\Modules\Customer\Repository;

interface CustomerInterface
{
    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC']);

    public function findByBranchId($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC']);

    public function find($id);

    public function findList();

    public function save($data);

    public function update($id, $role);

    public function delete($ids);

    public function findName($id);

}