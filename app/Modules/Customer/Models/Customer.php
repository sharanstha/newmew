<?php

namespace App\Modules\Customer\Models;

use App\Modules\Branch\Models\Branch;
use App\Modules\Stockout\Models\Stockout;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
      'first_name','last_name','age','gender','profession','location','primary_contact_no','secondary_contact_no','email','notes','branch_id'
    ];

    public function stockout()
    {
        return $this->belongsTo(Stockout::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

}
