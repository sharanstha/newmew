@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==1)
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Create
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">

            <div class="box-body">
                <div class="box-body">

                    {{ Form::open(['route'=>'customer.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) }}

                    @if (Session::get('sess_branch_id'))
                        {!! Form::hidden('branch_id', $value = Session::get('sess_branch_id')) !!}
                    @else
                        {!! Form::hidden('branch_id', $value = 1) !!}
                    @endif


                    <div class="form-group">
                        {!! Form::label('first_name', 'First-Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('first_name', $value = null, ['placeholder'=>'First-Name','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('first_name') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('last_name', 'Last-Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('last_name', $value = null, ['placeholder'=>'Last-Name','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('last_name') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('age', 'Age', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('age', $value = null, ['placeholder'=>'Age','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('age') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('gender', 'Gender', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            <select class="form-control" name="gender">
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                            <span class="error">{{ $errors->first('gender') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('profession', 'Profession', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('profession', $value = null, ['placeholder'=>'Profession','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('profession') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('location', 'Location', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('location', $value = null, ['placeholder'=>'Location','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('location') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('primary_contact_no', 'Primary_Contact_No', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('primary_contact_no', $value = null, ['placeholder'=>'Primary_Contact_No','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('primary_contact_no') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('secondary_contact_no', 'Secondary_Contact_No', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('secondary_contact_no', $value = null, ['placeholder'=>'Secondary_Contact_No','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('secondary_contact_no') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'Email', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('email', $value = null, ['placeholder'=>'Email','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('email') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('notes', 'Notes', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('notes', $value = null, ['id'=>'editor','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('notes') }}</span>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" id="btnSIsave" class="btn btn-rounded btn-success btn-raised active"
                                onclick="return confirm('want to SAVE?');"><b>SAVE</b>
                        </button>

                        <a href="{{route('customer.index')}}"
                           class="glyphicon glyphicon-chevron-left"
                           onclick="return confirm('Are you sure ?');">
                            <b>BACK</b></a>
                    </div>

                {{ Form::close() }}

                <!-- /.box-body -->
                    <div class="box-footer">
                        Footer
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>
        </div>
    </section>
@endsection
@endif
@endfor
@empty
@endforelse