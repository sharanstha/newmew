@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==1)
@section('content')

    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Edit
        </h1>
    </section>


    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">

            <div class="box-body">
                <div class="box-body">

                    {{ Form::model($customer, array('method' => 'post', 'route' => array('customer.update', $customer->id),'files' => true)) }}

                    <div class="form-group">
                        {!! Form::label('first_name', 'First_Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('first_name', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('first_name') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('last_name', 'Last_Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('last_name', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('last_name') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('age', 'Age', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('age', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('age') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('gender', 'Gender', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            <div class="col-lg-22.5">
                                <select class="form-control" name="gender">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                            <span class="error">{{ $errors->first('gender') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('profession', 'Profession', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('profession', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('profession') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('location', 'location', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('location', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('location') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('primary_contact_no', 'Primary_Contact_No', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('primary_contact_no', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('primary_contact_no') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('secondary_contact_no', 'Secondary_Contact_No', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('secondary_contact_no', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('secondary_contact_no') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('email', 'Email', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('email', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('email') }}</span>
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        {!! Form::label('notes', 'Notes', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('notes', $value = null, ['id'=>'editor','class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('notes') }}</span>
                        </div>
                    </div>
                    <br><br><br>
                </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-rounded btn-success btn-raised active"
                                onclick="return confirm('Ready to UPDATE ?');">
                            <b>UPDATE</b>
                        </button>

                        <a href="{{route('customer.index')}}"
                           class="glyphicon glyphicon-chevron-left"
                           onclick="return confirm('Are you sure ?');">
                            <b>BACK</b></a>
                    </div>

                    {{ Form::close() }}

                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->

            </div>
        </div>
    </section>
@endsection

@endif
@endfor
@empty
@endforelse