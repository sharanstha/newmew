@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==1)
@section('content')
    <body>
    <section class="content">

        <div class="box box-primary ">

            <div class="panel-heading">
                <div class="col-xs-4">
                    <input type="text" class="search form-control" placeholder="search?...">
                </div>
            </div>
            <br><br>

            {!! Form::open(['route' => 'customer.destroy','method'=>'DELETE','id'=>'formDelete']) !!}
            <div class="box-header with-border">

                @include('flash::message')

                <ul class="icons-list pull-right">

                    <a href="{{route('customer.create')}}">
                        <button id="create-user" type="button" class="btn btn-primary btn-sm glyphicon glyphicon-plus">
                            <b>CREATE</b>
                        </button>
                    </a>

                    <a href="{{route('customer.index')}}">
                        <button type="button" class="btn btn-default btn-sm glyphicon glyphicon-repeat">
                            <b>RESET</b>
                        </button>
                    </a>
                    {{--<button type="submit" class="btn btn-danger btn-sm glyphicon glyphicon-remove"--}}
                    {{--onclick="return confirm('Are you sure ?');">--}}
                    {{--<b>DELETE</b>--}}
                    {{--</button>--}}
                </ul>
                <br><br><br>

                <div style="width: auto; height: auto; overflow: scroll;" id="userTbl">

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Check_All</th>
                            <th>ID</th>
                            <th>Branch</th>
                            <th>First_Name</th>
                            <th>Last_Name</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Profession</th>
                            <th>Location</th>
                            <th>Primary_Contact_No</th>
                            <th>Secondary_Contact_No</th>
                            <th>Email</th>
                            <th>Notes</th>
                            <th>Created_Date</th>
                        </tr>
                        </thead>

                        <tbody>

                        @forelse($customer as $customers)
                            @if($customers->id<>1)
                                <tr>
                                    <td>
                                        <a href="{{ route('customer.edit', array($customers->id))}}"
                                           class="btn btn-info btn-xs"><label
                                                    class="glyphicon glyphicon-pencil"></label> &nbsp;Edit</a>

                                        <a href="{{ route('customer.detail', array($customers->id))}}"
                                           class="btn btn-success btn-xs"><label
                                                    class="glyphicon glyphicon-eye-open"></label> &nbsp;View Detail</a>
                                    </td>
                                    <td>
                                        <div class="pretty p-default">
                                            {!! Form::checkbox('toDelete[]',$customers->id, false,['class'=>'checkItem']) !!}
                                            <div class="state">
                                                <label></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{$customers->id}}

                                    <td>{{$customers->branch->name}}

                                    <td>{{$customers->first_name}}

                                    <td>{{$customers->last_name}}

                                    <td>{{$customers->age}}

                                    <td>{{$customers->gender}}

                                    <td>{{$customers->profession}}

                                    <td>{{$customers->location}}

                                    <td>{{$customers->primary_contact_no}}

                                    <td>{{$customers->secondary_contact_no}}

                                    <td>{{$customers->email}}

                                    <td>{!! $customers->notes !!}

                                    <td>{!! $customers->created_at !!}</td>


                                </tr>
                            @endif
                        @empty
                            <tr>
                                <td colspan="15">
                                    <p class="text-danger text-center"><b>No data found !</b></p>
                                </td>

                            </tr>
                        @endforelse

                        </tbody>

                    </table>
                </div>
                {{ $customer->links() }}
            </div>
            {!! Form::close() !!}
        </div>
    </section>

    </body>

@endsection

@endif
@endfor
@empty
@endforelse