<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {

    Route::group(['prefix' => 'customer'], function () {

//INDEX
        Route::get('', ['as' => 'customer.index', 'uses' => 'CustomerController@index']);

//CREATE
        Route::get('create', ['as' => 'customer.create', 'uses' => 'CustomerController@create']);

//STORE
        Route::post('store', ['as' => 'customer.store', 'uses' => 'CustomerController@store']);

//EDIT
        Route::get('edit/{id}',['as' => 'customer.edit', 'uses' => 'CustomerController@edit']);

//UPDATE
        Route::post('edit/{id}',['as' => 'customer.update', 'uses' => 'CustomerController@update']);

//DELETE
        Route::get('delete/{id}',['as' => 'customer.delete', 'uses' => 'CustomerController@delete']);

        Route::delete('delete', ['as' => 'customer.destroy', 'uses' => 'CustomerController@destroy']);

//DETAIL
        Route::get('detail/{id}', ['as' => 'customer.detail', 'uses' => 'CustomerController@detail']);

    });
});

