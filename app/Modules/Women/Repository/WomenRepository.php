<?php
namespace App\Modules\Women\Repository;





use App\Modules\Women\Models\Women;

class WomenRepository implements WomenInterface
{

    public function findAll($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = Women::when(array_keys($filter, true), function ($query) use ($filter) {
            $query->where('id', 'like', '%' . $filter['id'] . '%');

            return $query;
        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    /**
     * @param $id
     * @return mixed|static
     */
    public function find($id)
    {
        return Women::find($id);
    }

    public function findList()
    {
        return Women::find('id', null, '---');
    }


    public function save($data)
    {

        $men = Women::create($data);

        $men->save();

    }

    public function update($id, $data)
    {
        $men = Women::find($id);

        $men->update($data);

    }

    public function delete($ids)
    {
        return Women::destroy($ids);
    }

}
