<?php
namespace App\Modules\Women\Repository;


interface WomenInterface
{

    public function findAll($limit =null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC']);

    public function find($id);

    public function findList();

    public function save($data);

    public function update($id, $role);

    public function delete($ids);

}
