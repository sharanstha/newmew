@extends("admin::layout")
@section('content')

    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Edit
        </h1>
    </section>


    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">

            <div class="box-body">
                <div class="box-body">

                    {{ Form::model($women, array('method' => 'post', 'route' => array('women.update', $women->id),'files' => true)) }}

                    <div class="form-group">
                        {{ Form::label('item_id', 'Item',['class'=>'col-lg-1 control-label']) }}
                        <div class="col-lg-8">
                            <select name="item_id" class="form-control">
                                @foreach($items as $key=>$item)
                                    <option value="{{$key}}"
                                            @if($key == $women->item_id) selected @endif >{{$item}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="text-right">
                        <button type="submit" class="btn btn-rounded btn-success btn-raised active"
                                onclick="return confirm('Ready to UPDATE ?');">
                            <b>UPDATE</b>
                        </button>

                        <a href="{{route('women.index')}}"
                           class="glyphicon glyphicon-chevron-left"
                           onclick="return confirm('Are you sure ?');">
                            <b>BACK</b></a>
                    </div>

                    {{ Form::close() }}

                <!-- /.box-body -->
                    <div class="box-footer">
                        Footer
                    </div>
                    <!-- /.box-footer-->

                </div>
            </div>
        </div>
    </section>
@endsection

