@extends("admin::layout")
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Create
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">

            <div class="box-body">
                <div class="box-body">

                    {{ Form::open(['route'=>'women.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) }}

                    <div class="form-group">
                        {!! Form::label('item', 'Item', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::select('item_id',$items,null,['class'=>'form-control select','placeholder'=>'Select Item']) !!}
                            <span class="error">{{ $errors->first('item_id') }}</span>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" id="btnSIsave" class="btn btn-rounded btn-success btn-raised active"
                                onclick="return confirm('want to SAVE?');"><b>SAVE</b>
                        </button>
                        <a href="{{route('women.index')}}"
                           class="glyphicon glyphicon-chevron-left"
                           onclick="return confirm('Are you sure ?');">
                            <b>BACK</b></a>
                    </div>

                {{ Form::close() }}

                <!-- /.box-body -->
                    <div class="box-footer">
                        Footer
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>
        </div>
    </section>
@endsection