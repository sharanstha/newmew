<?php

namespace App\Modules\Women\Models;

use App\Modules\Stockin\Models\StockIn;
use Illuminate\Database\Eloquent\Model;

class Women extends Model
{
    protected $fillable = [
        'item_id'
    ];

    public function stockIn()
    {
        return $this->belongsTo(StockIn::class);
    }
}
