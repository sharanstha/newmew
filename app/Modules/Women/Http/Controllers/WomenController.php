<?php

namespace App\Modules\Women\Http\Controllers;

use App\Modules\Item\Models\Item;
use App\Modules\Item\Repository\ItemInterface;
use App\Modules\Women\Http\Requests\WomenFormRequest;
use App\Modules\Women\Models\Women;
use App\Modules\Women\Repository\WomenInterface;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class WomenController extends Controller
{
    protected $women;

    protected $item;

    public function __construct(WomenInterface $women, ItemInterface $item)
    {
        $this->women = $women;

        $this->item = $item;
    }

    public function index(Request $request)
    {
        $filter['id'] = $request->get('id');

        $limit = $request->get('limit', 100000);

        $data['women'] = $this->women->findAll($limit, $filter);
        $data['women']->appends(['id' => $filter['id']]);

        $data['items'] = $this->item->findAll();

        return view('women::women.index', $data);
    }

    public function create()
    {
        $data['items'] = Item::pluck('name', 'id');

        $data['item'] = $this->item->findAll();

        $data['women'] = $this->women->findList();

        return view('women::women.create', $data);
    }

    public function store(WomenFormRequest $request)
    {

        $input = $request->all();

        try {

            $this->women->save($input);

            Flash::success("Data Created Successfully");

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('women.index'));

    }

    public function edit($id)
    {
        $data['women'] = $this->women->find($id);

        $data['womens'] = $this->women->findList();

        $data['items'] = Item::pluck('name', 'id');

        return view('women::women.edit', $data);

    }

    public function update(WomenFormRequest $request, $id)
    {
        try {
            $input = $request->all();

            $this->women->update($id, $input);

            Flash::success("Data Updated Successfully");

            return redirect(route('women.index'));

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());
        }
    }

    public function delete($ids)
    {
        return women::destroy($ids);

    }

    public function destroy(Request $request)
    {
        $ids = $request->all();

        try {

            if ($request->has('toDelete')) {

                women::destroy($ids['toDelete']);

                Flash::success("Successfully Deleted");

            } else {

                Flash::error("Please check at least one to delete");

            }
        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('women.index'));
    }

    public function detail($id)
    {
        $data['womens'] = Women::find($id);

        return view('women::women.detail',$data);
    }

}
