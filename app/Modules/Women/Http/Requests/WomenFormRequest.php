<?php

namespace App\Modules\Women\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WomenFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()){
            case 'POST':
                return[
                    'item_id' => 'required|unique:womens',
                ];

            case 'PUT':
                return[
                    'item_id' => 'required|unique:womens,name,' . $this->route('id'),
                ];
            default:
                break;
        }
    }
}
