<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {

    Route::group(['prefix' => 'women'], function () {

//INDEX
        Route::get('', ['as' => 'women.index', 'uses' => 'WomenController@index']);

//CREATE
        Route::get('create', ['as' => 'women.create', 'uses' => 'WomenController@create']);

//STORE
        Route::post('store', ['as' => 'women.store', 'uses' => 'WomenController@store']);

//EDIT
        Route::get('edit/{id}',['as' => 'women.edit', 'uses' => 'WomenController@edit']);

//UPDATE
        Route::post('edit/{id}',['as' => 'women.update', 'uses' => 'WomenController@update']);

//DELETE
        Route::get('delete/{id}',['as' => 'women.delete', 'uses' => 'WomenController@delete']);

        Route::delete('delete', ['as' => 'women.destroy', 'uses' => 'WomenController@destroy']);

//DETAIL
        Route::get('detail/{id}', ['as' => 'women.detail', 'uses' => 'WomenController@detail']);

    });
});


