<?php

namespace App\Modules\Women\Providers;

use App\Modules\Women\Repository\WomenInterface;
use App\Modules\Women\Repository\WomenRepository;
use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'women');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'women');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'women');
        $this->loadConfigsFrom(__DIR__.'/../config');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->womenRegister();
    }

    public function womenRegister()
    {
        $this->app->bind(WomenInterface::class, WomenRepository::class);
    }
}
