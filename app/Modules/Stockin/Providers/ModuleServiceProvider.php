<?php

namespace App\Modules\Stockin\Providers;

use App\Modules\Stockin\Repository\StockinInterface;
use App\Modules\Stockin\Repository\StockinRepository;
use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'stockin');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'stockin');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'stockin');
        $this->loadConfigsFrom(__DIR__.'/../config');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->stockinRegister();
    }

    public function stockinRegister()
    {
        $this->app->bind(StockinInterface::class, StockinRepository::class);
    }
}
