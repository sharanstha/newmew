@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==6)
@section('printscript')
    <script type="text/javascript">
        {{--@yield('scripts')--}}

        jQuery(function ($) {
            'use strict';
            @forelse($stockIns as $stockIn)
            $("#{{ $stockIn->id }}").click(function () {
                $(".tdhide").show();
                var thePopup = window.open('', "barcode",);
                $('#{{ $stockIn->id }}_{{ $stockIn->id }}').clone().appendTo(thePopup.document.body);
                thePopup.print();
                thePopup.close();
                $(".tdhide").hide();
            });
            @empty
            @endforelse

            $("#myModal-print").click(function () {
                var thePopup = window.open('', "finishing item Listing");
                $('#finishingItemList').clone().appendTo(thePopup.document.body);
                thePopup.print();
                thePopup.close();
            });


            {{--$("#{{$stockIn->sku}}-{{$stockIn->id}}-td").mouseover(function (e) {--}}
            {{--$(".img-zoom-container-{{$stockIn->sku}}-{{$stockIn->id}}").show();--}}
            {{--e.preventDefault();--}}
            {{--});--}}
            {{--$(".img-zoom-container-{{$stockIn->sku}}-{{$stockIn->id}}").hide();--}}
            {{--$("#{{$stockIn->sku}}-{{$stockIn->id}}-td").mouseout(function (e) {--}}
            {{--$(".img-zoom-container-{{$stockIn->sku}}-{{$stockIn->id}}").hide();--}}
            {{--e.preventDefault();--}}
            {{--});--}}
        });

    </script>
@stop
@section('reordernotification')
    <script type="text/javascript">
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal
        modal.style.display = "block";

        // When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
@stop
@section('content')
    <body>
    <section class="content">

        <div class="box box-primary ">

            <div class="panel-heading">
                {{--<div class="col-xs-4">--}}
                {{--<input type="text" class="search form-control" placeholder="search?...">--}}
                {{--</div>--}}

                <div class="panel-heading">
                    <div class="col-xs-4">
                        <form action="{{ route('stockIn.index') }}" method="GET">
                            <div class="form-group input-group">
                                <input type="text" name="sku" class="form-control" placeholder="Search By Barcode"
                                       required/>
                                <label class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-sm" style="margin-left: 10px">
                                        <i class="fa fa-search"></i>
                                        Find
                                    </button>
                                </label>
                            </div>
                        </form>
                    </div>

                    @forelse (Session::get('sess_permission') as $permission)
                        @for($i=0;$i<Session::get('sess_lastID');$i++)
                            @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==13)
                                <div class="col-xs-8">
                                    {{ Form::open(['route'=>'stockIn.import_file','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) }}
                                    <div class="col-lg-4">
                                        {!! Form::file('file',null,['required'=>'required','id'=>'file','class'=>'form-control']) !!}
                                    </div>
                                    <input type="submit" value="import data" class="col-lg-3 btn btn-success"/>

                                    {{ Form::close() }}
                                </div>
                            @endif
                        @endfor
                    @empty
                    @endforelse
                </div>
                <br><br>


                {!! Form::open(['route' => 'stockIn.destroy','method'=>'DELETE','id'=>'formDelete']) !!}
                <div class="box-header with-border">

                    @include('flash::message')

                    <ul class="icons-list pull-right">

                        @forelse (Session::get('sess_permission') as $permission)
                            @for($i=0;$i<Session::get('sess_lastID');$i++)
                                @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==10)
                                    <a href="{{route('stockIn.create')}}">
                                        <button type="button" class="btn btn-primary btn-sm glyphicon glyphicon-plus">
                                            <b>CREATE</b>
                                        </button>
                                    </a>
                                @endif
                            @endfor
                        @empty
                        @endforelse

                        <a href="{{route('stockIn.index')}}">
                            <button type="button" class="btn btn-default btn-sm glyphicon glyphicon-repeat">
                                <b>RESET</b>
                            </button>
                        </a>
                        {{--<button type="submit" class="btn btn-danger btn-sm glyphicon glyphicon-remove"--}}
                        {{--onclick="return confirm('Are you sure ?');">--}}
                        {{--<b>DELETE</b>--}}
                        {{--</button>--}}
                    </ul>
                    <br><br><br>

                    <div style="width: auto; height: auto; overflow: scroll;" id="userTbl">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Check_All</th>
                                <th>ID</th>
                                <th>Branch</th>
                                <th>Status</th>
                                <th>Name</th>
                                <th>Unit</th>
                                <th>Category</th>
                                <th>Vendor</th>
                                <th>Brand</th>
                                <th>Color</th>
                                <th>Size</th>
                                {{--<th>Image</th>--}}
                                <th>LOT_No</th>
                                <th>SKU</th>
                                <th>Selling_Price</th>
                                <th>Quantity</th>
                                <th>invoice_no</th>
                                <th>Created_Date</th>
                                <th>Updated_Date</th>

                            </tr>
                            </thead>

                            <tbody>

                            @forelse($stockIns as $stockIn)
                                <tr>
                                    <td>
                                        @forelse (Session::get('sess_permission') as $permission)
                                            @for($i=0;$i<Session::get('sess_lastID');$i++)
                                                @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==11)
                                                    <a href="{{ route('stockIn.edit', array($stockIn->id))}}"
                                                       class="btn btn-info btn-xs"><label
                                                                class="glyphicon glyphicon-pencil"></label>
                                                        &nbsp;Edit</a>
                                                @endif
                                            @endfor
                                        @empty
                                        @endforelse

                                        @forelse (Session::get('sess_permission') as $permission)
                                            @for($i=0;$i<Session::get('sess_lastID');$i++)
                                                @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==12)
                                                    <a href="{{ route('stockIn.detail', array($stockIn->id))}}"
                                                       class="btn btn-success btn-xs"><label
                                                                class="glyphicon glyphicon-eye-open"></label> &nbsp;View
                                                        Detail</a>
                                                @endif
                                            @endfor
                                        @empty
                                        @endforelse
                                    </td>
                                    <td>
                                        <div class="pretty p-default">
                                            {!! Form::checkbox('toDelete[]',$stockIn->id, false,['class'=>'checkItem']) !!}
                                            <div class="state">
                                                <label></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{$stockIn->id}}
                                    <td>{{$stockIn->branch->name}}

                                    <td>
                                        @if ($stockIn->quantity > 20)

                                            <a href="#" type="button" style="color: green">
                                                <i class="fa fa-toggle-on fa-2x text-success-800">
                                                </i>
                                            </a>
                                        @else($stockIn->quantity < 20 && $stockIn->quantity >=0)
                                            @if(($stockIn->quantity) <= ($stockIn->reorder_level))
                                                <a href="#" class="btnStatus" style="color: red">
                                                    <i class="fa fa-toggle-on fa-2x text-warning-800">
                                                    </i>
                                                </a>
                                                <!-- The Modal -->
                                                <div id="myModal" class="modal"
                                                     style="width: auto; height: auto; overflow: scroll;">
                                                    <!-- Modal content -->
                                                    <div class="modal-content col-lg-6 col-lg-offset-3">

                                                        <span class="close">X</span>
                                                        <table class="table table-striped" id="finishingItemList">
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>ITEM NAME</th>
                                                                <th>SKU</th>
                                                                <th>Brand</th>
                                                                <th>Category</th>
                                                                <th>REMAINING QUANTITY</th>
                                                            </tr>
                                                            @forelse($allStockin as $s)
                                                                @if(($s->quantity) <= ($s->reorder_level))
                                                                    <tr>
                                                                        <td>{{$s->id}}</td>
                                                                        <td>{{$s->name}}</td>
                                                                        <td>
                            <span>
                                {{substr($s->sku, strpos($s->sku, "/")+1)}}<br>
                            <img src="data:image/png;base64,{{DNS1D::getBarcodePNG(substr($s->sku, strpos($s->sku, "/")+1),'C128',1,35,array(1,1,1))}}"
                                 alt="barcode"/><br>
                                Rs:{{ $s->per_item_selling_price }}

                           </span>


                                                                        </td>
                                                                        <td>{{$s->brand->name}}</td>
                                                                        <td>{{$s->category->name}}</td>
                                                                        <td>{{$s->quantity}}</td>

                                                                    </tr>
                                                                @endif
                                                            @empty
                                                            @endforelse
                                                        </table>
                                                        <h1 align="center">These items is going to be finished</h1>
                                                        <a class="btn btn-link" id="myModal-print">
                                                            print
                                                        </a>
                                                    </div>
                                                </div>

                                            @else
                                                <a href="#" class="btnStatus" style="color: yellow">
                                                    <i class="fa fa-toggle-on fa-2x text-danger-800">
                                                    </i>
                                                </a>
                                            @endif
                                        @endif

                                    </td>

                                    <td>{{$stockIn->name}}</td>

                                    <td>{{$stockIn->unit_id}}</td>

                                    <td>{{$stockIn->category->name}}</td>

                                    <td>{{$stockIn->vendor->company_name}}</td>

                                    <td>{{$stockIn->brand->name}}</td>

                                    <td>{{$stockIn->color}}</td>

                                    <td>{{$stockIn->size}}</td>


                                    {{--<td id="{{$stockIn->sku}}-{{$stockIn->id}}-td">--}}
                                    {{--<img src="{{asset('assets/uploads/stockin/'.$stockIn->file_name)}}"--}}
                                    {{--style="width:90px;height:100px;"/>--}}
                                    {{--</td>--}}

                                    <td>{{$stockIn->lot_no}}</td>

                                    <td>
                            <span id="{{ $stockIn->id }}_{{ $stockIn->id }}">
                                <table>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                <td class="col-lg-2">
                                    {{substr($stockIn->sku, strpos($stockIn->sku, "/")+1)}}<br>
                            <img src="data:image/png;base64,{{DNS1D::getBarcodePNG(substr($stockIn->sku, strpos($stockIn->sku, "/")+1),'C128',1,35,array(1,1,1))}}"
                                 alt="barcode"/><br>
                                Rs:{{ $stockIn->per_item_selling_price }}
                                    </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                <td class="col-lg-2 tdhide" style="display:none">
                                {{substr($stockIn->sku, strpos($stockIn->sku, "/")+1)}}<br>
                            <img src="data:image/png;base64,{{DNS1D::getBarcodePNG(substr($stockIn->sku, strpos($stockIn->sku, "/")+1),'C128',1,35,array(1,1,1))}}"
                                 alt="barcode"/><br>
                                Rs:{{ $stockIn->per_item_selling_price }}
                                    </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                <td class="col-lg-2 tdhide" style="display:none">
                               {{substr($stockIn->sku, strpos($stockIn->sku, "/")+1)}}<br>
                            <img src="data:image/png;base64,{{DNS1D::getBarcodePNG(substr($stockIn->sku, strpos($stockIn->sku, "/")+1),'C128',1,35,array(1,1,1))}}"
                                 alt="barcode"/><br>
                                Rs:{{ $stockIn->per_item_selling_price }}
                                    </td>
                                    </tr>
                                </table>
                           </span>
                                        <a class="btn btn-link" id="{{ $stockIn->id }}">
                                            generate barcode
                                        </a>
                                        <br><br>
                                    </td>


                                    <td>{{$stockIn->per_item_selling_price}}</td>

                                    <td>{{$stockIn->quantity}}</td>

                                    <td>{{$stockIn->invoice_no}}</td>

                                    <td>{!! $stockIn->created_at !!}</td>
                                    <td>{!! $stockIn->updated_at !!}</td>


                                </tr>
                            @empty
                                <tr>
                                    <td colspan="19">
                                        <p class="text-danger text-center"><b>No data found !</b></p>
                                    </td>

                                </tr>
                            @endforelse

                            </tbody>

                        </table>
                    </div>
                    {{ $stockIns->links() }}
                </div>
                {!! Form::close() !!}
            </div>
    </section>

    </body>

@endsection

@endif
@endfor
@empty
@endforelse