@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==10)
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Create Stock-In
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">


            <div class="box-body">
                <div class="box-body">

                    {{ Form::open(['route'=>'stockIn.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) }}


                    @if (Session::get('sess_branch_id'))
                        {!! Form::hidden('branch_id', $value = Session::get('sess_branch_id')) !!}
                    @else
                        {!! Form::hidden('branch_id', $value = 1) !!}
                    @endif

                    <div class="form-group">
                        {!! Form::label('category', 'Category', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::select('category_id',$categories,null,['required'=>'required','id'=>'category_id','class'=>'form-control select','placeholder'=>'Select Category']) !!}
                            <span class="error">{{ $errors->first('category_id') }}</span>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped" id="tblItemDoc">

                            <tr id="uploadItemRow_0">
                                <td>
                                    {{ Form::label('name', 'Name',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::text('name0',null,['required'=>'required','id'=>'name0','placeholder'=>'Name','class'=>'form-control'] ) }}
                                    <span class="error">{{ $errors->first('name') }}</span><br/><br/></br>

                                    {!! Form::label('unit', 'Unit', ['class' => 'col-lg-2 control-label required_label']) !!}
                                    {!! Form::text('unit_id0',null,['readonly','required'=>'required','id'=>'unit0','class'=>'form-control','placeholder'=>'Select Unit']) !!}
                                    <span class="error">{{ $errors->first('unit_id') }}</span>

                                </td>
                                <td>

                                    {!! Form::label('vendor_id', 'Vendor', ['class' => 'col-lg-2 control-label required_label']) !!}
                                    {!! Form::select('vendor_id0',$vendors,null,['required'=>'required','id'=>'vendor0','class'=>'form-control select','placeholder'=>'Select Vendor']) !!}
                                    <span class="error">{{ $errors->first('vendor_id') }}</span><br/><br/></br>

                                    {{ Form::label('brand_id', 'Brand',['class'=>'col-lg-2 control-label']) }}
                                    {!! Form::select('brand_id0',$brands,null,['required','id'=>'brand0','class'=>'form-control select','placeholder'=>'Select Brand']) !!}
                                    <span class="error">{{ $errors->first('brand_id') }}</span>
                                </td>

                                <td>
                                    {{ Form::label('color', 'Color',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::select('color0',['white'=>'white','silver'=>'silver','grey'=>'grey','black'=>'black',
                                    'navy-blue'=>'navy-blue','cerulean'=>'cerulean','sky-blue'=>'sky-blue','turquoise'=>'turquoise',
                                    'blue-green'=>'blue-green','azure'=>'azure','teal'=>'teal','cyan'=>'cyan','green'=>'green',
                                    'lime'=>'lime','chartreuse'=>'chartreuse','olive'=>'olive','yellow'=>'yellow','gold'=>'gold',
                                    'amber'=>'amber','orange'=>'orange','brown'=>'brown','orange-red'=>'orange-red','red'=>'red',
                                    'maroon'=>'maroon','rose'=>'rose','red-violet'=>'red-violet','pink'=>'pink','magenta'=>'magenta',
                                    'purple'=>'purple','blue_violet'=>'blue_violet','indigo'=>'indigo','violet'=>'violet',
                                    'peach'=>'peach','apricot'=>'apricot','ochre'=>'ochre','plum'=>'plum'],null,
                                    ['id'=>'color0','placeholder'=>'Color','class'=>'form-control'] ) }}
                                    <span class="error">{{ $errors->first('color') }}</span><br/><br/></br>

                                    {{ Form::label('size', 'Size',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::text('size0',null,['id'=>'size0','placeholder'=>'Size','class'=>'form-control'] ) }}

                                    <span class="error">{{ $errors->first('size') }}</span>
                                </td>
                                <td>
                                    {{ Form::label('cp', 'C.P',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::text('cp0',null,['required','id'=>'cp0','placeholder'=>'cost price','class'=>'form-control'] ) }}
                                    <span class="error">{{ $errors->first('cp') }}</span><br/><br/></br>

                                    {{ Form::label('margin', 'margin',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::text('margin0',null,['id'=>'margin0','placeholder'=>'margin','class'=>'form-control'] ) }}

                                    <span class="error">{{ $errors->first('margin') }}</span>
                                </td>
                                <td>
                                    {{ Form::label('sku', 'SKU',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::text('sku0',null,['required'=>'required','id'=>'sku0','placeholder'=>'SKU','class'=>'form-control'] ) }}
                                    <div>
                                        <a href="#" id="sku_generator0">
                                            <span>generate sku</span>
                                        </a>
                                    </div>
                                    <span id="skuresult0"></span>
                                    <span class="error">{{ $errors->first('sku') }}</span><br/><br/>

                                    {{ Form::label('selling_price', 'Selling_Price',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::text('per_item_selling_price0',null,['required'=>'required','id'=>'per_item_selling_price0','placeholder'=>'Selling_Price','class'=>'form-control'] ) }}
                                    {{--<a href="#" id="opener0">generate SP</a><br/>--}}
                                    <span class="error">{{ $errors->first('per_item_selling_price') }}</span>
                                </td>

                                <td>
                                    {{ Form::label('lot_no', 'LOT_No',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::text('lot_no0',null,['required'=>'required','id'=>'lot_no0','placeholder'=>'LOT-No','class'=>'form-control'] ) }}
                                    <span class="error">{{ $errors->first('lot_no') }}</span><br/><br/></br>

                                    {{ Form::label('quantity', 'Quantity',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::text('quantity0',null,['required'=>'required','id'=>'maxquantity0','placeholder'=>'Quantity','class'=>'form-control'] ) }}
                                    <span class="error">{{ $errors->first('quantity') }}</span>
                                </td>

                                <td>
                                    {{ Form::label('invoice_no', 'Invoice_no',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::text('invoice_no0',null,['required'=>'required','id'=>'invoice_no0','placeholder'=>'Invoice-No','class'=>'form-control'] ) }}
                                    <span class="error">{{ $errors->first('invoice_no') }}</span><br/><br/></br>

                                    {{ Form::label('reorder_level', 'Reorder_level',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::text('reorder_level0',null,['required'=>'required','id'=>'reorder_level0','placeholder'=>'Re-order level','class'=>'form-control'] ) }}
                                    <span class="error">{{ $errors->first('reorder_level') }}</span>
                                </td>

                                <td>
                                    {{ Form::label('detail', 'Description',['class'=>'col-lg-2 control-label']) }}
                                    {{ Form::textarea('detail0',null,['id'=>'detail0','class'=>'form-control'] ) }}
                                    <span class="error">{{ $errors->first('detail') }}</span>
                                </td>

                                <td>
                                    <a href="#">
                                        <span id="remove0" class="delete-row">Delete</span>
                                    </a>
                                </td>
                                <td>
                                    {{ Form::hidden('clickcount',null,['id'=>'clickcount0','class'=>'form-control'] ) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <a href="#" id="addAnotherItem">
                            <span class="add-another">+ Add Another row</span>
                        </a>
                    </div>
                    <div class="text-right">
                        <button type="submit" id="btnSIsave" class="btn btn-rounded btn-success btn-raised active"
                                onclick="return confirm('want to SAVE?');"><b>CHECK IN</b>
                        </button>

                        <a href="{{route('stockIn.index')}}"
                           class="btn btn-orange btn-sm glyphicon glyphicon-chevron-left"
                           onclick="return confirm('Are you sure ?');">
                            <b>BACK</b></a>
                    </div>

                {{ Form::close() }}

                <!-- /.box-body -->
                    <div class="box-footer">
                        Footer
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>
        </div>
    </section>
@endsection

@endif
@endfor
@empty
@endforelse