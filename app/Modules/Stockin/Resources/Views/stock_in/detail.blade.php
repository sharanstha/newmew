@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==12)
@section('content')

    <div class="container">

        <div style="width: 40%; margin: 0px auto;">

            <h1><b>Name: </b>{{ $stockIns->name }}</h1>

            <h1><b>Category Name: </b>{{ $stockIns->category->name }}</h1>

            <h2><b>Vendor Name: </b>{{ $stockIns->vendor->name }}</h2>

            <h2><b>Brand Name </b>{{ $stockIns->brand->name }}</h2>

            <h3><b>Unit:</b>{{ $stockIns->unit_id }}</h3>

            <div class="post_commentbox"><span><i
                            class="fa fa-calendar"></i><b>Created_Date: </b>{{ $stockIns->created_at }}</span><br><br>
                <span>
                            <b>Color: </b>{!! $stockIns->color !!}</span><br><br>
                <span>
                            <b>Size: </b>{!! $stockIns->size !!}</span><br><br>
                <span>
                            <b>LOT-No: </b>{!! $stockIns->lot_no !!}</span><br><br>
                <span>
                            <b>previous barcode: </b>{!! $stockIns->barcode !!}</span><br><br>
                <span><b>SKU: </b></span><br/>
                <span id="ele2">
                            {{substr($stockIns->sku, strpos($stockIns->sku, "/")+1)}}<br>
                            <img src="data:image/png;base64,{{DNS1D::getBarcodePNG(substr($stockIns->sku, strpos($stockIns->sku, "/")+1),'C128',1,35,array(1,1,1))}}"
                                 alt="barcode"/><br>
                                Rs:{{ $stockIns->per_item_selling_price }}
                </span>
                <a class="btn btn-link" id="print-link">
                    generate barcode
                </a>
                <br><br>
                <span id="myimage-td">
                    {{--<img src="" style="width:90px;height:100px;"/>--}}
                    <img id="myimage" src="{{asset('assets/uploads/stockin/'.$stockIns->file_name)}}"
                         width="150" height="100">

                    <div class="img-zoom-container" id="hidden">
                        <div id="myresult" class="img-zoom-result"></div>
                    </div>
                    @section('zoomimage')
                        <script>
                            imageZoom("myimage", "myresult");
                        </script>
                    @stop
                </span><br/><br/>
                <b>S.P: </b>{!! $stockIns->per_item_selling_price !!}</span><br><br>
                <b>C.P.: </b>{!! $stockIns->cp !!}</span><br><br>
                <b>margin: </b>{!! $stockIns->margin !!}</span><br><br>
                <span><b>invoice_no: </b>{!! $stockIns->invoice_no !!}</span><br><br>
                <span>
                            <b>Quantity: </b>{!! $stockIns->quantity !!}</span><br><br>
                <span>
                            <b>reorder_level: </b>{!! $stockIns->reorder_level !!}</span><br><br>
                <span>
                            <b>Detail: </b>{!! $stockIns->detail !!}</span><br><br>

            </div>

        </div>

    </div>

@endsection

@endif
@endfor
@empty
@endforelse

