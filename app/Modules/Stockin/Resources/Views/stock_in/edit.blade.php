@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==11)
@section('content')

    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Edit
        </h1>
    </section>


    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">

            <div class="box-body">
                <div class="box-body">

                    {{ Form::model($stockIn, array('method' => 'post', 'route' => array('stockIn.update', $stockIn->id),'files' => true)) }}

                    <div class="form-group">
                        {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('name', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('name') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {{ Form::label('category_id', 'Category',['class'=>'col-lg-2 control-label']) }}
                        <div class="col-lg-8">
                            <select name="category_id" class="form-control">
                                @foreach($categories as $key=>$category)
                                    <option value="{{$key}}"
                                            @if($key == $stockIn->category_id) selected @endif >{{$category}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {{ Form::label('vendor', 'Vendor',['class'=>'col-lg-2 control-label']) }}
                        <div class="col-lg-8">
                            <select name="vendor_id" class="form-control">
                                @foreach($vendors as $key=>$vendor)
                                    <option value="{{$key}}"
                                            @if($key == $stockIn->vendor_id) selected @endif >{{$vendor}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {{ Form::label('brand', 'Brand',['class'=>'col-lg-2 control-label']) }}
                        <div class="col-lg-8">
                            <select name="brand_id" class="form-control">
                                @foreach($brands as $key=>$brand)
                                    <option value="{{$key}}"
                                            @if($key == $stockIn->brand_id) selected @endif >{{$brand}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {!! Form::label('color', 'Color', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {{ Form::select('color',[$stockIn->color,'white'=>'white','silver'=>'silver','grey'=>'grey','black'=>'black',
                                    'navy-blue'=>'navy-blue','cerulean'=>'cerulean','sky-blue'=>'sky-blue','turquoise'=>'turquoise',
                                    'blue-green'=>'blue-green','azure'=>'azure','teal'=>'teal','cyan'=>'cyan','green'=>'green',
                                    'lime'=>'lime','chartreuse'=>'chartreuse','olive'=>'olive','yellow'=>'yellow','gold'=>'gold',
                                    'amber'=>'amber','orange'=>'orange','brown'=>'brown','orange-red'=>'orange-red','red'=>'red',
                                    'maroon'=>'maroon','rose'=>'rose','red-violet'=>'red-violet','pink'=>'pink','magenta'=>'magenta',
                                    'purple'=>'purple','blue_violet'=>'blue_violet','indigo'=>'indigo','violet'=>'violet',
                                    'peach'=>'peach','apricot'=>'apricot','ochre'=>'ochre','plum'=>'plum'],null,
                                    ['id'=>'color0','class'=>'form-control'] ) }}
                            <span class="error">{{ $errors->first('color') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {!! Form::label('size', 'Size', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('size', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('size') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {!! Form::label('lot_no', 'LOT_No', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('lot_no', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('lot_no') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {!! Form::label('barcode', 'Previous_Barcode', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('barcode', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('barcode') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {!! Form::label('cp', 'Cost_Price', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('cp', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('cp') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {!! Form::label('selling_price', 'Selling_Price', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('per_item_selling_price', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('per_item_selling_price') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {!! Form::label('invoice_no', 'invoice_no', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('invoice_no', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('invoice_no') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {!! Form::label('quantity', 'Quantity', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('quantity', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('quantity') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {!! Form::label('reorder_level', 'reorder_level', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('reorder_level', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('reorder_level') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        {!! Form::label('detail', 'Description', ['class' => 'col-lg-2 control-label required_label']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('detail', $value = null, ['class'=>'form-control']) !!}
                            <span class="error">{{ $errors->first('detail') }}</span>
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="text-right">
                        <button type="submit" class="btn btn-rounded btn-success btn-raised active"
                                onclick="return confirm('Ready to UPDATE ?');">
                            <b>UPDATE</b>
                        </button>

                        <a href="{{route('stockIn.index')}}"
                           class="glyphicon glyphicon-chevron-left"
                           onclick="return confirm('Are you sure ?');">
                            <b>BACK</b></a>
                    </div>

                {{ Form::close() }}

                <!-- /.box-body -->
                    <div class="box-footer">
                        Footer
                    </div>
                    <!-- /.box-footer-->

                </div>
            </div>
        </div>
    </section>
@endsection

@endif
@endfor
@empty
@endforelse