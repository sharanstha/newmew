<?php

namespace App\Modules\Stockin\Repository;


use App\Modules\Stockin\Models\StockIn;
use Illuminate\Support\Facades\Session;

class StockInRepository implements StockInInterface
{

    protected static $filePath = '/assets/uploads/stockin/';

    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {

        $result = StockIn::when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['sku'])) {
                $query->where('sku', 'like', '%' . $filter['sku'] . '%');
            }

            if (!empty($filter['vendor_id'])) {
                $query->where('vendor_id', 'like', '%' . $filter['vendor_id'] . '%');
            }

            return $query;
        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate($limit ? $limit : env('DEF_PAGE_LIMIT'));
        return $result;
    }

    public function findByBranchId($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {

        $result = StockIn::where('branch_id', '=', Session::get('sess_branch_id'))->when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['sku'])) {
                $query->where('sku', 'like', '%' . $filter['sku'] . '%');
            }

            if (!empty($filter['vendor_id'])) {
                $query->where('vendor_id', 'like', '%' . $filter['vendor_id'] . '%');
            }

            return $query;
        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate($limit ? $limit : env('DEF_PAGE_LIMIT'));
        return $result;
    }

    public function find($id)
    {
        return StockIn::find($id);
    }

    public function showAll()
    {
        if (Session::get('sess_branch_id')) {
            return StockIn::where('branch_id', Session::get('sess_branch_id'))->get();

        } else {
            return StockIn::all();
        }
    }

    public function findList()
    {
        return StockIn::find('sku', null, '---');
    }

    public function save($data)
    {
        $data['status'] = 1;
        return StockIn::create($data);
    }

    public function update($id, $data)
    {
        $stock_in = StockIn::find($id);
        return $stock_in->update($data);
    }

    public function delete($ids)
    {
        return StockIn::destroy($ids);
    }

    public function upload($file, $filePath = '')
    {

        $imageName = time() . '.' . $file->getClientOriginalExtension();

        $path = !empty($filePath) ? $filePath : self::$filePath;

        $destinationPath = public_path($path);

        $file->move($destinationPath, $imageName);

        return $imageName;
    }

    public function findData($id)
    {
        $data = StockIn::where('id', '=', $id)->get();
        return json_encode($data);
    }


    public function findSKU($sku)
    {
        if (Session::get('sess_branch_id')) {
            $data = StockIn::where('sku', '=', Session::get('sess_branch_id') . '/' . $sku)->get();
        } else {
            $data = StockIn::where('sku', '=', 'A' . '/' . $sku)->get();
        }

        if ($data->count() > 0) {
            return 'sku already exists in database';
        } else {
            return null;
        }
    }

}
