<?php

namespace App\Modules\Stockin\Repository;


interface StockinInterface
{

    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function findByBranchId($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);

    public function showAll();

    public function findList();

    public function save($data);

    public function update($id, $role);

    public function delete($ids);

    public function upload($file, $filePath = '');

    public function findData($ids);

    public function findSKU($sku);

}
