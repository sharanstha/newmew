<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStockIns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_ins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('unit_id');
            $table->integer('category_id')->nullable();
            $table->string('vendor_id');
            $table->string('brand_id')->nullable();
            $table->string('sku')->unique();
            $table->string('barcode')->nullable();
            $table->string('color')->nullable();
            $table->string('size')->nullable();
            $table->string('file_name')->nullable();
            $table->string('lot_no');
            $table->string('per_item_selling_price');
            $table->integer('quantity');
            $table->longText('detail')->nullable();
            $table->string('invoice_no');
            $table->integer('reorder_level');
            $table->boolean('status')->default(1);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_ins');
    }
}
