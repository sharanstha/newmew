<?php

namespace App\Modules\Stockin\Http\Controllers;

use App\Modules\Branch\Models\Branch;
use App\Modules\Branch\Repository\BranchInterface;
use App\Modules\Brand\Models\Brand;
use App\Modules\Brand\Repository\BrandInterface;
use App\Modules\Category\Models\Category;
use App\Modules\Category\Repository\CategoryInterface;
use App\Modules\Stockin\Http\Requests\StockinFormRequest;
use App\Modules\Stockin\Models\StockIn;
use App\Modules\Stockin\Repository\StockinInterface;
use App\Modules\Vendor\Models\Vendor;
use App\Modules\Vendor\Repository\VendorInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;
use Maatwebsite\Excel\Facades\Excel;

class StockInController extends Controller
{
    protected $stockIn;
    protected $category;
    protected $vendor;
    protected $brand;
    protected $branch;

    public function __construct(StockinInterface $stockIn, CategoryInterface $category, VendorInterface $vendor, BrandInterface $brand, BranchInterface $branch)
    {
        $this->stockIn = $stockIn;
        $this->category = $category;
        $this->vendor = $vendor;
        $this->brand = $brand;
        $this->branch = $branch;
    }

    public function index(Request $request)
    {
        $filter['sku'] = $request->get('sku');
        $filter['vendor_id'] = $request->get('vendor_id');

        $limit = $request->get('limit', 5);

        if (Session::get('sess_branch_id')) {
            $data['stockIns'] = $this->stockIn->findByBranchId($limit, $filter);
        } else {
            $data['stockIns'] = $this->stockIn->findAll($limit, $filter);
        }
        $data['allStockin'] = $this->stockIn->showAll();

        $data['stockIns']->appends(['sku' => $filter['sku']]);

        $data['category'] = $this->category->findAll();

        $data['vendors'] = Vendor::pluck('company_name', 'id');
        $data['vendor'] = $this->vendor->findList();

        $data['brand'] = $this->brand->findAll();

        return view('stockin::stock_in.index', $data);
    }

    public function create()
    {
//        $data['categories'] = Category::pluck('name', 'id');
//
//        $data['category'] = $this->category->findList();

        $data['categories'] = $this->category->findList();

        $data['vendors'] = Vendor::pluck('company_name', 'id');

        $data['vendor'] = $this->vendor->findList();

        $data['brands'] = Brand::pluck('name', 'id');

        $data['brand'] = $this->brand->findAll();

        $data['stockIns'] = $this->stockIn->findAll();

        return view('stockin::stock_in.create', $data);
    }

    public function store(Request $request)
    {
        // $input = $request->all();
        $count = Input::get('clickcount');
        try {
            for ($i = 0; $i <= $count; $i++) {
                if (Session::get('sess_branch_id')) {
                    $sku = Session::get('sess_branch_id') . '/' . Input::get('sku' . $i);
                } else {
                    $sku = 'A' . '/' . Input::get('sku' . $i);
                }
                $input = array(
                    'branch_id' => input::get('branch_id'),
                    'category_id' => input::get('category_id'),
                    'name' => Input::get('name' . $i),
                    'unit_id' => Input::get('unit_id' . $i),
                    'vendor_id' => Input::get('vendor_id' . $i),
                    'brand_id' => Input::get('brand_id' . $i),
                    'color' => Input::get('color' . $i),
                    'size' => Input::get('size' . $i),
                    'sku' => $sku,
                    'barcode' => Input::get('barcode' . $i),
                    'lot_no' => Input::get('lot_no' . $i),
                    'per_item_selling_price' => Input::get('per_item_selling_price' . $i),
                    'cp' => Input::get('cp' . $i),
                    'margin' => Input::get('margin' . $i),
                    'quantity' => Input::get('quantity' . $i),
                    'detail' => Input::get('detail' . $i),
                    'invoice_no' => Input::get('invoice_no' . $i),
                    'reorder_level' => Input::get('reorder_level' . $i)
                );
                $this->stockIn->save($input);
            }


            // if ($request->hasFile('file_name'))
            // {
            //     $input['file_name'] = $this->stockIn->upload($input['file_name']);
            // }


            Flash::success("Data Created Successfully");

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('stockIn.index'));

    }

    public function edit($id)
    {

        $data['stockIn'] = $this->stockIn->find($id);

        $data['stockIns'] = $this->stockIn->findList();

        $data['categories'] = Category::pluck('name', 'id');

        $data['vendors'] = Vendor::pluck('company_name', 'id');

        $data['brands'] = Brand::pluck('name', 'id');

        return view('stockin::stock_in.edit', $data);

    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        try {

            if ($request->hasFile('file_name')) {
                $input['file_name'] = $this->stockIn->upload($input['file_name']);
            }

            $this->stockIn->update($id, $input);

            Flash::success("Updated Successfully");

            return redirect(route('stockIn.index', ['id' => $id]));

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());
        }
    }

    public function delete($ids)
    {
        return StockIn::destroy($ids);

    }

    public function destroy(Request $request)
    {
        $ids = $request->all();

        try {

            if ($request->has('toDelete')) {

                StockIn::destroy($ids['toDelete']);

                Flash::success("Successfully Deleted");

            } else {

                Flash::error("Please check at least one to delete");

            }
        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('stockIn.index'));
    }

    public function detail($id)
    {
        $data['stockIns'] = StockIn::find($id);

        return view('stockin::stock_in.detail', $data);
    }

    public function status(Request $request)
    {
        try {
            if ($request->ajax()) {
                $stat = null;
                $id = $request->input('id');
                $status = $this->stockIn->changeStatus($id);
                if ($status == 0) {
                    $stat = '<i class="fa fa-toggle-off fa-2x fa-lg text-danger"></i>';
                } elseif ($status == 1) {
                    $stat = '<i class="fa fa-toggle-on fa-2x text-success-800"></i>';
                }
                $data['tgle'] = $stat;

            }
        } catch (\Throwable $e) {
            $data['error'] = $e->getMessage();
        }

        return response()->json('$data');
    }


    public function checkSKU()
    {
        $sku = Input::get('sku');
        return $this->stockIn->findSKU($sku);
    }

    public function import_file(Request $request)
    {
        $file = $request->file('file');
        if ($file) {
            $path = $file->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                foreach ($data as $key => $value) {
                    if ($this->stockIn->findSKU($value->sku)) {
                        Flash::success("please enter unique SKU in excel file");
                        return redirect(route('stockIn.index'));
                    } else {
                        $insert[] = [
                            'name' => $value->name,
                            'unit_id' => $value->unit_id,
                            'category_id' => $value->category_id,
                            'vendor_id' => $value->vendor_id,
                            'brand_id' => $value->brand_id,
                            'sku' => $value->sku,
                            'color' => $value->color,
                            'size' => $value->size,
                            'lot_no' => $value->lot_no,
                            'per_item_selling_price' => $value->per_item_selling_price,
                            'quantity' => $value->quantity,
                            'detail' => $value->detail,
                        ];
                    }
                }
                if (!empty($insert)) {
                    DB::table('stock_ins')->insert($insert);
                    Flash::success("Data imported Successfully");
                    return redirect(route('stockIn.index'));
                }
            }
        } else {
            Flash::success("please select file...");
            return redirect(route('stockIn.index'));
        }

    }

    public function getUnit()
    {
        $id = Input::get('category');
        return $this->category->findUnit($id);
    }


}