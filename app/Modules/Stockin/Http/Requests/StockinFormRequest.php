<?php

namespace App\Modules\Stockin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StockinFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()){
            case 'POST':
                return[
                    'name' => 'required',
                    'unit_id' => 'required',
                    'category_id' => 'required',
                    'vendor_id' => 'required',
                    'brand_id' => 'required',
                    'sku' => 'required|unique:stock_ins',
                    'lot_no' => 'required',
                    'per_item_selling_price' => 'required',
                    'quantity' => 'required',
                ];

            case 'PUT':
                return[
                    'name' => 'required',
                    'unit_id' => 'required',
                    'category_id' => 'required',
                    'vendor_id' => 'required',
                    'brand_id' => 'required',
                    'barcode' => 'required',
                    'lot_no' => 'required',
                    'per_item_selling_price' => 'required',
                    'quantity' => 'required',
            ];
        }
    }
}
