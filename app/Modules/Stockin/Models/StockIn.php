<?php

namespace App\Modules\Stockin\Models;

use App\Modules\Branch\Models\Branch;
use App\Modules\Brand\Models\Brand;
use App\Modules\Category\Models\Category;
use App\Modules\Stockout\Models\Stockout;
use App\Modules\Unit\Models\Unit;
use App\Modules\Vendor\Models\Vendor;
use Illuminate\Database\Eloquent\Model;

class StockIn extends Model
{
    protected $fillable = [
        'name',
        'unit_id',
        'branch_id',
        'category_id',
        'vendor_id',
        'brand_id',
        'sku',
        'barcode',
        'color',
        'size',
        'file_name',
        'lot_no',
        'per_item_selling_price',
        'cp',
        'margin',
        'quantity',
        'detail',
        'invoice_no',
        'reorder_level',
        'status'
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function stockOut()
    {
        return $this->belongsTo(Stockout::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

}
