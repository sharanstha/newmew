<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {

    Route::group(['prefix' => 'stockIn'], function () {

//INDEX
        Route::get('', ['as' => 'stockIn.index', 'uses' => 'StockInController@index']);

//CREATE
        Route::get('create', ['as' => 'stockIn.create', 'uses' => 'StockInController@create']);

//STORE
        Route::post('store', ['as' => 'stockIn.store', 'uses' => 'StockInController@store']);

//EDIT
        Route::get('edit/{id}', ['as' => 'stockIn.edit', 'uses' => 'StockInController@edit']);

//UPDATE
        Route::post('edit/{id}', ['as' => 'stockIn.update', 'uses' => 'StockInController@update']);

//DELETE
        Route::get('delete/{id}', ['as' => 'stockIn.delete', 'uses' => 'StockInController@delete']);

        Route::delete('delete', ['as' => 'stockIn.destroy', 'uses' => 'StockInController@destroy']);

//DETAIL
        Route::get('detail/{id}', ['as' => 'stockIn.detail', 'uses' => 'StockInController@detail']);

//IMAGE
        Route::post('upload', ['as' => 'upload.image', 'uses' => 'StockInController@upload']);

//STATUS
        Route::get('stockIn/status', ['as' => 'stockIn.status', 'uses' => 'StockInController@status']);

//UNIQUE SKU
        Route::get('checkSKU', ['as' => 'checkSKU', 'uses' => 'StockInController@checkSKU']);

//IMPORT-EXCEL
        Route::post('import_file', ['as' => 'stockIn.import_file', 'uses' => 'StockInController@import_file']);

//FETCH unit
        Route::get('getUnit', ['as' => 'getUnit', 'uses' => 'StockInController@getUnit']);
    });
});