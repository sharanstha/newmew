<?php

namespace App\Modules\Branch\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Branch\Http\Requests\BranchFormRequest;
use App\Modules\Branch\Models\Branch;
use App\Modules\Branch\Repository\BranchInterface;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class BranchController extends Controller
{
    protected $branch;

    public function __construct(BranchInterface $branch)
    {
        $this->branch = $branch;
    }

    public function index(Request $request)
    {
        $filter['name'] = $request->get('name');

        $limit = $request->get('limit', 10000);

        $branch = $this->branch->findAll($limit, $filter);

        $branch->appends(['name' => $filter['name']]);

        return view('branch::branch.index', compact('branch'));
    }

    public function create()
    {
        return view('branch::branch.create');
    }

    public function store(BranchFormRequest $request)
    {

        $input = $request->all();

        try {

            $this->branch->save($input);

            Flash::success("Data Created Successfully");

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('branch.index'));

    }

    public function edit($id)
    {
        $branches = $this->branch->find($id);

        return view('branch::branch.edit', compact('branches'));
    }

    public function update(BranchFormRequest $request, $id)
    {
        try {
            $input = $request->all();

            $this->branch->update($id, $input);

            Flash::success("Data Updated Successfully");

            return redirect(route('branch.index'));

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());
        }
    }

    public function delete($ids)
    {
        return Branch::destroy($ids);
    }

    public function destroy(Request $request)
    {
        $ids = $request->all();

        try {

            if ($request->has('toDelete')) {

                Branch::destroy($ids['toDelete']);

                Flash::success("Successfully Deleted");

            } else {

                Flash::error("Please check at least one to delete");

            }
        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('branch.index'));
    }

    public function detail($id)
    {
        $data['branches'] = Branch::find($id);

        return view('branch::branch.detail', $data);
    }
}
