@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==8)
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Create New Branch
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-primary ">
            <div class="box-body">

                {{ Form::open(['route'=>'branch.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) }}


                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label required_label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('name', $value = null, ['placeholder'=>'Name...','class'=>'form-control']) !!}
                        <span class="error">{{ $errors->first('name') }}</span>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('location', 'Location', ['class' => 'col-lg-2 control-label required_label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('location', $value = null, ['placeholder'=>'Location...','class'=>'form-control']) !!}
                        <span class="error">{{ $errors->first('location') }}</span>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email', ['class' => 'col-lg-2 control-label required_label']) !!}
                    <div class="col-lg-8">
                        {!! Form::email('email', $value = null, ['placeholder'=>'Email...','class'=>'form-control']) !!}
                        <span class="error">{{ $errors->first('email') }}</span>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('password', 'Password', ['class' => 'col-lg-2 control-label required_label']) !!}
                    <div class="col-lg-8">
                        {!! Form::password('password',['placeholder'=>'Password...','class'=>'form-control']) !!}
                        <span class="error">{{ $errors->first('password') }}</span>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" id="btnSIsave" class="btn btn-rounded btn-success btn-raised active"
                            onclick="return confirm('want to SAVE?');"><b>SAVE</b>
                    </button>

                    <a href="{{route('branch.index')}}"
                       class="glyphicon glyphicon-chevron-left"
                       onclick="return confirm('Are you sure ?');">
                        <b>BACK</b></a>
                </div>

            {{ Form::close() }}

            <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
    </section>
@endsection

@endif
@endfor
@empty
@endforelse