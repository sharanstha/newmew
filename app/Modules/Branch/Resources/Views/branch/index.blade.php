@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==8)
@section('content')
    <body>
    <section class="content">

        <div class="box box-primary ">

            <div class="panel-heading">
                <div class="col-xs-4">
                    <input type="text" class="search form-control" placeholder="search?...">
                </div>
            </div>
            <br>

            {!! Form::open(['route' => 'branch.destroy','method'=>'DELETE','id'=>'formDelete']) !!}
            <div class="box-header with-border">

                @include('flash::message')

                <ul class="icons-list pull-right">

                    <a href="{{route('branch.create')}}">
                        <button type="button" class="btn btn-primary btn-sm glyphicon glyphicon-plus">
                            <b>CREATE</b>
                        </button>
                    </a>

                    <a href="{{route('branch.index')}}">
                        <button type="button" class="btn btn-default btn-sm glyphicon glyphicon-repeat">
                            <b>RESET</b>
                        </button>
                    </a>
                    {{--<button type="submit" class="btn btn-danger btn-sm glyphicon glyphicon-remove" onclick="return confirm('Are you sure ?');">--}}
                    {{--<b>DELETE</b>--}}
                    {{--</button>--}}
                </ul>
                <br><br><br>

                <div style="width: auto; height: auto; overflow: scroll;" id="userTbl">

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Check All</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Location</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>Permission</th>
                        </tr>
                        </thead>

                        <tbody>

                        @forelse($branch as $branches)
{{--                            @if($branches->id<>1)--}}
                                <tr>
                                    <td>
                                        <a href="{{ route('branch.edit', array($branches->id))}}"
                                           class="btn btn-info btn-xs"><label
                                                    class="glyphicon glyphicon-pencil"></label> &nbsp;Edit</a>
                                        <a href="{{ route('branch.detail', array($branches->id))}}"
                                           class="btn btn-success btn-xs"><label
                                                    class="glyphicon glyphicon-eye-open"></label> &nbsp;View Detail</a>
                                    </td>
                                    <td>
                                        <div class="pretty p-default">
                                            {!! Form::checkbox('toDelete[]',$branches->id, false,['class'=>'checkItem']) !!}
                                            <div class="state">
                                                <label></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{$branches->id}}</td>

                                    <td>{{$branches->name}}</td>

                                    <td>{{$branches->location}}</td>

                                    <td>{{$branches->email}}</td>

                                    <td>{{$branches->password}}</td>

                                    <td>


                                        @forelse (Session::get('sess_permission') as $permission)
                                            @for($i=0;$i<Session::get('sess_lastID');$i++)
                                                @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==14)
                                                    {{--sess_staff_id is id of currently login user--}}
                                                    <a href="{{ route('branchpermission.create')}}?branchid={{$branches->id}}"
                                                       class="btn btn-success btn-xs"><label
                                                                class="glyphicon glyphicon-eye-open"></label>Branch
                                                        Permission</a>
                                                @endif
                                            @endfor
                                        @empty
                                        @endforelse
                                    </td>

                                </tr>
                            {{--@endif--}}
                        @empty
                            <tr>
                                <td colspan="15">
                                    <p class="text-danger text-center"><b>No data found !</b></p>
                                </td>

                            </tr>
                        @endforelse

                        </tbody>

                    </table>
                </div>
                {{ $branch->links() }}
            </div>
            {!! Form::close() !!}
        </div>
    </section>

    </body>

@endsection

@endif
@endfor
@empty
@endforelse