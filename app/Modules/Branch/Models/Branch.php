<?php

namespace App\Modules\Branch\Models;

use App\BranchUser;
use App\Modules\Customer\Models\Customer;
use App\Modules\Stockin\Models\StockIn;
use App\Modules\Stockout\Models\Stockout;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
        'name',
        'location',
        'email',
        'password'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function branchuser()
    {
        return $this->belongsTo(BranchUser::class);

    }

    public function stockin()
    {
        return $this->belongsTo(StockIn::class);

    }

    public function stockout()
    {
        return $this->belongsTo(Stockout::class);

    }


}
