<?php
/**
 * Created by PhpStorm.
 * User: bibek
 * Date: 7/3/18
 * Time: 12:07 PM
 */

namespace App\Modules\Branch\Repository;

interface BranchInterface

{
    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC']);

    public function find($id);

    public function save($data);

    public function update($id, $role);

    public function delete($ids);

}