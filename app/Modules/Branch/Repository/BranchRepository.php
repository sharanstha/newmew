<?php
/**
 * Created by PhpStorm.
 * User: bibek
 * Date: 7/3/18
 * Time: 12:07 PM
 */

namespace App\Modules\Branch\Repository;


use App\Modules\Branch\Models\Branch;

class BranchRepository implements BranchInterface
{

    public function findAll($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = Branch::when(array_keys($filter, true), function ($query) use ($filter) {
            $query->where('name', 'like', '%' . $filter['name'] . '%');

            return $query;
        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    /**
     * @param $id
     * @return mixed|static
     */
    public function find($id)
    {
        return Branch::find($id);
    }


    public function save($data)
    {

        $branch = Branch::create($data);

        $branch->save();

    }

    public function update($id, $data)
    {
        $branch = Branch::find($id);

        $branch->update($data);

    }

    public function delete($ids)
    {
        return Branch::destroy($ids);
    }

}
