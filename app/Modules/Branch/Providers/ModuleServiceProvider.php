<?php

namespace App\Modules\Branch\Providers;

use App\Modules\Branch\Repository\BranchInterface;
use App\Modules\Branch\Repository\BranchRepository;
use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'branch');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'branch');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'branch');
        $this->loadConfigsFrom(__DIR__.'/../config');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->branchRegister();
    }

    public function branchRegister()
    {
        $this->app->bind(BranchInterface::class, BranchRepository::class);
    }
}
