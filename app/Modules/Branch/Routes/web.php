<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {

    Route::group(['prefix' => 'branch'], function () {

//INDEX
        Route::get('', ['as' => 'branch.index', 'uses' => 'BranchController@index']);

//CREATE
        Route::get('create', ['as' => 'branch.create', 'uses' => 'BranchController@create']);

//STORE
        Route::post('store', ['as' => 'branch.store', 'uses' => 'BranchController@store']);

//EDIT
        Route::get('edit/{id}',['as' => 'branch.edit', 'uses' => 'BranchController@edit']);

//UPDATE
        Route::post('edit/{id}',['as' => 'branch.update', 'uses' => 'BranchController@update']);

//DELETE
        Route::get('delete/{id}',['as' => 'branch.delete', 'uses' => 'BranchController@delete']);

        Route::delete('delete', ['as' => 'branch.destroy', 'uses' => 'BranchController@destroy']);

//DETAIL
        Route::get('detail/{id}', ['as' => 'branch.detail', 'uses' => 'BranchController@detail']);

    });
});