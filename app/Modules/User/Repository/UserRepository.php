<?php

namespace App\Modules\User\Repository;


use App\BranchUser;
use App\User;
use Illuminate\Support\Facades\Session;

class UserRepository implements UserInterface
{

    public function findAll($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = User::when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['name'])) {
                $query->where('name', 'like', '%' . $filter['name'] . '%')->get();
            }

            return $query;

        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    public function findByBranchId($limit = 99999, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'])
    {
        $result = BranchUser::where('branch_id', '=', Session::get('sess_branch_id'))->when(array_keys($filter, true), function ($query) use ($filter) {

            if (!empty($filter['name'])) {
                $query->where('name', 'like', '%' . $filter['name'] . '%')->get();
            }

            return $query;

        })
            ->orderBy($sort['by'], $sort['sort'])
            ->paginate(env('DEF_PAGE_LIMIT', $limit));

        return $result;

    }

    public function save($data)
    {

        if (Session::get('sess_branch_id')) {
            $user = BranchUser::create($data);
            $user->save();
        } else {
            $user = User::create($data);
            $user->save();
        }

    }

    public function find($id)
    {
        return User::find($id);
    }

    public function update($id, $data)
    {
        $user = User::find($id);

        $user->update($data);

    }

    public function delete($ids)
    {
        return User::destroy($ids);
    }

}
