<?php
/**
 * Created by PhpStorm.
 * User: bibek
 * Date: 4/9/18
 * Time: 4:48 PM
 */

namespace App\Modules\User\Repository;

interface UserInterface
{
    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC']);

    public function findByBranchId($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC']);

    public function save($data);

    public function find($id);

    public function update($id, $role);

    public function delete($ids);

}