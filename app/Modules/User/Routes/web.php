<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['prefix' => ''], function () {

//LOGIN & LOGOUT

    Route::get('/', ['as' => 'login', 'uses' => 'AuthController@login']);

    Route::post('login', ['as' => 'login.post', 'uses' => 'AuthController@authenticate']);

    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);

//INDEX
    Route::get('user', ['as' => 'user.index', 'uses' => 'AuthController@index']);

//CREATE
    Route::get('create', ['as' => 'user.create', 'uses' => 'AuthController@create']);

//STORE
    Route::post('store', ['as' => 'user.store', 'uses' => 'AuthController@store']);

//EDIT
    Route::get('edit/{id}',['as' => 'user.edit', 'uses' => 'AuthController@edit']);

//UPDATE
    Route::post('edit/{id}',['as' => 'user.update', 'uses' => 'AuthController@update']);

//DELETE
    Route::get('delete/{id}',['as' => 'user.delete', 'uses' => 'AuthController@delete']);

    Route::delete('delete', ['as' => 'user.destroy', 'uses' => 'AuthController@destroy']);

//DETAIL
    Route::get('detail/{id}', ['as' => 'user.detail', 'uses' => 'AuthController@detail']);

});