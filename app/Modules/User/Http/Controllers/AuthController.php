<?php

namespace App\Modules\User\Http\Controllers;

use App\BranchUser;
use App\Http\Controllers\Controller;
use App\Modules\Branch\Models\Branch;
use App\Modules\Permission\Models\BranchPermission;
use App\Modules\Permission\Models\BranchUserPermission;
use App\Modules\Permission\Models\Permission;
use App\Modules\Unit\Http\Requests\UnitFormRequest;
use App\Modules\User\Http\Requests\UserFormRequest;
use App\Modules\User\Repository\UserInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class AuthController extends Controller
{
    protected $user;

    public function __construct(userInterface $user)
    {
        $this->user = $user;
    }

    public function login()
    {
        if (Auth::check() and Auth::user()->user_type == 'admin') {
            return view('admin::dashboard.index');
        } elseif (Auth::check() and Auth::user()->user_type == 'staff') {
            return view('admin::dashboard.index');
        } else if (Session::get('sess_user_type') == 'branch') {
            return view('admin::dashboard.index');
        } else if (Session::get('sess_user_type') == 'branch_staff') {
            return view('admin::dashboard.index');
        } else {
            return view('user::auth.login');
        }
    }

    public function authenticate(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        $usertype = $request->get('usertype');

        if (Auth::attempt(['email' => $email, 'password' => $password]) && $usertype == 0) {
            Session::put('sess_id_forstockout', Auth::user()->branch_id);
            //this session to display only data of admin that is presence not to display branches data
            Session::put('sess_staff_id', Auth::user()->id);
            Session::put('sess_staff_name', Auth::user()->name);
            Session::put('sess_user_type', Auth::user()->user_type);
            //to retrive all data from table permission
            $permissions[] = Permission::all();
            Session::put('sess_permission', $permissions);

            //this code is to count number of row from database
            $forLastID = Permission::count();
            Session::put('sess_lastID', $forLastID);

            return redirect(route('login'));
        } else if ($usertype == 1) {
            $branches = Branch::all();

            foreach ($branches as $branch) {
                if ($email == $branch->email && $password == $branch->password) {
                    Session::put('sess_branch_id', $branch->id);
                    //this sess_branch_id is used at controller (function index)

                    Session::put('sess_staff_id', $branch->id);
                    //in this case staff_id == branch_id
                    //if i used column name branch id then there will be lots of changes in other pages so that
                    //i have used column name staff_id
                    Session::put('sess_branch_name', $branch->name);
                    Session::put('sess_user_type', 'branch');
                    Session::put('sess_branch_location', $branch->location);

                    //to retrive all data from table branchpermission
                    $permissions[] = BranchPermission::all();
                    Session::put('sess_permission', $permissions);

                    //this code is to count number of row from database
                    $forLastID = BranchPermission::count();
                    Session::put('sess_lastID', $forLastID);

                    return redirect(route('login'));
                }
            }
            Flash::success('invalid email and password for branch...');
            return view('user::auth.login');

        } else if ($usertype == 2) {
            $branches = BranchUser::all();

            foreach ($branches as $branch) {
                if ($email == $branch->email && Hash::check($password, $branch->password)) {
                    Session::put('sess_branch_id', $branch->branch_id);
                    //this sess_branch_id is used at controller (function index)

                    Session::put('sess_staff_id', $branch->id);
                    //in this case staff_id == branch_id
                    //if i used column name branch id then there will be lots of changes in other pages so that
                    //i have used column name staff_id
                    Session::put('sess_branch_name', $branch->name);
                    Session::put('sess_user_type', 'branch_staff');

                    //to retrive all data from table branchpermission
                    $permissions[] = BranchUserPermission::all();
                    Session::put('sess_permission', $permissions);

                    //this code is to count number of row from database
                    $forLastID = BranchUserPermission::count();
                    Session::put('sess_lastID', $forLastID);

                    return redirect(route('login'));
                }
            }
            Flash::success('invalid email and password for branch staff...');
            return view('user::auth.login');

        } else {
            Flash::success('invalid email and password...');
            return view('user::auth.login');

        }
    }

    public function logout()
    {
        Auth::logout();
//        Session::put('sess_staff_id', '');
//        Session::put('sess_permission_staff_id', '');
//        Session::put('sess_permission_action_id', '');
//        Session::put('sess_user_type', '');
        Session::flush();
        return redirect(route('login'));

    }

    public function index(Request $request)
    {
        $filter['name'] = $request->get('name');

        $limit = $request->get('limit', 100);

        if (Session::get('sess_branch_id')) {
            $data['user'] = $this->user->findByBranchId($limit, $filter);
        } else {
            $data['user'] = $this->user->findAll($limit, $filter);
        }

        return view('user::user.index', $data);
    }

    public function create()
    {
        return view('user::user.create');
    }

    public function store(UserFormRequest $request)
    {

//        $input = $request->all();

        $input = array(
            'user_type' => $request->user_type,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'branch_id' => $request->branch_id

        );
        try {

            $this->user->save($input);

            Flash::success("Successfully Created");

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('user.index'));

    }

    public function edit($id)
    {
        $users = $this->user->find($id);

        return view('user::user.edit', compact('users'));
    }

    public function update(UserFormRequest $request, $id)
    {
        try {

            $input = array(
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            );

            $this->user->update($id, $input);

            Flash::success("Data Updated Successfully");

            return redirect(route('user.index'));

        } catch (\Throwable $e) {

            Flash::error($e->getMessage());
        }
    }

    public function delete($ids)
    {
        return User::destroy($ids);
    }

    public function destroy(Request $request)
    {
        $ids = $request->all();

        try {

            if ($request->has('toDelete')) {

                User::destroy($ids['toDelete']);

                Flash::success("Successfully Deleted");

            } else {

                Flash::error("Please check at least one to delete");

            }
        } catch (\Throwable $e) {

            Flash::error($e->getMessage());

        }

        return redirect(route('user.index'));
    }

    public function detail($id)
    {
        $data['users'] = User::find($id);

        return view('user::user.detail', $data);
    }

}