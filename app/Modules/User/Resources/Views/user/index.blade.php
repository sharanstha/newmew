@extends("admin::layout")
@forelse (Session::get('sess_permission') as $permission)
    @for($i=0;$i<Session::get('sess_lastID');$i++)
        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==2)
@section('content')
    <body>
    <section class="content">

        <div class="box box-primary ">

            <div class="panel-heading">
                <div class="col-xs-4">
                    <input type="text" class="search form-control" placeholder="search?...">
                </div>
            </div>
            <br><br>

            {!! Form::open(['route' => 'user.destroy','method'=>'DELETE','id'=>'formDelete']) !!}
            <div class="box-header with-border">

                @include('flash::message')

                <ul class="icons-list pull-right">

                    <a href="{{ route('user.create')}}">
                        <button id="create-user" type="button" class="btn btn-primary btn-sm glyphicon glyphicon-plus">
                            <b>CREATE</b>
                        </button>
                    </a>

                    <a href="{{route('user.index')}}">
                        <button type="button" class="btn btn-default btn-sm glyphicon glyphicon-repeat">
                            <b>RESET</b>
                        </button>
                    </a>
                    {{--<button type="submit" class="btn btn-danger btn-sm glyphicon glyphicon-remove"--}}
                    {{--onclick="return confirm('Are you sure ?');">--}}
                    {{--<b>DELETE</b>--}}
                    {{--</button>--}}
                </ul>
                <br><br><br>

                <div style="width: auto; height: auto; overflow: scroll;" id="userTbl">

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Check All</th>
                            <th>ID</th>
                            <th>User Type</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Created Date</th>
                            <th>Updated Date</th>
                            <th>Permission</th>


                        </tr>
                        </thead>

                        <tbody>

                        @forelse($user as $users)
                            <tr>
                                    <td>
                                        <a href="{{ route('user.edit', array($users->id))}}"
                                           class="btn btn-info btn-xs"><label
                                                    class="glyphicon glyphicon-pencil"></label> &nbsp;Edit</a>

                                        <a href="{{ route('user.detail', array($users->id))}}"
                                           class="btn btn-success btn-xs"><label
                                                    class="glyphicon glyphicon-eye-open"></label> &nbsp;View Detail</a>
                                    </td>
                                    <td>
                                        <div class="pretty p-default">
                                            {!! Form::checkbox('toDelete[]',$users->id, false,['class'=>'checkItem']) !!}
                                            <div class="state">
                                                <label></label>
                                            </div>
                                        </div>
                                    </td>

                                    <td>{{$users->id}}

                                    <td>{{$users->user_type}}

                                    <td>{{$users->name}}

                                    <td>{{$users->email}}

                                    <td>{{$users->created_at}}

                                    <td>{{$users->updated_at}}

                                @if($users->id<>1 || Session::get('sess_branch_id'))

                                    <td>

                                        @if(Session::get('sess_branch_id'))
                                            @forelse (Session::get('sess_permission') as $permission)
                                                @for($i=0;$i<Session::get('sess_lastID');$i++)
                                                    @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==14)

                                                        <a href="{{ route('branchuserpermission.usercreate')}}?branchuserid={{$users->id}}"
                                                           class="btn btn-success btn-xs"><label
                                                                    class="glyphicon glyphicon-eye-open"></label>Branch
                                                            Permission</a>
                                                    @endif
                                                @endfor
                                            @empty
                                            @endforelse
                                        @else

                                            @forelse (Session::get('sess_permission') as $permission)
                                                @for($i=0;$i<Session::get('sess_lastID');$i++)
                                                    @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==9)
                                                        {{--sess_staff_id is id of currently login user--}}
                                                        <a href="{{ route('permission.create')}}?userid={{$users->id}}"
                                                           class="btn btn-success btn-xs"><label
                                                                    class="glyphicon glyphicon-eye-open"></label>User
                                                            Permission</a>
                                                    @endif
                                                @endfor
                                            @empty
                                            @endforelse

                                        @endif

                                    </td>
                                @endif
                            </tr>
                            {{--@else--}}
                            {{--<td></td>--}}

                        @empty
                            <tr>
                                <td colspan="15">
                                    <p class="text-danger text-center"><b>No data found !</b></p>
                                </td>

                            </tr>
                        @endforelse

                        </tbody>

                    </table>
                </div>
                {{ $user->links() }}
            </div>
            {!! Form::close() !!}
        </div>
    </section>

    </body>

@endsection

@endif
@endfor
@empty
@endforelse