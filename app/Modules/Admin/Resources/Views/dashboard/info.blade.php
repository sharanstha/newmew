<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-commerce</title>
    <!-- Tell the browser to be responsive to screen width -->
{{--<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">--}}
<!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendor/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendor/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendor/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/skins/_all-skins.min.css')}}">
@yield('styles')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            max-width: 300px;
            margin: auto;
            color: gray;
            text-align: center;
            font-family: arial;
        }

        .title {
            color: grey;
            font-size: 18px;
        }

        button {
            border: none;
            outline: 0;
            display: inline-block;
            padding: 8px;
            color: black;
            background-color: #00b3ee;
            text-align: center;
            cursor: pointer;
            width: 100%;
            font-size: 18px;
        }

        a {
            text-decoration: none;
            font-size: 22px;
            color: black;
        }

        button:hover, a:hover {
            opacity: 0.7;
        }
    </style>
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<h2 style="text-align:center">Admin Profile Card</h2>

<div class="card">
    <img src="{{asset('assets/admin/dist/img/bibek.jpg')}}" style="height: 200px;width: 300px " alt="Bibek" style="width:100%">
    <h1>Bibek Dhami</h1>
    <p class="title">CEO & Founder at 201%</p>
    <p class="title">Laravel Developer</p>

    <p>Pokhara University</p>
    <div style="margin: 24px 0;">
        <a href="https://www.instagram.com/bibek.dhami/"><i class="fa fa-instagram"></i></a>
        <a href="https://www.facebook.com/dhami.bibek"><i class="fa fa-facebook"></i></a>
    </div>
    <p><button>Contact</button></p>
</div>


<script src="{{asset('assets/admin/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/admin/vendor/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/admin/vendor/fastclick/lib/fastclick.js')}}"></script>
<script src="{{asset('assets/admin/js/adminlte.min.js')}}"></script>
<script src="{{asset('assets/admin/js/demo.js')}}"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#editor' });</script>

<script src="{{asset('js/custom.js')}}"></script>



@yield('scripts')
<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
</script>
</body>
</html>
