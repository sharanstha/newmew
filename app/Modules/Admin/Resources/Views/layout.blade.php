<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NewMew</title>
    <!-- Tell the browser to be responsive to screen width -->
{{--<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">--}}
<!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendor/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendor/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendor/Ionicons/css/ionicons.min.css')}}">
    <!-- Morris charts -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendor/morris.js/morris.css')}}">
    <!-- fullCalendar -->
    <link rel="stylesheet" href="{{asset('assets/admin/vendor/fullcalendar/dist/fullcalendar.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/vendor/fullcalendar/dist/fullcalendar.print.min.css')}}"
          media="print">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/skins/_all-skins.min.css')}}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Alex+Brush" rel="stylesheet">
    <style>
        select:focus {
            border: solid 2px red !important;
        }

        input:focus {
            border: solid 2px red !important;
        }

        textarea:focus {
            border: solid 2px red !important;
        }

        * {
            box-sizing: border-box;
        }

        .img-zoom-container {
            position: relative;
        }

        .img-zoom-lens {
            position: absolute;
            border: 1px solid #d4d4d4;
            /*set the size of the lens:*/
            width: 40px;
            height: 40px;
        }

        .img-zoom-result {
            border: 1px solid #d4d4d4;
            /*set the size of the result div:*/
            width: 450px;
            height: 350px;
        }

        #hidden {
            z-index: 9999;
            display: block;
            position: fixed;
            height: 20%;
            width: 40%;
            left: 800px;
            top: 130px;
            text-align: center;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        body {
            /*background: #FFF url("https://i.imgur.com/KheAuef.png") top left repeat-x;*/
            font-family: 'Alex Brush', !important;

        }

        #loading {
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 100;
            width: 100vw;
            height: 100vh;
            background-color: rgba(192, 192, 192, 0.5);
            background-image: url("https://i.stack.imgur.com/MnyxU.gif");
            background-repeat: no-repeat;
            background-position: center;
        }

    </style>
@yield('styles')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>

<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <a href="{{route('dashboard')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>NewMew</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>NewMew</b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>


            <div class="navbar-custom-menu">
                {{--<marquee scrolldelay="200" class="logo"><b>USERTYPE:</b>{{Session::get('sess_user_type')}}</marquee>--}}
                {{--<marquee scrolldelay="200" class="logo">--}}
                {{--<b>USERNAME:</b>{{Session::get('sess_staff_name')}}{{Session::get('sess_branch_name')}}</marquee>--}}
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <a href="{{route('logout')}}" class="user-header">
                            <img src="{{asset('assets/admin/dist/img/index.png')}}" class="user-image" alt="User Image">
                            <span class="hidden-xs">Logout</span>
                        </a>
                    </li>

                </ul>
            </div>
        </nav>
    </header>


    <aside class="main-sidebar">
        <section class="scrollbar">
            <div id="loading"></div>

            <ul class="sidebar-menu" data-widget="tree"><br>

                <li>
                    <a href="{{route('dashboard')}}">
                        <i class="glyphicon glyphicon-dashboard fa-lg"></i> <span
                                style="font-size: medium">Dashboard</span>
                        <span class="pull-right-container">
                            </span>
                    </a>
                </li>


                @forelse (Session::get('sess_permission') as $permission)
                    @for($i=0;$i<Session::get('sess_lastID');$i++)
                        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==1)
                            <li>
                                <a href="{{route('customer.index')}}">
                                    <i class="fa fa-users fa-lg"></i> <span style="font-size: medium">Customer</span>
                                    <span class="pull-right-container">
                                     </span>
                                </a>
                            </li>
                        @endif
                    @endfor
                @empty
                @endforelse

                @forelse (Session::get('sess_permission') as $permission)
                    @for($i=0;$i<Session::get('sess_lastID');$i++)
                        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==2)
                            <li>
                                <a href="{{route('user.index')}}">
                                    <i class="fa fa-user fa-lg"></i>
                                    <span style="color:blue;font-size: larger">User</span>
                                </a>
                            </li>
                        @endif
                    @endfor
                @empty
                @endforelse

                @forelse (Session::get('sess_permission') as $permission)
                    @for($i=0;$i<Session::get('sess_lastID');$i++)
                        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==3)
                            <li>
                                <a href="{{route('vendor.index')}}">
                                    <i class="glyphicon glyphicon-chevron-down"></i> <span style="font-size: medium">Vendor</span>
                                    <span class="pull-right-container"></span>
                                </a>
                            </li>
                        @endif
                    @endfor
                @empty
                @endforelse

                @forelse (Session::get('sess_permission') as $permission)
                    @for($i=0;$i<Session::get('sess_lastID');$i++)
                        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==4)
                            <li>
                                <a href="{{route('category.index')}}">
                                    <i class="glyphicon glyphicon-th-list fa-lg"></i>
                                    <span style="font-size: medium">Category</span>
                                </a>
                            </li>
                        @endif
                    @endfor
                @empty
                @endforelse

                @forelse (Session::get('sess_permission') as $permission)
                    @for($i=0;$i<Session::get('sess_lastID');$i++)
                        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==5)
                            <li>
                                <a href="{{route('brand.index')}}">
                                    <i class="glyphicon glyphicon-bold fa-lg"></i>
                                    <span style="font-size: medium">Brand</span>
                                </a>
                            </li>
                        @endif
                    @endfor
                @empty
                @endforelse

                @forelse (Session::get('sess_permission') as $permission)
                    @for($i=0;$i<Session::get('sess_lastID');$i++)
                        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==8)
                            <li>
                                <a href="{{route('branch.index')}}">
                                    <i class="glyphicon glyphicon-asterisk fa-lg"></i>
                                    <span style="font-size: medium">Branch</span>
                                </a>
                            </li>
                        @endif
                    @endfor
                @empty
                @endforelse

                @forelse (Session::get('sess_permission') as $permission)
                    @for($i=0;$i<Session::get('sess_lastID');$i++)
                        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==6)
                            <li>
                                <a href="{{route('stockIn.index')}}">
                                    <i class="fa fa-arrow-circle-o-right fa-lg"></i>
                                    <span style="font-size: medium">StockIn</span>
                                </a>
                            </li>
                        @endif
                    @endfor
                @empty
                @endforelse

                @forelse (Session::get('sess_permission') as $permission)
                    @for($i=0;$i<Session::get('sess_lastID');$i++)
                        @if(Session::get('sess_staff_id')== $permission[$i]->staff_id && $permission[$i]->action_id==7)
                            <li>
                                <a href="{{route('stockout.index')}}">
                                    <i class="fa fa-arrow-circle-o-left fa-lg"></i>
                                    <span style="font-size: medium">StockOut</span>
                                </a>
                            </li>
                        @endif
                    @endfor
                @empty
                @endforelse

            </ul>
        </section>
    </aside>


    <div class="content-wrapper">
        @yield('content')
    </div>

    <div class="control-sidebar-bg"></div>
</div>


<script src="{{asset('assets/admin/vendor/jquery/dist/jquery.min.js')}}"></script>

<script src="{{asset('assets/admin/vendor/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<script src="{{asset('assets/admin/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>

<script src="{{asset('assets/admin/vendor/fastclick/lib/fastclick.js')}}"></script>

<script src="{{asset('assets/admin/js/adminlte.min.js')}}"></script>

<script src="{{asset('assets/admin/js/demo.js')}}"></script>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=naen39d3dd8ozw6ulsv5mrt6dr4enaveqgjodf3gi98d3mmn"></script>

<script>tinymce.init({selector: '#editor'});</script>

<script src="{{asset('js/custom.js')}}"></script>

<!-- Morris.js charts -->
<script src="{{asset('assets/admin/vendor/raphael/raphael.min.js')}}"></script>

<script src="{{asset('assets/admin/vendor/morris.js/morris.min.js')}}"></script>

<!-- fullCalendar -->
<script src="{{asset('assets/admin/vendor/moment/moment.js')}}"></script>

<script src="{{asset('assets/admin/vendor/fullcalendar/dist/fullcalendar.min.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

{{--<script src="{{ asset('js/jQuery.print.js') }}"></script>--}}
<script type="text/javascript">
    jQuery(function ($) {
        'use strict';
        $("#print-link").click(function () {
            // $.print("#ele2");
            // $.close();
            var thePopup = window.open('', "barcode",);
            $('#ele2').clone().appendTo(thePopup.document.body);
            thePopup.print();
            thePopup.close();
        });
    });
</script>

@yield('printscript')
@yield('printitemlist')
@yield('reordernotification')
@yield('grantRevoke')
@yield('scripts')
<script>
    $(function () {
        "use strict";

        // AREA CHART
        var area = new Morris.Area({
            element: 'revenue-chart',
            resize: true,
            data: [
                {y: '2011 Q1', item1: 2666, item2: 2666},
                {y: '2011 Q2', item1: 2778, item2: 2294},
                {y: '2011 Q3', item1: 4912, item2: 1969},
                {y: '2011 Q4', item1: 3767, item2: 3597},
                {y: '2012 Q1', item1: 6810, item2: 1914},
                {y: '2012 Q2', item1: 5670, item2: 4293},
                {y: '2012 Q3', item1: 4820, item2: 3795},
                {y: '2012 Q4', item1: 15073, item2: 5967},
                {y: '2013 Q1', item1: 10687, item2: 4460},
                {y: '2013 Q2', item1: 8432, item2: 5713}
            ],
            xkey: 'y',
            ykeys: ['item1', 'item2'],
            labels: ['Item 1', 'Item 2'],
            lineColors: ['#a0d0e0', '#3c8dbc'],
            hideHover: 'auto'
        });

        // LINE CHART
        var line = new Morris.Line({
            element: 'line-chart',
            resize: true,
            data: [
                {y: '2011 Q1', item1: 2666},
                {y: '2011 Q2', item1: 2778},
                {y: '2011 Q3', item1: 4912},
                {y: '2011 Q4', item1: 3767},
                {y: '2012 Q1', item1: 6810},
                {y: '2012 Q2', item1: 5670},
                {y: '2012 Q3', item1: 4820},
                {y: '2012 Q4', item1: 15073},
                {y: '2013 Q1', item1: 10687},
                {y: '2013 Q2', item1: 8432}
            ],
            xkey: 'y',
            ykeys: ['item1'],
            labels: ['Item 1'],
            lineColors: ['#3c8dbc'],
            hideHover: 'auto'
        });

        //DONUT CHART
        var donut = new Morris.Donut({
            element: 'sales-chart',
            resize: true,
            colors: ["#3c8dbc", "#f56954", "#00a65a"],
            data: [
                {label: "Download Sales", value: 12},
                {label: "In-Store Sales", value: 30},
                {label: "Mail-Order Sales", value: 20}
            ],
            hideHover: 'auto'
        });
        //BAR CHART
        var bar = new Morris.Bar({
            element: 'bar-chart',
            resize: true,
            data: [
                {y: '2006', a: 100, b: 90},
                {y: '2007', a: 75, b: 65},
                {y: '2008', a: 50, b: 40},
                {y: '2009', a: 75, b: 65},
                {y: '2010', a: 50, b: 40},
                {y: '2011', a: 75, b: 65},
                {y: '2012', a: 100, b: 90}
            ],
            barColors: ['#00a65a', '#f56954'],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['CPU', 'DISK'],
            hideHover: 'auto'
        });
    });
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })

    $(function () {



        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date()
        var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear()
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                today: 'today',
                month: 'month',
                week: 'week',
                day: 'day'
            },
            //Random default events

            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject')

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject)

                // assign it the date that was reported
                copiedEventObject.start = date
                copiedEventObject.allDay = allDay
                copiedEventObject.backgroundColor = $(this).css('background-color')
                copiedEventObject.borderColor = $(this).css('border-color')

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove()
                }

            }
        })


        //Create eventsbib
        var event = $('<div />')
        event.css({
            'background-color': currColor,
            'border-color': currColor,
            'color': '#fff'
        }).addClass('external-event')
        event.html(val)
        $('#external-events').prepend(event)

        //Add draggable funtionality
        init_events(event)

        //Remove event from text input
        $('#new-event').val('')
    })


</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<style>
    label, input {
        display: block;
    }

    input.text {
        margin-bottom: 12px;
        width: 95%;
        padding: .4em;
    }

    fieldset {
        padding: 0;
        border: 0;
        margin-top: 25px;
    }

    h1 {
        font-size: 1.2em;
        margin: .6em 0;
    }

    div#users-contain {
        width: 350px;
        margin: 20px 0;
    }

    div#users-contain table {
        margin: 1em 0;
        border-collapse: collapse;
        width: 100%;
    }

    div#users-contain table td, div#users-contain table th {
        border: 1px solid #eee;
        padding: .6em 10px;
        text-align: left;
    }

    .ui-dialog .ui-state-error {
        padding: .3em;
    }

    .validateTips {
        border: 1px solid transparent;
        padding: 0.3em;
    }
</style>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<!-- for stockout page -->
<script type="text/javascript">
    $(document).ready(function () {
        // default focus in customer number
        $('#customerNumber').focus();

        // $('#formid').on('keyup keypress', function (e) {
        //     var keyCode = e.keyCode || e.which;
        //     if (keyCode === 13) {
        //         e.preventDefault();
        //         return false;
        //     }
        // });

        //to display customer name without reloading page after selecting customernumber
        $('#customerNumber').change(function () {
            var customerNumber = $('#customerNumber').val();
            $.ajax({
                url: "{{route('getName')}}",
                type: "GET",
                data: {customerNumber: customerNumber},
                success: function (data) {
                    var d = JSON.parse(data);
                    for (var x in d) {
                        $('#customerName').val(d[x].first_name + " " + d[x].last_name);
                        $('#customerId').val((d[x].id));
                    }
                    // $('#customerName').val(data);
                }
            });
            $('#itembarcode0').focus().trigger('keydown');

        });
        
        //to display rate, maximum quantity and calculatte total rate
        $('#itembarcode0').click(function (event) {

            var item = $('#itembarcode0').val();
            $.ajax({
                url: "{{route('getData')}}",
                type: "GET",
                data: {item: item},
                datatype: 'json',
                success: function (data) {
                    var d = JSON.parse(data);
                    for (var x in d) {
                        $('#item0').val((d[x].name));
                        $('#rate0').val((d[x].per_item_selling_price));
                        $('#maxquantity0').val((d[x].quantity));
                        $('#reorder_level0').val((d[x].reorder_level));
                    }

                }
            });

            $('#quantity0').focus();
            $('#quantity0').val('');

            forNetAmountManagementup();
            $('#totalamount0').val('');
            event.preventDefault();

        });

        // to auto calculatte quantity
        $('#quantity0').change(function () {
            //jquery
            var rate = $('#rate0').val()
            var quantity = $('#quantity0').val();
            var maxquantity = $('#maxquantity0').val();
            var reorderlevel = $('#reorder_level0').val();
            var quantityMsg = $('#quantityMsg0');
            var totalrate = rate * quantity;
            $('#totalrate0').val(totalrate);
            $('#discount0').val('');
            quantityMsg.html('');

            if (parseInt(quantity) <= parseInt(maxquantity) || quantity == '') {

                // to notify if item is going to be finished
                if (parseInt(maxquantity) < parseInt(reorderlevel)) {

                    // Get the modal
                    var modal = document.getElementById('myModal');
                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close")[0];

                    $('#itemname').html($('#item0').val());
                    modal.style.display = "block";

                    // When the user clicks on <span> (x), close the modal
                    span.onclick = function () {
                        modal.style.display = "none";
                        $('#discount0').focus();
                    }

                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                            $('#discount0').focus();
                        }
                    }

                }
                $('#totalrate0').val(totalrate);
                quantityMsg.html('');
                $('#btnSOsave').prop('disabled', false);
                $('#discount0').attr('readonly', false);
                $('#discount0').focus();

                //to get remaining value to update qunatity in stockin
                var rem_quantity = (parseInt(maxquantity) - parseInt(quantity));
                $('#rem_quantity0').val(rem_quantity);
                $('#addAnother').show();

            } else {
                quantityMsg.css({
                    'color': 'red'
                });
                quantityMsg.html('stockin quantity exceeds');
                alert('stockin quantity exceeds');
                $('#quantity0').focus();
                $('#btnSOsave').prop('disabled', true);
                $('#discount0').attr('readonly', true);

                $('#addAnother').hide();
            }

        });

        // // to recalculatte total rate after making change in item, quantity
        // $('#itembarcode0').change(function () {
        //     var quantity = $('#quantity0').val();
        //     var rate = $('#rate0').val()
        //     $('#totalrate0').val(quantity * rate);
        //
        // });

        // for overall calculation
        $('#taken_cost').change(function (event) {
            var net_total = $('#net_total').val();
            var taken_cost = $('#taken_cost').val();
            var return_cost = parseFloat(taken_cost) - parseFloat(net_total);
            $('#return_cost').val(Intl.NumberFormat('en-IN', {maximumSignificantDigits: 3}).format(return_cost));
            event.preventDefault();
        });

        $('#discount0').change(function (event) {
            forDiscount();
            event.preventDefault();
        });


        // to calculatte after selecting sign
        $('#sign0').change(function (event) {
            forDiscount();
            event.preventDefault();
        });

        function forDiscount() {
            var totalrate = $('#totalrate0').val();
            var discount = $('#discount0').val();
            var sign = $('#sign0').val();
            var net_total = $('#net_total').val();
            var totalamount;

            if (sign > 0) {
                if (parseInt(discount) > 0 && parseInt(discount) < parseInt(totalrate)) {
                    totalamount = totalrate - discount;
                    var net_total_at_last = parseInt(net_total) + parseInt(totalamount);
                    $('#net_total').val(net_total_at_last);
                    $('#totalamount0').val(totalamount);
                    $('#btnSOsave').prop('disabled', false);
                    $('#addAnother').show();

                } else {
                    $('#discount0').val('');
                    $('#discount0').focus();
                    alert('please enter valid discount');
                    $('#btnSOsave').prop('disabled', true);
                    $('#addAnother').hide();

                }
            } else {
                if (parseInt(discount) > 0 && parseInt(discount) < 100) {
                    totalamount = totalrate - ((discount / 100) * totalrate);
                    var net_total_at_last = parseInt(net_total) + parseInt(totalamount);
                    $('#net_total').val(net_total_at_last);
                    $('#totalamount0').val(totalamount);
                    $('#btnSOsave').prop('disabled', false);
                    $('#addAnother').show();

                } else {
                    $('#discount0').val('');
                    $('#discount0').focus();
                    alert('please enter valid discount');
                    $('#btnSOsave').prop('disabled', true);
                    $('#addAnother').hide();

                }
            }
            $('#sign0').prop('disabled', true);

        }

        $('#discount0').click(function () {
            forNetAmountManagementup();

            $('#discount0').val('');
            $('#totalamount0').val('');

        });

        $('#quantity0').click(function () {
            forNetAmountManagementup();

            $('#quantity0').val('');
            $('#totalamount0').val('');

        });

        // net amount management on change of quantity and discounr
        function forNetAmountManagementup() {

            //to (substract) or manage net_total on changing discount
            var totalamount = $('#totalamount0').val();
            var net_total = $('#net_total').val();
            if (parseInt(totalamount) > 0) {
                var net_total_at_last = parseInt(net_total) - parseInt(totalamount);
                $('#net_total').val(parseInt(net_total_at_last));
            }
        }

        $("#addAnother").click(function (event) {
            addAnotherRow();
            event.preventDefault();
        });

        function addAnotherRow() {

            var row = $("#tblDoc tr").last().clone().find("input:text").val("").end();
            //row.id = row.id.slice(-1);
            var oldId = Number(row.attr('id').slice(-1));
            var id = 1 + oldId;


            //attribute id
            row.attr('id', 'uploadrow_' + id);
            row.find('#itembarcode' + oldId).attr('id', 'itembarcode' + id);
            row.find('#item' + oldId).attr('id', 'item' + id);
            row.find('#itemresult' + oldId).attr('id', 'itemresult' + id);
            row.find('#rate' + oldId).attr('id', 'rate' + id);
            row.find('#maxquantity' + oldId).attr('id', 'maxquantity' + id);
            row.find('#quantity' + oldId).attr('id', 'quantity' + id);
            row.find('#rem_quantity' + oldId).attr('id', 'rem_quantity' + id);
            row.find('#reorder_level' + oldId).attr('id', 'reorder_level' + id);
            row.find('#quantityMsg' + oldId).attr('id', 'quantityMsg' + id);
            row.find('#totalrate' + oldId).attr('id', 'totalrate' + id);
            row.find('#discount' + oldId).attr('id', 'discount' + id);
            row.find('#totalamount' + oldId).attr('id', 'totalamount' + id);
            row.find('#sign' + oldId).attr('id', 'sign' + id);
            row.find('#remove' + oldId).attr('id', 'remove' + id);
            row.find('#clickcount' + oldId).attr('id', 'clickcount' + id);

            //attribute name
            row.find('#itembarcode' + id).attr('name', 'stockin_id' + id);
            row.find('#quantity' + id).attr('name', 'quantity' + id);
            row.find('#rem_quantity' + id).attr('name', 'rem_quantity' + id);
            row.find('#totalrate' + id).attr('name', 'totalrate' + id);
            row.find('#discount' + id).attr('name', 'discount' + id);
            row.find('#totalamount' + id).attr('name', 'totalamount' + id);
            $('#tblDoc').append(row);


            //for clickcount
            $('#' + row.find('#clickcount' + id).attr('id')).val(id);

            //to make discount_by dropdox enabled
            $('#' + row.find('#sign' + id).attr('id')).prop('disabled', false);


            // to make upper row readonly
            // $("#quantity0").attr('readonly', true);
            // $("#itembarcode0").hide();
            // $("#sign0").prop('disabled', true);
            // $("#remove0").hide();
            // $("#discount0").attr('readonly', true);

            // $("#quantity" + oldId).attr('readonly', true);
            $("#itembarcode" + oldId).hide();
            $("#sign" + oldId).prop('disabled', true);
            $("#remove" + oldId).hide();
            // $("#discount" + oldId).attr('readonly', true);


            $('#' + row.find('#itembarcode' + id).attr('id')).click(function () {
                var item = $('#' + row.find('#itembarcode' + id).attr('id')).val();
                var itemresult = $('#' + row.find('#itemresult' + id).attr('id'));
                var count = $('#' + row.find('#clickcount' + id).attr('id')).val();
                $.ajax({
                    url: "{{route('getData')}}",
                    type: "GET",
                    data: {item: item},
                    datatype: 'json',
                    success: function (data) {
                        var d = JSON.parse(data);
                        for (var x in d) {
                            $('#' + row.find('#item' + id).attr('id')).val((d[x].name));
                            $('#' + row.find('#rate' + id).attr('id')).val((d[x].per_item_selling_price));
                            $('#' + row.find('#maxquantity' + id).attr('id')).val((d[x].quantity));
                            $('#' + row.find('#reorder_level' + id).attr('id')).val((d[x].reorder_level));
                        }

                    }
                });

                //css for itemresult
                itemresult.css({
                    'color': 'red'
                });
                // check if already select at up
                for (var i = 0; i < count; i++) {
                    var aboveitem = $('#itembarcode' + i).val();

                    if (aboveitem == item) {
                        if (itemresult.html('you have used this item at up...')) {
                            $('#addAnother').hide();
                            alert('please select unique item...');
                            $('#btnSOsave').prop('disabled', true);
                        }
                        $('#' + row.find('#itembarcode' + id).attr('id')).attr('readonly', false);
                        $('#' + row.find('#quantity' + id).attr('id')).attr('readonly', true);
                        $('#' + row.find('#discount' + id).attr('id')).attr('readonly', true);
                        document.getElementById(row.find('#itembarcode' + id).attr('id')).focus();
                        $('#' + row.find('#itemresult' + id).attr('id')).html('you have used this item at up...');
                        $('#btnSOsave').prop('disabled', true);
                        return false;
                    } else {
                        // this code is to show 'add another row' button after hidden due to duplicate SKU
                        $('#addAnother').show();
                        $('#' + row.find('#itemresult' + id).attr('id')).html('');
                        $('#' + row.find('#itembarcode' + id).attr('id')).attr('readonly', false);
                        $('#' + row.find('#quantity' + id).attr('id')).attr('readonly', false);
                        $('#' + row.find('#discount' + id).attr('id')).attr('readonly', false);
                        $('#btnSOsave').prop('disabled', false);

                    }
                }

                $('#' + row.find('#quantity' + id).attr('id')).focus();
                $('#' + row.find('#quantity' + id).attr('id')).val('');

                forNetAmountManagement();
                $('#' + row.find('#totalamount' + id).attr('id')).val('');
                event.preventDefault();

            });

            // to calculate toral rate
            $('#' + row.find('#quantity' + id).attr('id')).change(function () {
                //jquery
                var rate = $('#' + row.find('#rate' + id).attr('id')).val()
                var quantity = $('#' + row.find('#quantity' + id).attr('id')).val();
                var reorderlevel = $('#' + row.find('#reorder_level' + id).attr('id')).val();
                var maxquantity = $('#' + row.find('#maxquantity' + id).attr('id')).val();
                var quantityMsg = $('#quantityMsg' + id);
                var totalrate = rate * quantity;
                $('#' + row.find('#totalrate' + id).attr('id')).val(totalrate);
                $('#' + row.find('#discount' + id).attr('id')).val('');
                quantityMsg.html('');


                if (parseInt(quantity) <= parseInt(maxquantity) || quantity == '') {

                    // to notify if item is going to be finished
                    if (parseInt(maxquantity) < parseInt(reorderlevel)) {
                        // Get the modal
                        var modal = document.getElementById('myModal');
                        // Get the <span> element that closes the modal
                        var span = document.getElementsByClassName("close")[0];

                        $('#itemname').html($('#' + row.find('#item' + id).attr('id')).val());
                        modal.style.display = "block";

                        // When the user clicks on <span> (x), close the modal
                        span.onclick = function () {
                            modal.style.display = "none";
                            $('#' + row.find('#discount' + id).attr('id')).focus();

                        }

                        // When the user clicks anywhere outside of the modal, close it
                        window.onclick = function (event) {
                            if (event.target == modal) {
                                modal.style.display = "none";
                                $('#' + row.find('#discount' + id).attr('id')).focus();

                            }
                        }
                        $('#' + row.find('#quantity' + id).attr('id')).focus();
                    }

                    $('#' + row.find('#totalrate' + id).attr('id')).val(totalrate);
                    quantityMsg.html('');
                    $('#btnSOsave').prop('disabled', false);
                    $('#' + row.find('#discount' + id).attr('id')).attr('readonly', false);
                    $('#' + row.find('#discount' + id).attr('id')).focus();

                    //to get remaining value to update quantity in stockin
                    var rem_quantity = (parseInt(maxquantity) - parseInt(quantity));
                    $('#' + row.find('#rem_quantity' + id).attr('id')).val(rem_quantity);
                    $('#addAnother').show();
                } else {
                    quantityMsg.css({
                        'color': 'red'
                    });
                    quantityMsg.html('stockin quantity exceeds');
                    alert('stockin quantity exceeds');
                    $('#' + row.find('#quantity' + id).attr('id')).focus();
                    $('#btnSOsave').prop('disabled', true);
                    $('#' + row.find('#discount' + id).attr('id')).attr('readonly', true);
                    $('#addAnother').hide();

                }
            });

            // to recalculatte total rate after making change in item, quantity
            // $('#' + row.find('#itembarcode' + id).attr('id')).change(function () {
            //     var quantity = $('#' + row.find('#quantity' + id).attr('id')).val();
            //     var rate = $('#' + row.find('#rate' + id).attr('id')).val()
            //     $('#' + row.find('#totalrate' + id).attr('id')).val(quantity * rate);
            //
            // });

            $('#' + row.find('#discount' + id).attr('id')).change(function () {
                forOfOtherDiscount();
            });

            $('#' + row.find('#discount' + id).attr('id')).click(function () {
                forNetAmountManagement();

                $('#' + row.find('#discount' + id).attr('id')).val('');
                $('#' + row.find('#totalamount' + id).attr('id')).val('');

            });

            $('#' + row.find('#quantity' + id).attr('id')).click(function () {
                forNetAmountManagement();

                $('#' + row.find('#quantity' + id).attr('id')).val('');
                $('#' + row.find('#totalamount' + id).attr('id')).val('');

            });

            // net amount management on change of quantity and discounr
            function forNetAmountManagement() {

                //to (substract) or manage net_total on changing discount
                var totalamount = $('#' + row.find('#totalamount' + id).attr('id')).val();
                var net_total = $('#net_total').val();
                if (parseInt(totalamount) > 0) {
                    var net_total_at_last = parseInt(net_total) - parseInt(totalamount);
                    $('#net_total').val(parseInt(net_total_at_last));
                }
            }

            // to calculate amount after discount
            $('#' + row.find('#sign' + id).attr('id')).change(function () {
                forOfOtherDiscount();
            });

            function forOfOtherDiscount() {
                var totalrate = $('#' + row.find('#totalrate' + id).attr('id')).val();
                var discount = $('#' + row.find('#discount' + id).attr('id')).val();
                var sign = $('#' + row.find('#sign' + id).attr('id')).val();
                var net_total = $('#net_total').val();
                var totalamount;

                if (sign > 0) {
                    if (parseInt(discount) > 0 && parseInt(discount) < parseInt(totalrate)) {
                        totalamount = totalrate - discount;
                        var net_total_at_last = parseInt(net_total) + parseInt(totalamount);
                        $('#net_total').val(net_total_at_last);
                        $('#' + row.find('#totalamount' + id).attr('id')).val(totalamount);
                        $('#btnSOsave').prop('disabled', false);
                        $('#addAnother').show();

                    } else {
                        $('#' + row.find('#discount' + id).attr('id')).val('');
                        $('#' + row.find('#discount' + id).attr('id')).focus();
                        alert('please enter valid discount');
                        $('#btnSOsave').prop('disabled', true);
                        $('#addAnother').hide();

                    }
                } else {
                    if (parseInt(discount) > 0 && parseInt(discount) < 100) {
                        totalamount = totalrate - ((discount / 100) * totalrate);
                        var net_total_at_last = parseInt(net_total) + parseInt(totalamount);
                        $('#net_total').val(net_total_at_last);
                        $('#' + row.find('#totalamount' + id).attr('id')).val(totalamount);
                        $('#btnSOsave').prop('disabled', false);
                        $('#addAnother').show();

                    } else {
                        $('#' + row.find('#discount' + id).attr('id')).val('');
                        $('#' + row.find('#discount' + id).attr('id')).focus();
                        alert('please enter valid discount');
                        $('#btnSOsave').prop('disabled', true);
                        $('#addAnother').hide();

                    }
                }
                $('#' + row.find('#sign' + id).attr('id')).prop('disabled', true);

            }

            // to remove unwanted row
            $('#' + row.find('#remove' + id).attr('id')).click(function (event) {

                //to (substract) or manage net_total on changing discount
                var totalamount = $('#' + row.find('#totalamount' + id).attr('id')).val();
                var net_total = $('#net_total').val();
                if (parseInt(totalamount) > 0) {
                    var net_total_at_last = parseInt(net_total) - parseInt(totalamount);
                    $('#net_total').val(parseInt(net_total_at_last));
                }


                $('#' + row.attr('id')).remove();
                $('#btnSOsave').prop('disabled', false);
                $('#addAnother').show();

                // to remove upper row readonly
                $("#quantity" + oldId).attr('readonly', false);
                $("#itembarcode" + oldId).show();
                $("#itembarcode" + oldId).focus();
                $("#sign" + oldId).prop('disabled', false);
                $("#remove" + oldId).show();
                $("#discount" + oldId).attr('readonly', false);

                event.preventDefault();
            });

            $('#' + row.find('#itembarcode' + id).attr('id')).focus();
        }


    });
</script>


<!-- for stockin page -->
<script type="text/javascript">
    $(document).ready(function () {

            // for unique SKU
            $('#sku0').change(function () {
                checkSKU();
            });

            function checkSKU() {
                var skuresult = $('#skuresult0');
                var sku = $('#sku0').val();

                if (!sku == '') {
                    $.ajax({
                        url: "{{route('checkSKU')}}",
                        type: "get",
                        data: {sku: sku},
                        success: function (data) {
                            skuresult.css({
                                'color': 'red'
                            });
                            skuresult.html(data);
                            //this is to again focus on sku if sku is already register
                            if (skuresult.html() == 'sku already exists in database') {
                                $('#addAnotherItem').hide();
                                alert('This is already registered in database.please enter unique SKU...');
                                document.getElementById('sku0').focus();
                                $('#btnSIsave').prop('disabled', true);
                                $('#sku_generator0').hide();
                                $('#sku0').attr('readonly', false);
                            // } else if (sku.match(/[^A-Za-z0-9_-. ]/)) {
                            //     $('#addAnotherItem').hide();
                            //     alert("Special characters are not allowed. Use 'A-Z', 'a-z' and '0-9'.");
                            //     document.getElementById('sku0').focus();
                            //     $('#sku0').val('');
                            //     $('#btnSIsave').prop('disabled', true);
                            //     // $('#sku_generator0').hide();
                            } else {
                                $('#addAnotherItem').show();
                                $('#sku0').attr('readonly', true);
                                $('#btnSIsave').prop('disabled', false);
                                $('#sku_generator0').show();
                            }
                        },
                    });
                }
            }

            // to focus on vendor id writing name
            $('#name0').change(function () {
                $('#vendor0').focus();
            });

            // for auto sku generate
            $('#sku_generator0').click(function (event) {
                var name = $('#name0').val().substr(0, 1);
                var category = $("#category_id option:selected").text().substr(0, 1);
                var vendor = $("#vendor0 option:selected").text().substr(0, 1);
                var vendor_id = $('#vendor0').val();
                var brand = $("#brand0 option:selected").text().substr(0, 1);
                var unit = $("#unit0").val().substr(0, 1);
                var skulastnum = $('#clickcount0').val();
                var sku = name + category + vendor + vendor_id + brand + unit + skulastnum;
                $('#sku0').val(sku);
                $('#sku0').attr('readonly', true);
                event.preventDefault();
            });
            $('#sku_generator0').mouseout(function (event) {
                checkSKU();
                event.preventDefault();
            });

            // for SP calculation
            $('#margin0').change(function (event) {
                var cp = $('#cp0').val();
                var margin = $('#margin0').val();
                var per_value = (parseInt(margin) / 100) * parseInt(cp);
                var sp = (parseInt(cp) + parseInt(per_value));
                $('#per_item_selling_price0').val(sp);
                event.preventDefault();
            });

            // to auto focus in category select box
            $("#category_id").focus();
            $("#category_id").change(function (event) {
                // to display unit
                var category = $('#category_id').val();
                $.ajax({
                    url: "{{route('getUnit')}}",
                    type: "GET",
                    data: {category: category},
                    success: function (data) {
                        $('#unit0').val(data);
                    }
                });
                // to set default value =1 in quantity text box
                $('#maxquantity0').val(1);
                $('#name0').focus();
            });

            $("#addAnotherItem").click(function (event) {
                addAnotherItemRow();
                event.preventDefault();
            });

            function addAnotherItemRow() {
                var row = $("#tblItemDoc tr").last().clone().find("input:text").val("").end();
                //row.id = row.id.slice(-1);
                var oldId = Number(row.attr('id').slice(-1));
                var id = 1 + oldId;

                //attribute id
                row.attr('id', 'uploadItemRow_' + id);
                row.find('#name' + oldId).attr('id', 'name' + id);
                row.find('#unit' + oldId).attr('id', 'unit' + id);
                row.find('#vendor' + oldId).attr('id', 'vendor' + id);
                row.find('#brand' + oldId).attr('id', 'brand' + id);
                row.find('#color' + oldId).attr('id', 'color' + id);
                row.find('#size' + oldId).attr('id', 'size' + id);
                row.find('#sku' + oldId).attr('id', 'sku' + id);
                row.find('#sku_generator' + oldId).attr('id', 'sku_generator' + id);
                row.find('#barcode' + oldId).attr('id', 'barcode' + id);
                row.find('#lot_no' + oldId).attr('id', 'lot_no' + id);
                row.find('#per_item_selling_price' + oldId).attr('id', 'per_item_selling_price' + id);
                row.find('#cp' + oldId).attr('id', 'cp' + id);
                row.find('#margin' + oldId).attr('id', 'margin' + id);
                row.find('#maxquantity' + oldId).attr('id', 'maxquantity' + id);
                row.find('#detail' + oldId).attr('id', 'detail' + id);
                row.find('#invoice_no' + oldId).attr('id', 'invoice_no' + id);
                row.find('#reorder_level' + oldId).attr('id', 'reorder_level' + id);

                row.find('#remove' + oldId).attr('id', 'remove' + id);
                row.find('#clickcount' + oldId).attr('id', 'clickcount' + id);
                row.find('#skuresult' + oldId).attr('id', 'skuresult' + id);
                // row.find('#opener' + oldId).attr('id', 'opener' + id);

                //attribute name
                row.find('#name' + id).attr('name', 'name' + id);
                row.find('#unit' + id).attr('name', 'unit_id' + id);
                row.find('#vendor' + id).attr('name', 'vendor_id' + id);
                row.find('#brand' + id).attr('name', 'brand_id' + id);
                row.find('#color' + id).attr('name', 'color' + id);
                row.find('#size' + id).attr('name', 'size' + id);
                row.find('#sku' + id).attr('name', 'sku' + id);
                row.find('#barcode' + id).attr('name', 'barcode' + id);
                row.find('#lot_no' + id).attr('name', 'lot_no' + id);
                row.find('#per_item_selling_price' + id).attr('name', 'per_item_selling_price' + id);
                row.find('#cp' + id).attr('name', 'cp' + id);
                row.find('#margin' + id).attr('name', 'margin' + id);
                row.find('#maxquantity' + id).attr('name', 'quantity' + id);
                row.find('#detail' + id).attr('name', 'detail' + id);
                row.find('#invoice_no' + id).attr('name', 'invoice_no' + id);
                row.find('#reorder_level' + id).attr('name', 'reorder_level' + id);

                $('#tblItemDoc').append(row);

                // for auto insert unit in unit text box
                row.find('#unit' + id).attr('id', 'unit' + id).val($('#unit0').val());

                //for clickcount
                $('#' + row.find('#clickcount' + id).attr('id')).val(id);

                // for default value 1 in every quantity
                $('#' + row.find('#maxquantity' + id).attr('id')).val(1);

                //for auto focus
                row.find('#name' + id).attr('id', 'name' + id).focus();

                // to make upper row readonly
                $("#category_id").attr('readonly', true);
                $("#name" + oldId).attr('readonly', true);
                $("#vendor" + oldId).attr('readonly', true);
                $("#brand" + oldId).attr('readonly', true);
                $("#color" + oldId).attr('readonly', true);
                $("#size" + oldId).attr('readonly', true);
                $("#cp" + oldId).attr('readonly', true);
                $("#margin" + oldId).attr('readonly', true);
                $("#sku" + oldId).attr('readonly', true);
                $("#per_item_selling_price" + oldId).attr('readonly', true);
                $("#lot_no" + oldId).attr('readonly', true);
                $("#maxquantity" + oldId).attr('readonly', true);
                $("#invoice_no" + oldId).attr('readonly', true);
                $("#reorder_level" + oldId).attr('readonly', true);
                $("#detail" + oldId).attr('readonly', true);
                $("#remove" + oldId).hide();


                // to remove unwamted row
                $('#' + row.find('#remove' + id).attr('id')).click(function (event) {
                    $('#' + row.attr('id')).remove();
                    $('#btnSIsave').prop('disabled', false);

                    // to remove upper row readonly attribute
                    $("#category_id").attr('readonly', false);
                    $("#name" + oldId).attr('readonly', false);
                    $("#name" + oldId).focus();
                    $("#vendor" + oldId).attr('readonly', false);
                    $("#brand" + oldId).attr('readonly', false);
                    $("#color" + oldId).attr('readonly', false);
                    $("#size" + oldId).attr('readonly', false);
                    $("#cp" + oldId).attr('readonly', false);
                    $("#margin" + oldId).attr('readonly', false);
                    $("#per_item_selling_price" + oldId).attr('readonly', false);
                    $("#lot_no" + oldId).attr('readonly', false);
                    $("#maxquantity" + oldId).attr('readonly', false);
                    $("#invoice_no" + oldId).attr('readonly', false);
                    $("#reorder_level" + oldId).attr('readonly', false);
                    $("#detail" + oldId).attr('readonly', false);
                    $("#remove" + oldId).show();

                    $('#addAnotherItem').show();
                    event.preventDefault();
                });

                //to make text box readable
                $('#' + row.find('#sku' + id).attr('id')).attr('readonly', false);

                // for other sku textbox that is cloned after clicking add another row button
                $('#' + row.find('#sku' + id).attr('id')).change(function () {
                    checkSKUForNewRow();
                });

                function checkSKUForNewRow() {
                    var skuresult = $('#' + row.find('#skuresult' + id).attr('id'));
                    var sku = $('#' + row.find('#sku' + id).attr('id')).val();
                    var count = $('#' + row.find('#clickcount' + id).attr('id')).val();

                    for (var i = 0; i < count; i++) {
                        var abovesku = $('#sku' + i).val();
                        if (sku == abovesku) {

                            if (skuresult.html('you have used this SKU at up...')) {
                                $('#addAnotherItem').hide();
                                alert('please enter unique SKU...');
                                $('#btnSIsave').prop('disabled', true);
                                $('#' + row.find('#sku_generator' + id).attr('id')).hide();
                            }
                            $('#' + row.find('#sku' + id).attr('id')).attr('readonly', false);
                            document.getElementById(row.find('#sku' + id).attr('id')).focus();
                            $('#' + row.find('#skuresult' + id).attr('id')).html('you have used this SKU at up...');
                            $('#btnSIsave').prop('disabled', true);
                            return false;
                        } else {
                            // this code is to show 'add another row' button after hidden due to duplicate SKU
                            $('#addAnotherItem').show();
                            $('#' + row.find('#sku' + id).attr('id')).attr('readonly', false);
                            $('#' + row.find('#skuresult' + id).attr('id')).html('');
                            $('#btnSIsave').prop('disabled', false);
                            $('#' + row.find('#sku_generator' + id).attr('id')).show();
                        }
                    }


                    // this check sku in database
                    if (!sku == '') {
                        $.ajax({
                            url: "{{route('checkSKU')}}",
                            type: "get",
                            data: {sku: sku},
                            success: function (data) {

                                skuresult.css({
                                    'color': 'red'
                                });
                                skuresult.html(data);
                                //this is to again focus on sku if sku is already register
                                if (skuresult.html() == 'sku already exists in database') {
                                    $('#addAnotherItem').hide();
                                    $('#' + row.find('#sku' + id).attr('id')).attr('readonly', false);
                                    alert('This is already registered in database.please enter unique SKU...');
                                    document.getElementById(row.find('#sku' + id).attr('id')).focus();
                                    $('#btnSIsave').prop('disabled', true);
                                    $('#' + row.find('#sku_generator' + id).attr('id')).hide();

                                // } else if (sku.match(/[^A-Za-z0-9_. ]/)) {
                                //     $('#addAnotherItem').hide();
                                //     $('#' + row.find('#sku' + id).attr('id')).attr('readonly', false);
                                //     alert("Special characters are not allowed. Use 'A-Z', 'a-z' and '0-9'.");
                                //     document.getElementById(row.find('#sku' + id).attr('id')).focus();
                                //     $('#' + row.find('#sku' + id).attr('id')).val('');
                                //     $('#btnSIsave').prop('disabled', true);
                                    // $('#' + row.find('#sku_generator' + id).attr('id')).hide();
                                } else {
                                    $('#addAnotherItem').show();
                                    $('#' + row.find('#sku' + id).attr('id')).attr('readonly', true);
                                    $('#btnSIsave').prop('disabled', false);
                                    $('#' + row.find('#sku_generator' + id).attr('id')).show();

                                }
                            },
                        });
                    }
                }

                // to focus on vendor id writing name
                $('#' + row.find('#name' + id).attr('id')).change(function () {
                    $('#' + row.find('#vendor' + id).attr('id')).focus();
                });

                // for auto sku generate
                $('#' + row.find('#sku_generator' + id).attr('id')).click(function (event) {
                    var name = $('#' + row.find('#name' + id).attr('id')).val().substr(0, 1);
                    var category = $("#category_id option:selected").text().substr(0, 1);
                    var vendor = $('#' + row.find('#vendor' + id).attr('id') + ' ' + 'option:selected').text().substr(0, 1);
                    var vendor_id = $('#' + row.find('#vendor' + id).attr('id')).val();
                    var brand = $('#' + row.find('#brand' + id).attr('id') + ' ' + 'option:selected').text().substr(0, 1);
                    var unit = $('#' + row.find('#unit' + id).attr('id')).val().substr(0, 1);
                    var skulastnum = $('#' + row.find('#clickcount' + id).attr('id')).val();

                    var sku = name + category + vendor + vendor_id + brand + unit + skulastnum;
                    $('#' + row.find('#sku' + id).attr('id')).val(sku);
                    $('#' + row.find('#sku' + id).attr('id')).attr('readonly', true);
                    event.preventDefault();
                });
                $('#' + row.find('#sku_generator' + id).attr('id')).mouseout(function (event) {
                    checkSKUForNewRow();

                });

                // for SP calculation
                $('#' + row.find('#margin' + id).attr('id')).change(function (event) {
                    var cp = $('#' + row.find('#cp' + id).attr('id')).val();
                    var margin = $('#' + row.find('#margin' + id).attr('id')).val();
                    var per_value = (parseInt(margin) / 100) * parseInt(cp);
                    var sp = (parseInt(cp) + parseInt(per_value));
                    $('#' + row.find('#per_item_selling_price' + id).attr('id')).val(sp);
                    event.preventDefault();
                });

                $("#category_id").change(function (event) {
                    // to display unit
                    var category = $('#category_id').val();
                    $.ajax({
                        url: "{{route('getUnit')}}",
                        type: "GET",
                        data: {category: category},
                        success: function (data) {
                            $('#' + row.find('#unit' + id).attr('id')).val(data);
                        }
                    });
                });
            }

        }
    );
</script>
<script>

    function imageZoom(imgID, resultID) {
        var img, lens, result, cx, cy;
        img = document.getElementById(imgID);
        result = document.getElementById(resultID);
        /*create lens:*/
        lens = document.createElement("DIV");
        lens.setAttribute("class", "img-zoom-lens");
        /*insert lens:*/
        img.parentElement.insertBefore(lens, img);
        /*calculate the ratio between result DIV and lens:*/
        cx = result.offsetWidth / lens.offsetWidth;
        cy = result.offsetHeight / lens.offsetHeight;
        /*set background properties for the result DIV:*/
        result.style.backgroundImage = "url('" + img.src + "')";
        result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
        /*execute a function when someone moves the cursor over the image, or the lens:*/
        lens.addEventListener("mousemove", moveLens);
        img.addEventListener("mousemove", moveLens);
        /*and also for touch screens:*/
        lens.addEventListener("touchmove", moveLens);
        img.addEventListener("touchmove", moveLens);

        function moveLens(e) {
            var pos, x, y;
            /*prevent any other actions that may occur when moving over the image:*/
            e.preventDefault();
            /*get the cursor's x and y positions:*/
            pos = getCursorPos(e);
            /*calculate the position of the lens:*/
            x = pos.x - (lens.offsetWidth / 2);
            y = pos.y - (lens.offsetHeight / 2);
            /*prevent the lens from being positioned outside the image:*/
            if (x > img.width - lens.offsetWidth) {
                x = img.width - lens.offsetWidth;
            }
            if (x < 0) {
                x = 0;
            }
            if (y > img.height - lens.offsetHeight) {
                y = img.height - lens.offsetHeight;
            }
            if (y < 0) {
                y = 0;
            }
            /*set the position of the lens:*/
            lens.style.left = x + "px";
            lens.style.top = y + "px";
            /*display what the lens "sees":*/
            result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
        }

        function getCursorPos(e) {
            var a, x = 0, y = 0;
            e = e || window.event;
            /*get the x and y positions of the image:*/
            a = img.getBoundingClientRect();
            /*calculate the cursor's x and y coordinates, relative to the image:*/
            x = e.pageX - a.left;
            y = e.pageY - a.top;
            /*consider any page scrolling:*/
            x = x - window.pageXOffset;
            y = y - window.pageYOffset;
            return {x: x, y: y};
        }

// for image zoom
        $("#myimage-td").mouseover(function (e) {
            $(".img-zoom-container").show();
            e.preventDefault();
        });
        $(".img-zoom-container").hide();
        $("#myimage-td").mouseout(function (e) {
            $(".img-zoom-container").hide();
            e.preventDefault();
        });

    }

</script>

<script>
    $(document).ready(function () {
        $('.search').on('keyup', function () {
            var searchTerm = $(this).val().toLowerCase();
            $('#userTbl tbody tr').each(function () {
                var lineStr = $(this).text().toLowerCase();
                if (lineStr.indexOf(searchTerm) === -1) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        });
        $(".search").focus();

    });
</script>

<script>
    function onReady(callback) {
        var intervalId = window.setInterval(function () {
            if (document.getElementsByTagName('body')[0] !== undefined) {
                window.clearInterval(intervalId);
                callback.call(this);
            }
        }, 10);
    }

    function setVisible(selector, visible) {
        document.querySelector(selector).style.display = visible ? 'block' : 'none';
    }

    onReady(function () {
        setVisible('#loading', false);
    });

</script>
@yield('zoomimage')
</body>
</html>