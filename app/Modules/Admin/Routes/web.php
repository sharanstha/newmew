<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

    //DASHBOARD
    Route::get('about', ['as' => 'about', 'uses' => 'DashboardController@About']);

    Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {

    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

});
