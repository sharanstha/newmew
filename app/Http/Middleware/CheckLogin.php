<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Support\Facades\Session;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check() and Auth::user()->user_type == 'admin') {
            return $next($request);
        } elseif (Auth::check() and Auth::user()->user_type == 'staff') {
            return $next($request);
        } elseif (Session::get('sess_user_type') == 'branch') {
            return $next($request);
        } elseif (Session::get('sess_user_type') == 'branch_staff') {
            return $next($request);
        } else {
            return redirect(route('login'));
        }
    }
}
