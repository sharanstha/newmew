<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
class SessionController extends Controller
{
    public function accessSessionData(Request $request){
        if($request->session()->has('email'))
            echo $request->session()->get('email');
        else
            echo 'No data in the session';
    }
    public function storeSessionData(Request $request){
        $request->session()->put('email','admin@gmail.com');
        echo "Data has been added to session";
    }
    public function deleteSessionData(Request $request){
        $request->session()->forget('email');
        echo "Data has been removed from session.";
    }
}
